#!/usr/bin/env bash

function cleanup() {
    echo ">> cleanup"
    k3d cluster delete $CLUSTER_NAME || true
}

function _printresult() {
    if [ $? -eq 0 ]; then
        echo " ✓"
    else
        echo -ne " ✗\n\nOutput:\n$OUTPUT\n"

        exit 1
    fi
}

function test.cli() {
    echo -n "Test [haven $@] "

    OUTPUT=$(haven $@ 2>&1)

    _printresult
}

function test.cmd() {
    echo -n "Test [$1] "
    shift

    OUTPUT=$($@ 2>&1)

    _printresult
}

function test.addon() {
    test.cli addons install "$1"
    sleep 3
    test.cli addons uninstall "$1"
}

function test.docs() {
  helm upgrade --install docs ./docs/helm \
    --namespace docs \
    --create-namespace \
    --set "replicacount=1" \
    --set "service.type=NodePort" \
    --set "image.repository=$CI_REGISTRY_IMAGE/docs/dev" \
    --set "image.tag=$HAVEN_VERSION" >/dev/null 2>&1

  kubectl -n docs patch service docs-haven-docs --type='json' -p='[{"op": "add", "path": "/spec/ports/0/nodePort", "value": 32000}]' >/dev/null 2>&1
  kubectl -n docs wait --for=condition=Available deployment/docs-haven-docs >/dev/null 2>&1

  test.cmd 'smoke: docs' eval 'curl -si "http://docker:32000/techniek" | grep -q "Wat is Haven"'

  helm uninstall docs --namespace docs >/dev/null 2>&1
}

function test.dashboard() {
  echo '#!/bin/sh' >/bin/xdg-open
  chmod 755 /bin/xdg-open

  haven dashboard >/dev/null 2>&1 &
  HDPID=$!

  timeout 5 sh -c 'until nc -z localhost 3000 >/dev/null 2>&1; do sleep 0.5; done'

  test.cmd 'smoke: dashboard' eval 'curl -si "http://localhost:3000/haven" | grep -q "enable JavaScript"'

  kill $HDPID

  rm /bin/xdg-open
}

function exportHavenCli() {
    local image="$1"
    local dest="$2"

    local container_id=$(docker create "$image")

    docker cp "$container_id:/usr/local/bin/haven" "$dest"
    docker rm -f "$container_id"
}

export -f test.cli
export -f test.cmd
export -f test.docs
export -f test.dashboard
export -f test.addon
export -f _printresult

trap cleanup EXIT

# Setup

export K3S_VERSION="$1"
export DOCKER_CREDENTIALS=""

if [ -z "$K3S_VERSION" ]; then
    echo "invalid k3s version: version is empty"

    exit 1
fi

if [ ! -z "$CG_CORE_CI_DOCKER_USER" ]; then
    export DOCKER_CREDENTIALS="-u $CG_CORE_CI_DOCKER_USER:$CG_CORE_CI_DOCKER_PASS"
elif [ ! -z "$CI_JOB_TOKEN" ]; then
    export DOCKER_CREDENTIALS="-u gitlab-ci-token:$CI_JOB_TOKEN"
fi

export HAVEN_E2E_MODE=1
export IMAGE="${CI_REGISTRY_IMAGE}/cli/dev:${HAVEN_VERSION}"
export KUBECONFIG="${PWD}/kubeconfig"
export CLUSTER_NAME="${K3S_VERSION:0:4}-${HAVEN_VERSION:0:19}"

echo ">> exporting Haven CLI binary (image=$IMAGE)"
exportHavenCli "$IMAGE" "/bin/haven"

echo ">> create cluster.."
k3d cluster create --image "rancher/k3s:${K3S_VERSION}" "${CLUSTER_NAME}" --k3s-arg "--disable=traefik,metrics-server@server:0" -p "32000:32000@server:0" --wait
k3d kubeconfig get "${CLUSTER_NAME}" > kubeconfig

echo -ne ">> running tests..\n\n"

# Tests

test.cli check
test.cli addons list
cat <<EOF | xargs -I{} bash -c 'test.addon $0' {}
traefik
ingress-nginx
loki
postgres-operator
EOF

test.docs
test.dashboard
