# Team Haven

## Onboarding

### Development
Working on Haven typically only requires Gitlab access to the [Haven repository](https://gitlab.com/commonground/haven/haven). This includes write access to the [public Kanban board](https://gitlab.com/commonground/haven/haven/-/boards/963655).

### Background
When actually maintaining the Haven standard it's probably a good idea to become familiar with "Haven - Visie en Strategie.docx" and "Haven - Voorstel Standaardisatie.docx" as well.

### Internal VNG clusters
Using or maintaining the internal VNG Haven clusters is documented in the internal wiki ('How To') and the internal configuration repo ('OPERATOR README').

## Haven release management
When ready to release a new Haven version (for example "v10.1.3"):

- Adhere to the public website [Release management](https://haven.commonground.nl/techniek/de-standaard) documentation.
- Update CHANGELOG.md.
- Update docs/public/releases.json.
- Update reference/openstack/kops/haven_ri_openstack.sh version if this RI has been updated.
- Create a merge request to master with commit message: "Haven v10.1.3 release" and merge after approval.
- Tag the new release on the master branch with: `git tag v10.1.3 -m Release && git push origin v10.1.3`.
- Trigger the manual release jobs to create a new Gitlab release.
- Update the release notes in Project overview > Releases by adding the release notes from CHANGELOG.md.
- Announce the new release in #haven on the Common Ground Slack.

For more information about changes, versioning and the release cycle please visit the online documentation section [De Standaard](https://haven.commonground.nl/techniek).

## Maintenance

### What to do when a new version of Kubernetes is released
- Bump all versions for k3s images in .gitlab/ci/cli.yml for the e2e-cli-latest/e2e-cli-previous jobs.
- Check for compatibility with the CLI: Haven Compliancy Checker, Haven community addons and Haven dashboard.
- Bump CLI Go module versions.
- Bump Pipeline E2E tests Kubernetes versions.
- Notify vendors to ensure their Reference Implementation's are compatible and up to date.
- Contact all Reference Implementation vendors and work together to ensure everything is up to date for their platforms.
- Upgrade the internal VNG Haven clusters if this is still applicable for the same team.

### Haven Compliancy Checker
Ensure that every Reference Implementation still works when making changes to the Haven Compliancy Checker, by actually running it against all the different environments. Seek for balance, when changing the contents of a Printf it's highly unlikely this needs broad testing, but changing checks or platform detection may require investing time in proper test runs.

### Rebuilding the CRDs
When making changes to the CRD types (in `compliancy_types.go`) the manifests have to be regenerated. You can do this by using the makefile in the `pkg/kubernetes` directory which will automatically install missing dependencies:

```bash
haven/cli/pkg/kubernetes> $ make build
```

## Reference Implementations

### Maintained by vendors
Occasionally check with the suppliers of all the Reference Implementations wether or not their RI needs a maintenance update,
ideally testing the RI with the latest versions of their platform, Kubernetes and Haven Compliancy Checker.

### Maintained by team Haven

#### OpenStack: what to do when a new version of Kops is released
Bump all versions you can in the Dockerfile (you can look into old MR's for examples).

Build the docker container and test cluster creation and cluster upgrade on a real OpenStack environment:

```bash
haven/reference/openstack/kops> $ docker build -t registry.gitlab.com/commonground/haven/haven/reference_openstack .
haven/reference/openstack/kops> $ ./haven_ri_openstack.sh <your-env-name>

Welcome to the Haven Reference Implementation for OpenStack
# upgrade

<etc>
```

Occasionally you have to make things work again with the new Kops release, for instance by making changes to [kops/bin/create](./reference/openstack/kops/bin/create).

## Community addons
Addons should be maintained and kept up to date by the community. Team Haven steps in to ensure this is the case and works on the addons where necessary.
