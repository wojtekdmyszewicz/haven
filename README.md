# Haven
"Een standaard voor platform-onafhankelijke cloud hosting"

## Haven Compliancy Checker
Primary concern of Haven is the Haven Compliancy Compliancy Checker. This tool ensures your [Kubernetes](https://kubernetes.io/) cluster is following the Haven standard in a fully automated fashion. In effect being Haven Compliant means you should be able to painlessly migrate entire workloads from one Haven cluster to another, which don't even have to be deployed on the same environment/cloud. Furthermore you may assume that [Common Ground](https://www.commonground.nl/) applications will always be compatible with your Haven environment(s).

See [haven/cli/README.md](cli/README.md) for the Haven CLI tooling which includes the Haven Compliancy Checker.
See ['De Standaard' online documentation](https://haven.commonground.nl/techniek) for more information about the Haven standard.

## Reference Implementations
Assisting in the deployment of Haven the team and many vendors have documented and engineered multiple Reference Implementations for anyone to use. Usage is not required to comply with the Haven Standard. Please feel free to submit a [merge request](https://gitlab.com/commonground/haven/haven/-/merge_requests/new) with new Reference Implementations.

See [reference](reference) and the documentation section ['Aan de slag'](https://haven.commonground.nl/techniek/aan-de-slag).

## Documentation
Besides the README's in this reposity please have a look at the [online documentation](https://haven.commonground.nl).

The implementation of the docs website can be found at [docs/README.md](docs/README.md).

## Community Addons
Haven ships optional addons you can easily deploy on your cluster to make access and management easier, like for instance the Haven Dashboard. Addons are not part of the Haven standard and therefore not required.

See [community/addons/dashboard/README.md](community/addons/dashboard/README.md) for the Haven CLI tooling which includes the addons manager.

## License
Copyright © VNG Realisatie 2019-2023
Licensed under EUPL v1.2
