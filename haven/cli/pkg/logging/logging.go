// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package logging

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/lunixbochs/vtclean"
)

var (
	OutputFormat *string
	LogFile      *string
)

type logWriter struct {
	Severity      string
	Out           *os.File
	PersistentOut string
}

func (writer logWriter) Write(msg []byte) (int, error) {
	if writer.PersistentOut != "" {
		f, err := os.OpenFile(writer.PersistentOut, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0640)
		if err != nil {
			fmt.Printf("[E] Could not open logfile '%s': %s\n", writer.PersistentOut, err)
		}

		if _, err := fmt.Fprintf(f, "%s [%s] %s", time.Now().UTC().Format(time.RFC3339), writer.Severity, vtclean.Clean(string(msg), false)); err != nil {
			fmt.Printf("[E] Could not write to logfile '%s': %s\n", writer.PersistentOut, err)
		}

		if err := f.Close(); err != nil {
			fmt.Printf("[E] Closing logfile '%s': %s\n", writer.PersistentOut, err)
		}
	}

	return fmt.Fprintf(writer.Out, "[%s] %s", writer.Severity[:1], msg)
}

func writeLog(severity string, out *os.File, msg string, params ...interface{}) {
	if OutputFormat == nil {
		outputFormat := "text"
		OutputFormat = &outputFormat
	}

	if LogFile == nil {
		outputFile := ""
		LogFile = &outputFile
	}

	if *OutputFormat == "json" {
		out = nil
	}

	log.SetFlags(0)
	log.SetOutput(logWriter{
		severity,
		out,
		*LogFile,
	})

	log.Printf(msg, params...)
}

// Debug optionally writes a log message to file.
func Debug(msg string, params ...interface{}) {
	writeLog("DEBUG", nil, msg, params...)
}

// Info writes a log message to stdout and optionally to file as well.
func Info(msg string, params ...interface{}) {
	writeLog("INFO", os.Stdout, msg, params...)
}

// Warning writes a log message to stdout and optionally to file as well.
func Warning(msg string, params ...interface{}) {
	writeLog("WARNING", os.Stdout, msg, params...)
}

// Error writes a log message to stderr and optionally to file as well.
func Error(msg string, params ...interface{}) {
	writeLog("ERROR", os.Stderr, msg, params...)
}

// Fatal writes a log message to stderr and optionally to file as well.
func Fatal(msg string, params ...interface{}) {
	writeLog("FATAL", os.Stderr, msg, params...)

	os.Exit(1)
}

// Output optionally writes raw check output to file.
func Output(msg string, source string) {
	writeLog(fmt.Sprintf("OUTPUT: %s", strings.ToUpper(source)), nil, "\n%s\n\n%s\n%s\n", strings.Repeat("#", 60), msg, strings.Repeat("#", 60))
}
