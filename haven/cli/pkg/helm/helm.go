// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package helm

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"

	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/cli"
	"helm.sh/helm/v3/pkg/downloader"
	"helm.sh/helm/v3/pkg/getter"
)

var settings = cli.New()

// Client can be used to manage Helm charts
type Client struct {
	namespace    string
	actionConfig *action.Configuration
}

// NewClient constructs a new instance of Client
func NewClient(namespace string, kube *kubernetes.Clientset) (*Client, error) {
	kubeConfig, err := kube.KubeCliConfig(namespace)
	if err != nil {
		return nil, err
	}

	actionConfig := new(action.Configuration)
	if err := actionConfig.Init(kubeConfig, namespace, os.Getenv("HELM_DRIVER"), debug); err != nil {
		return nil, err
	}

	c := &Client{
		namespace:    namespace,
		actionConfig: actionConfig,
	}

	return c, nil
}

func fetchChartDependencies(chartRequested *chart.Chart, chartPath string, keyring string) (bool, error) {
	if req := chartRequested.Metadata.Dependencies; req != nil {
		// If CheckDependencies returns an error, we have unfulfilled dependencies.
		// As of Helm 2.4.0, this is treated as a stopping condition:
		// https://github.com/helm/helm/issues/2209
		if err := action.CheckDependencies(chartRequested, req); err != nil {
			man := &downloader.Manager{
				Out:        os.Stdout,
				ChartPath:  chartPath,
				Keyring:    keyring,
				SkipUpdate: false,
				Getters:    getter.All(&cli.EnvSettings{}),
			}
			if err := man.Update(); err != nil {
				return false, err
			}
		}

		return true, nil
	}

	return false, nil
}

func debug(format string, v ...interface{}) {
	if settings.Debug {
		format = fmt.Sprintf("[debug] %s\n", format)
		_ = log.Output(2, fmt.Sprintf(format, v...))
	}
}
