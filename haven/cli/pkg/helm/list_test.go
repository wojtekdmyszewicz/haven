package helm

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/release"
	"helm.sh/helm/v3/pkg/storage"
	"helm.sh/helm/v3/pkg/storage/driver"
)

var fakeReleases = []*release.Release{
	{
		Name:      "test-2",
		Namespace: "test-ns-2",
		Version:   2,
		Info: &release.Info{
			Status: "",
		},
		Chart: &chart.Chart{
			Metadata: &chart.Metadata{
				Name:    "test-chart-2",
				Sources: []string{"https://test-chart-2.com"},
			},
		},
	},
	{
		Name:      "test-2",
		Namespace: "test-ns-2",
		Version:   1,
	},
	{
		Name:      "test-1",
		Namespace: "test-ns-1",
		Version:   10,
		Info: &release.Info{
			Status: "deployed",
		},
		Chart: &chart.Chart{
			Metadata: &chart.Metadata{
				Name:    "test-chart-1",
				Sources: []string{"https://test-chart-1.com"},
			},
		},
	},
}

type fakeHelmDriver struct {
	driver.Driver
	listResponse []*release.Release
	listError    error
}

func (d *fakeHelmDriver) List(filter func(*release.Release) bool) ([]*release.Release, error) {
	if filter != nil && len(d.listResponse) > 0 {
		var results []*release.Release

		for _, release := range d.listResponse {
			if !filter(release) {
				continue
			}

			results = append(results, release)
		}

		return results, d.listError
	}

	return d.listResponse, d.listError
}

func TestListReleases(t *testing.T) {
	client := &Client{
		actionConfig: &action.Configuration{
			Releases: storage.Init(&fakeHelmDriver{listResponse: fakeReleases}),
		},
	}

	releases, err := client.List(ListOpts{})

	assert.NoError(t, err)
	assert.Equal(t, []*release.Release{
		fakeReleases[2],
		fakeReleases[0],
	}, releases)

	client = &Client{
		actionConfig: &action.Configuration{
			Releases: storage.Init(&fakeHelmDriver{listError: errors.New("random")}),
		},
	}
	_, err = client.List(ListOpts{})

	assert.Error(t, err)
}
