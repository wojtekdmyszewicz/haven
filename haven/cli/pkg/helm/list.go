// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package helm

import (
	"fmt"
	"sort"
	"strings"

	"helm.sh/helm/v3/pkg/release"
	"helm.sh/helm/v3/pkg/storage/driver"
)

func filterLatestReleases(releases []*release.Release) (results []*release.Release) {
	latests := map[string]*release.Release{}

	for _, release := range releases {
		key := fmt.Sprintf("%s/%s", release.Namespace, release.Name)

		if existing, ok := latests[key]; !ok || existing.Version < release.Version {
			latests[key] = release
		}
	}

	for _, release := range latests {
		results = append(results, release)
	}

	return
}

// ListOpts are options for listing Helm charts
type ListOpts struct {
	Namespace string
	Filter    func(*release.Release) bool
}

// List installed Helm charts
func (h *Client) List(opts ListOpts) ([]*release.Release, error) {
	return ListWithDriver(h.actionConfig.Releases.Driver, opts)
}

// ListWithDriver lists installed Helm charts with a specific driver
func ListWithDriver(driver driver.Driver, opts ListOpts) ([]*release.Release, error) {
	releases, err := driver.List(func(release *release.Release) bool {
		if opts.Namespace != "" && release.Namespace != opts.Namespace {
			return false
		}

		if opts.Filter == nil {
			return true
		}

		return opts.Filter(release)
	})
	if err != nil {
		return nil, err
	}

	results := filterLatestReleases(releases)

	// Sort releases by name
	sort.Slice(results, func(i, j int) bool {
		return strings.Compare(results[i].Name, results[j].Name) < 0
	})

	return results, nil
}
