package e2e

import "os"

var allowedChecks = []string{
	"clusteradmin",
	"privatenetworking",
	"kubernetesversion",
	"rbac",
	"basicauth",
}

// IsRunning returns `true` when the E2E test is running
func IsRunning() bool {
	return os.Getenv("HAVEN_E2E_MODE") == "1"
}

// IsAllowedCheck returns `true` when the check is allowed to run in E2E test mode
func IsAllowedCheck(check string) bool {
	for _, c := range allowedChecks {
		if c == check {
			return true
		}
	}

	return false
}
