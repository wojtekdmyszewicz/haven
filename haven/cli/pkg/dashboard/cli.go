// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package dashboard

import (
	"embed"
	"errors"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/exec"
	"runtime"

	"github.com/gorilla/mux"
	cli "github.com/jawher/mow.cli"
	"helm.sh/helm/v3/pkg/action"
	"k8s.io/kubectl/pkg/proxy"

	"gitlab.com/commonground/haven/haven/haven/cli/gen/pkg/api/addon/v1/addonv1connect"
	"gitlab.com/commonground/haven/haven/haven/cli/gen/pkg/api/platform/v1/platformv1connect"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/api/addon"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/api/platform"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

const serveAddress = "127.0.0.1:3000"

//go:embed static/*
var embedFiles embed.FS

// CLIConfig renders the command line function
var CLIConfig = func(config *cli.Cmd) {
	config.Action = func() {
		kubePath, _ := kubernetes.ReadK8sConfigFromPath()
		kube, err := kubernetes.NewFromConfig(kubePath)
		if err != nil {
			logging.Fatal("Error retrieving Kubernetes configuration: %s\n", err.Error())
		}

		filter := &proxy.FilterServer{
			AcceptPaths:   proxy.MakeRegexpArrayOrDie(proxy.DefaultPathAcceptRE),
			RejectPaths:   proxy.MakeRegexpArrayOrDie(proxy.DefaultPathRejectRE),
			AcceptHosts:   proxy.MakeRegexpArrayOrDie(proxy.DefaultHostAcceptRE),
			RejectMethods: proxy.MakeRegexpArrayOrDie(proxy.DefaultMethodRejectRE),
		}

		server, err := proxy.NewServer("", "/", "/static", filter, kube.RestConfig(), 0, false)
		if err != nil {
			logging.Fatal("Error creating proxy: %s\n", err.Error())
		}

		go func() {
			listener, err := server.Listen("127.0.0.1", 3001)
			if err != nil {
				logging.Fatal("Could not listen to server: %s\n", err.Error())
			}

			if err := server.ServeOnListener(listener); err != nil && !errors.Is(err, http.ErrServerClosed) {
				logging.Error("Serving: %s ", err)
			}
		}()

		staticDirectory, err := fs.Sub(embedFiles, "static")
		if err != nil {
			logging.Fatal("Could not read static subdirectory: %s\n", err.Error())
		}

		spaHandler := http.FileServer(&spaFileSystem{http.FS(staticDirectory)})
		serveURL := fmt.Sprintf("http://%s", serveAddress)

		fmt.Printf("Opening browser on %s\n", serveURL)

		err = openBrowser(serveURL)
		if err != nil {
			logging.Fatal("Could not open browser: %s\n", err.Error())
		}

		actionConfig := new(action.Configuration)

		// initialize the action config only to get the Helm storage driver
		if err := actionConfig.Init(nil, "", os.Getenv("HELM_DRIVER"), log.Printf); err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		addonSvc := addon.NewService(actionConfig.Releases.Driver, kube)
		addonPath, addonHandler := addonv1connect.NewAddonServiceHandler(addonSvc)

		platformSvc := platform.NewService(kube)
		platformPath, platformHandler := platformv1connect.NewPlatformServiceHandler(platformSvc)

		proxyHandler := httputil.NewSingleHostReverseProxy(mustParseURL("http://127.0.0.1:3001"))

		router := mux.NewRouter()
		router.PathPrefix(addonPath).Handler(addonHandler)
		router.PathPrefix(platformPath).Handler(platformHandler)
		router.PathPrefix("/apis").Handler(proxyHandler)
		router.PathPrefix("/api").Handler(proxyHandler)
		router.PathPrefix("/").Handler(spaHandler)

		err = http.ListenAndServe(serveAddress, router)
		if err != nil {
			logging.Fatal("Could not start dashboard: %s\n", err.Error())
		}
	}
}

func openBrowser(url string) error {
	switch runtime.GOOS {
	case "linux":
		return exec.Command("xdg-open", url).Start()
	case "windows":
		return exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		return exec.Command("open", url).Start()
	default:
		return fmt.Errorf("unsupported platform")
	}
}

type spaFileSystem struct {
	root http.FileSystem
}

func (fs *spaFileSystem) Open(name string) (http.File, error) {
	f, err := fs.root.Open(name)
	if os.IsNotExist(err) {
		return fs.root.Open("index.html")
	}
	return f, err
}

func mustParseURL(u string) *url.URL {
	url, err := url.Parse(u)
	if err != nil {
		log.Fatal(err)
	}

	return url
}
