// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package compliancy

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"strconv"
	"strings"
	"time"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/sonobuoy"

	batchv1 "k8s.io/api/batch/v1"
	apiv1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8syaml "sigs.k8s.io/yaml"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

type JobContext struct {
	Name               string
	ServiceAccountName string
	KubernetesVersion  string
}

func externalCNCF(ctx context.Context, config *Config) (Result, error) {
	logging.Info("Check CNCF: Running compliancy external CNCF checker. This usually takes about 1.5 hours. See: https://github.com/vmware-tanzu/sonobuoy.\n")

	flag := ""

	if config.HostPlatform == PlatformOpenShift {
		flag += " --dns-namespace=openshift-dns --dns-pod-labels=dns.operator.openshift.io/daemonset-dns=default"
	}

	err := config.Sono.Run(flag)
	if err != nil {
		return ResultNo, err
	}

	progressBar, err := outputProgress(config.Sono, config.GlobalInterval)
	if err != nil {
		return ResultNo, err
	}

	progressBar.Start()
	defer progressBar.Finish()

	defer func() {
		logging.Info("Check CNCF: Cleaning up sonobuoy deployment.\n")

		if err := config.Sono.DeleteAll(); err != nil {
			logging.Error("Check CNCF: Failed to delete sonobuoy deployment: %s\n", err)
		}
	}()

progressLoop:
	for {
		select {
		case <-ctx.Done():
			break progressLoop
		default:
		}

		if _, err := config.Sono.Retrieve(); err == nil {
			break
		}

		if err := updateProgress(progressBar, config.Sono); err != nil {
			continue
		}

		time.Sleep(config.GlobalInterval)
	}

	results, err := config.Sono.Results()
	if err != nil {
		return ResultNo, err
	}

	if strings.Contains(results, "Status") &&
		!strings.Contains(results, "Status: failed") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func updateProgress(pb *ProgressBar, sono sonobuoy.Client) error {
	status, err := sono.Status()
	if err != nil {
		return err
	}

	var e2ePlugin sonobuoy.Plugin
	for _, plugin := range status.Plugins {
		if plugin.Plugin == "e2e" {
			e2ePlugin = plugin
		}
	}

	if e2ePlugin.Progress == nil {
		return nil
	}

	pb.completed.Store(e2ePlugin.Progress.Completed)

	return nil
}

// outputProgress shows the current progress of sonobuoy
func outputProgress(sono sonobuoy.Client, interval time.Duration) (*ProgressBar, error) {
	var e2ePlugin sonobuoy.Plugin

	for {
		sonoStatus, err := sono.Status()
		if err != nil {
			//it takes a few moments to start up sonobuoy
			time.Sleep(interval)
			continue
		} else {
			for _, plugin := range sonoStatus.Plugins {
				if plugin.Plugin == "e2e" {
					e2ePlugin = plugin
					break
				}
			}

			if e2ePlugin.Progress != nil {
				break
			} else {
				time.Sleep(interval)
				continue
			}
		}
	}

	return NewProgressBar(e2ePlugin.Progress.Total, e2ePlugin.Progress.Completed), nil
}

// generateName generates a random name for a job (as a variable so that tests can override it)
var generateName = func(prefix string) string {
	return fmt.Sprintf("%s-%s", prefix, strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5]))
}

func cleanupAdminServiceAccount(ctx context.Context, config *Config, name string) error {
	if err := config.Kube.CoreV1().
		ServiceAccounts(config.Namespace).
		Delete(ctx, name, metav1.DeleteOptions{}); err != nil {
		return err
	}

	if err := config.Kube.RbacV1().
		ClusterRoleBindings().
		Delete(ctx, name, metav1.DeleteOptions{}); err != nil {
		return err
	}

	return nil
}

func createAdminServiceAccount(ctx context.Context, config *Config, name string) error {
	var err error

	if _, err = config.Kube.CoreV1().
		ServiceAccounts(config.Namespace).
		Create(ctx, &apiv1.ServiceAccount{
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: config.Namespace,
			},
		}, metav1.CreateOptions{}); err != nil {
		return err
	}

	if _, err := config.Kube.RbacV1().
		ClusterRoleBindings().
		Create(ctx, &rbacv1.ClusterRoleBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			RoleRef: rbacv1.RoleRef{
				APIGroup: "rbac.authorization.k8s.io",
				Kind:     "ClusterRole",
				Name:     "cluster-admin",
			},
			Subjects: []rbacv1.Subject{
				{
					Kind:      "ServiceAccount",
					Name:      name,
					Namespace: config.Namespace,
				},
			},
		}, metav1.CreateOptions{}); err != nil {
		return err
	}

	return nil
}

func suggestionExternalKubescape(ctx context.Context, config *Config) (Result, error) {
	logging.Info("Check Kubescape: Running suggested external Kubescape checker on the worker nodes. This should not take long. See: https://github.com/kubescape/kubescape.\n")

	saName := generateName("kubescape-sa")

	if err := createAdminServiceAccount(ctx, config, saName); err != nil {
		return ResultNo, err
	}

	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), config.ShutdownTimeout)
		defer cancel()

		if err := cleanupAdminServiceAccount(ctx, config, saName); err != nil {
			logging.Error("Check Kubescape: Failed to cleanup service account: %s\n", err)
		}
	}()

	templateCtx := JobContext{
		ServiceAccountName: saName,
		Name:               generateName("kubescape"),
	}

	var job *batchv1.Job

	if err := readJobFile("static/kubescape/job.yaml.tpl", &job, templateCtx); err != nil {
		return ResultNo, err
	}

	output, err := runExternalCheck(ctx, config, templateCtx.Name, job)
	if err != nil {
		return ResultNo, err
	}

	logging.Output(output, "Kubescape")

	if strings.Contains(output, "Failed: 0, ") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

// suggestionExternalCIS is a 'Suggested Check' meaning it's not counted towards Haven Compliancy.
// Automating the manual CIS Benchmark is done via kube-bench. There are many ways to run kube-bench.
// - Most importantly it should work on any Haven cluster.
// - Tested successfully on Kops/OpenStack, GKE, AKS and EKS.
func suggestionExternalCIS(ctx context.Context, config *Config) (Result, error) {
	logging.Info("Check CIS: Running suggested external CIS checker on the worker nodes. This should not take long. See: https://github.com/aquasecurity/kube-bench.\n")

	var (
		err error
		job *batchv1.Job
	)

	saName := generateName("cis-sa")

	if err := createAdminServiceAccount(ctx, config, saName); err != nil {
		return ResultNo, err
	}

	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), config.ShutdownTimeout)
		defer cancel()

		if err := cleanupAdminServiceAccount(ctx, config, saName); err != nil {
			logging.Error("Check Kubescape: Failed to cleanup service account: %s\n", err)
		}
	}()

	templateCtx := JobContext{
		Name:               generateName("kube-bench"),
		ServiceAccountName: saName,
		KubernetesVersion:  fmt.Sprintf("%d.%d", config.KubeServer.Major(), config.KubeServer.Minor()),
	}

	switch config.HostPlatform {
	case PlatformGKE:
		err = readJobFile(fmt.Sprintf("static/kube-bench/job-%s.yaml.tpl", GkeKey), &job, templateCtx)
	case PlatformEKS:
		err = readJobFile(fmt.Sprintf("static/kube-bench/job-%s.yaml.tpl", EksKey), &job, templateCtx)
	case PlatformAKS:
		err = readJobFile(fmt.Sprintf("static/kube-bench/job-%s.yaml.tpl", AksKey), &job, templateCtx)
	default:
		err = readJobFile("static/kube-bench/job-generic.yaml.tpl", &job, templateCtx)
	}

	if err != nil {
		return ResultNo, err
	}

	output, err := runExternalCheck(ctx, config, templateCtx.Name, job)
	if err != nil {
		return ResultNo, err
	}

	logging.Output(output, "CIS")

	if strings.Contains(output, "0 checks FAIL") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func runExternalCheck(ctx context.Context, config *Config, name string, job *batchv1.Job) (string, error) {
	if _, err := config.Kube.BatchV1().
		Jobs(config.Namespace).
		Create(ctx, job, metav1.CreateOptions{}); err != nil {
		return "", fmt.Errorf("failed to create job: %w", err)
	}

	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), config.ShutdownTimeout)
		defer cancel()

		deletePolicy := metav1.DeletePropagationForeground
		if err := config.Kube.BatchV1().
			Jobs(config.Namespace).
			Delete(ctx, name, metav1.DeleteOptions{PropagationPolicy: &deletePolicy}); err != nil {
			logging.Error("Delete Job '%s': %s", name, err)
		}
	}()

	waitCtx, cancel := context.WithTimeout(ctx, time.Minute*5)
	defer cancel()

	pod, err := waitForPod(waitCtx, config, fmt.Sprintf("job-name=%s", name))
	if err != nil {
		return "", fmt.Errorf("failed to wait for pod: %w", err)
	}

	logs := config.Kube.CoreV1().
		Pods(config.Namespace).
		GetLogs(pod.ObjectMeta.Name, &apiv1.PodLogOptions{})

	return kubernetes.ReadLogs(ctx, logs)
}

func waitForPod(ctx context.Context, config *Config, labelSelector string) (*apiv1.Pod, error) {
	ticker := time.NewTicker(config.GlobalInterval)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case <-ticker.C:
			pods, err := config.Kube.CoreV1().
				Pods(config.Namespace).
				List(ctx, metav1.ListOptions{
					LabelSelector: labelSelector,
				})
			if err != nil {
				return nil, fmt.Errorf("failed to list pods: %w", err)
			}

			if len(pods.Items) != 1 {
				continue
			}

			if pods.Items[0].Status.Phase == "Succeeded" {
				return &pods.Items[0], nil
			}
		}
	}
}

func readJobFile(filename string, output interface{}, values interface{}) error {
	file, err := embedStatic.Open(filename)
	if err != nil {
		return err
	}

	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return err
	}

	if strings.HasSuffix(filename, ".tpl") {
		buf := bytes.Buffer{}

		tpl := template.Must(template.New(filename).Parse(string(content)))
		if err := tpl.Execute(&buf, values); err != nil {
			return err
		}

		content = buf.Bytes()
	}

	jsonContent, err := k8syaml.YAMLToJSON(content)
	if err != nil {
		return err
	}

	return json.Unmarshal(jsonContent, &output)
}
