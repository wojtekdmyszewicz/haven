// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"strings"

	apiv1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func deploymentLogAggregation(ctx context.Context, config *Config) (Result, error) {
	pods, err := config.Kube.CoreV1().
		Pods(apiv1.NamespaceAll).
		List(ctx, v1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, l := range whitelist.Loggers {
			if strings.Contains(p.Name, l.Name) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

func deploymentMetricsServer(ctx context.Context, config *Config) (Result, error) {
	pods, err := config.Kube.CoreV1().
		Pods(apiv1.NamespaceAll).
		List(ctx, v1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, m := range whitelist.Metrics {
			if strings.Contains(p.Name, m.Name) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

func deploymentHttpsCerts(ctx context.Context, config *Config) (Result, error) {
	pods, err := config.Kube.CoreV1().
		Pods(apiv1.NamespaceAll).
		List(ctx, v1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	// Detection via Pods.
	for _, pod := range pods.Items {
		for _, p := range whitelist.CertPods {
			if strings.Contains(pod.Name, p.Name) {
				return ResultYes, nil
			}
		}
	}

	crds, err := config.Kube.ApiextensionsV1().
		CustomResourceDefinitions().
		List(ctx, v1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	// Detection via CRD's.
	for _, crd := range crds.Items {
		for _, c := range whitelist.CertCrds {
			if strings.Contains(crd.Spec.Names.Kind, c.Name) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}
