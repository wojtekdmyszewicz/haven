// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package compliancy

// Category a compliancy check is part of
type Category string

const (
	CategoryFundamental    Category = "Fundamental"
	CategoryInfrastructure Category = "Infrastructure"
	CategoryCluster        Category = "Cluster"
	CategoryExternal       Category = "External"
	CategoryDeployment     Category = "Deployment"
)
