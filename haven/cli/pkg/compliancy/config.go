// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package compliancy

import (
	"time"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/haven"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/sonobuoy"
)

// Config holds the configuration of the checker
type Config struct {
	HavenReleases   HavenReleases
	KubeServer      *semver.Version
	KubeLatest      string
	Kube            *kubernetes.Clientset
	Sono            sonobuoy.Client
	CrdClient       haven.V1Alpha1
	ClusterID       string
	Namespace       string
	CNCFConfigured  bool
	RunKSChecks     bool
	RunCISChecks    bool
	HostPlatform    string
	CisCheckAppName string
	GlobalInterval  time.Duration
	ShutdownTimeout time.Duration
}
