// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/Masterminds/semver/v3"
	"github.com/gookit/color"
	"github.com/olekukonko/tablewriter"
	v1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/e2e"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/haven"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/v1alpha1"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/sonobuoy"
)

var (
	awsMasterPattern           = regexp.MustCompile(`https:\/\/.*.eks.amazonaws.com`)
	azureMasterPattern         = regexp.MustCompile(`https:\/\/.*.azmk8s.io`)
	googleMasterVersionPattern = regexp.MustCompile(`gke`)
	k3sMasterVersionPattern    = regexp.MustCompile(`k3s`)
)

// Checker runs the compliancy checks and returns a list of results
type Checker struct {
	config   *Config
	platform *v1alpha1.CompliancyPlatform
}

type CheckType int

const (
	CheckTypeCompliancy CheckType = iota + 1
	CheckTypeSuggested
)

type checkResult struct {
	Idx    int
	Type   CheckType
	Result Result
	Err    error
}

const (
	AksImageLabel   string = "kubernetes.azure.com/node-image-version"
	AksRole         string = "kubernetes.azure.com/role"
	AksAgent        string = "agent"
	EksKey          string = "eks"
	GkeKey          string = "gke"
	AksKey          string = "aks"
	NamespacePrefix string = "haven-check"

	PlatformUnknown   string = "Unknown"
	PlatformEKS       string = "Amazon AWS EKS"
	PlatformAKS       string = "Microsoft Azure AKS"
	PlatformGKE       string = "Google Cloud Platform GKE"
	PlatformK3S       string = "K3s"
	PlatformOpenShift string = "Red Hat OpenShift"
	PlatformARO       string = "Azure Red Hat OpenShift (managed service)"
)

type HavenRelease struct {
	Date    time.Time `json:"date"`
	Version string    `json:"version"`
}

type HavenReleases struct {
	Releases []HavenRelease `json:"releases"`
}

func (h HavenReleases) latest() HavenRelease {
	var hr HavenRelease

	lv, _ := semver.NewVersion("v0.0.0")

	for _, item := range h.Releases {
		cv, err := semver.NewVersion(item.Version)
		if err == nil {
			if cv.GreaterThan(lv) {
				lv = cv
				hr = item
			}
		}
	}

	return hr
}

func (h HavenReleases) findByVersion(version string) HavenRelease {
	var hr HavenRelease

	for _, item := range h.Releases {
		if item.Version == version {
			hr = item
		}
	}

	return hr
}

// NewChecker returns a new Checker
func NewChecker(cncfConfigured, runCISChecks, runKSChecks bool) (*Checker, error) {
	kubePath, _ := kubernetes.ReadK8sConfigFromPath()
	kube, err := kubernetes.NewFromConfig(kubePath)
	if err != nil {
		return nil, err
	}

	crdClient, err := haven.NewCrdConfig(kube)
	if err != nil {
		return nil, err
	}

	kubeServerSrc, err := kube.Discovery().ServerVersion()
	if err != nil {
		return nil, err
	}

	kubeServer, err := semver.NewVersion(kubeServerSrc.String())
	if err != nil {
		return nil, fmt.Errorf("Kubernetes server version: %s, Error: %s", kubeServerSrc.String(), err)
	}

	platform, err := BestEffortPlatformCheck(kube)
	if err != nil {
		return nil, err
	}

	havenReleases, err := FetchHavenReleases()
	if err != nil {
		return nil, err
	}

	kubeLatest, err := LatestKubernetesVersion()
	if err != nil {
		return nil, err
	}

	ks, err := kube.CoreV1().Namespaces().Get(context.TODO(), "kube-system", metav1.GetOptions{})
	if err != nil {
		logging.Fatal("Could not retrieve namespace kube-system: %s", err)
	}
	clusterID := string(ks.UID)

	ns := fmt.Sprintf("%s-%d", NamespacePrefix, time.Now().Unix())

	sono := sonobuoy.NewSonobuoyClient(sonobuoy.DefaultMode)

	return &Checker{
		config: &Config{
			HavenReleases:   havenReleases,
			KubeServer:      kubeServer,
			KubeLatest:      kubeLatest,
			ClusterID:       clusterID,
			CNCFConfigured:  cncfConfigured,
			RunKSChecks:     runKSChecks,
			RunCISChecks:    runCISChecks,
			HostPlatform:    platform.DeterminedPlatform,
			Namespace:       ns,
			Kube:            kube,
			CrdClient:       crdClient,
			Sono:            sono,
			GlobalInterval:  2500 * time.Millisecond,
			ShutdownTimeout: 10 * time.Second,
		},
		platform: platform,
	}, nil
}

func FetchHavenReleases() (HavenReleases, error) {
	var havenReleases HavenReleases

	res, err := http.Get("https://haven.commonground.nl/releases.json")
	if err != nil {
		return havenReleases, fmt.Errorf("Could not fetch latest Haven releases: '%s'", err.Error())
	}
	defer res.Body.Close()

	content, err := io.ReadAll(res.Body)
	if err != nil {
		return havenReleases, fmt.Errorf("Could not read latest Haven releases: '%s'", err.Error())
	}

	err = json.Unmarshal([]byte(content), &havenReleases)
	if err != nil {
		return havenReleases, fmt.Errorf("Could not parse latest Haven releases: '%s'", err.Error())
	}

	return havenReleases, nil
}

func LatestKubernetesVersion() (string, error) {
	res, err := http.Get("https://storage.googleapis.com/kubernetes-release/release/stable.txt")
	if err != nil {
		return "", fmt.Errorf("Could not fetch latest Kubernetes release: '%s'", err.Error())
	}
	defer res.Body.Close()

	content, err := io.ReadAll(res.Body)
	if err != nil {
		return "", fmt.Errorf("Could not read latest Kubernetes release: '%s'", err.Error())
	}

	return string(content), nil
}

func BestEffortPlatformCheck(kube *kubernetes.Clientset) (*v1alpha1.CompliancyPlatform, error) {
	platform := &v1alpha1.CompliancyPlatform{DeterminedPlatform: PlatformUnknown}

	if awsMasterPattern.MatchString(kube.Host()) {
		platform.DeterminedPlatform = PlatformEKS
	}

	if azureMasterPattern.MatchString(kube.Host()) {
		platform.DeterminedPlatform = PlatformAKS
	}

	serverVersion, err := kube.Discovery().ServerVersion()
	if err != nil {
		return nil, fmt.Errorf("error determining server version: %s", err)
	}

	if googleMasterVersionPattern.MatchString(serverVersion.String()) {
		platform.DeterminedPlatform = PlatformGKE
	}

	if k3sMasterVersionPattern.MatchString(serverVersion.String()) {
		platform.DeterminedPlatform = PlatformK3S
	}

	ctx := context.TODO()

	namespaces, err := kube.CoreV1().Namespaces().List(ctx, metav1.ListOptions{})
	if err == nil {
		// Loops over all namespaces and sets plaform to PlatformOpenShift if
		// openshift-apiserver namespace is found.
		for _, ns := range namespaces.Items {
			if ns.Name == "openshift-apiserver" {
				platform.DeterminedPlatform = PlatformOpenShift
			}
		}
		// Loops over all namespaces again and overrides platform to PlatformARO if
		// openshift-azure-logging namespace is found.
		for _, ns := range namespaces.Items {
			if ns.Name == "openshift-azure-logging" {
				platform.DeterminedPlatform = PlatformARO
			}
		}
	}

	// If the platform is still not known we try to get it by viewing the labels attached to a node
	// this is used primarily when there is a platform behind a proxy such as rancher or pinniped.
	if platform.DeterminedPlatform == PlatformUnknown {
		nodes, err := kube.CoreV1().Nodes().List(ctx, metav1.ListOptions{})
		if err != nil {
			return nil, err
		}

		randomNodeNumber := rand.Intn(len(nodes.Items))

		node := nodes.Items[randomNodeNumber]

		determinedPlatform, err := BestEffortLabelCheck(node.Labels, platform)
		if err != nil {
			return nil, err
		}

		if determinedPlatform != PlatformUnknown {
			platform.DeterminedPlatform = determinedPlatform
			platform.Proxy = true
		}
	}

	return platform, nil
}

func BestEffortLabelCheck(labels map[string]string, platform *v1alpha1.CompliancyPlatform) (string, error) {
	determinedPlatform := PlatformUnknown

	for key, value := range labels {
		if key == AksRole {
			if value == AksAgent {
				determinedPlatform = PlatformAKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}
		if key == AksImageLabel {
			match := AksBasedImage(value)
			if match {
				determinedPlatform = PlatformAKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}

		if strings.Contains(key, EksKey) {
			match := EksBasedLabel(key)
			if match {
				determinedPlatform = PlatformEKS
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}

		if strings.Contains(key, GkeKey) {
			match := GkeBasedLabel(key)
			if match {
				determinedPlatform = PlatformGKE
				platform.DeterminingLabel = fmt.Sprintf("%s=%s", key, value)
				break
			}
		}
	}

	return determinedPlatform, nil
}

func AksBasedImage(value string) bool {
	regex := "^AKSUbuntu[a-zA-Z0-9\\-\\.]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

func EksBasedLabel(value string) bool {
	regex := "^eks\\.amazonaws\\.com+\\/[a-zA-Z0-9\\-]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

func GkeBasedLabel(value string) bool {
	regex := "^cloud\\.google\\.com\\/gke-[a-zA-Z0-9\\-]+$"
	re := regexp.MustCompile(regex)
	match := re.MatchString(value)

	return match
}

func shouldRunCheck(name string) bool {
	if e2e.IsRunning() {
		if !e2e.IsAllowedCheck(name) {
			return false
		}
	}

	return true
}

func waitForDefaultServiceAccount(ctx context.Context, clientset *kubernetes.Clientset, namespace string) error {
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			_, err := clientset.CoreV1().ServiceAccounts(namespace).Get(ctx, "default", metav1.GetOptions{})
			if err != nil {
				if k8serrors.IsNotFound(err) {
					logging.Info("Waiting for service account to be created")
					time.Sleep(100 * time.Millisecond)
					continue
				}

				return err
			}
		}

		return nil
	}
}

func (c *Checker) prepare(ctx context.Context) error {
	_, err := c.config.Kube.CoreV1().Namespaces().Create(ctx, &v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: c.config.Namespace}}, metav1.CreateOptions{})
	if err != nil {
		return fmt.Errorf("failed to create namespace: %w", err)
	}

	waitCtx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	if err = waitForDefaultServiceAccount(waitCtx, c.config.Kube, c.config.Namespace); err != nil {
		return fmt.Errorf("failed to wait for default service account: %w", err)
	}

	return nil
}

// Run the checker and return a slice of results
func (c *Checker) Run(ctx context.Context) error {
	var wg sync.WaitGroup

	results := make(chan checkResult)

	if e2e.IsRunning() {
		logging.Warning("Running in E2E test mode, skipping most checks")
	}

	if err := c.prepare(ctx); err != nil {
		return err
	}

	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), c.config.ShutdownTimeout)
		defer cancel()

		if err := c.config.Kube.CoreV1().Namespaces().Delete(ctx, c.config.Namespace, metav1.DeleteOptions{}); err != nil {
			logging.Error("Could not cleanup namespace: '%s'", err.Error())
		}
	}()

	for i := range checks.CompliancyChecks {
		if !shouldRunCheck(checks.CompliancyChecks[i].Name) {
			continue
		}

		if !c.config.CNCFConfigured && checks.CompliancyChecks[i].Name == "cncf" {
			checks.CompliancyChecks[i].Result = ResultSkipped
			continue
		}

		wg.Add(1)

		go func(idx int, check Check) {
			defer wg.Done()

			result, err := check.Exec(ctx, c.config)
			results <- checkResult{idx, CheckTypeCompliancy, result, err}
		}(i, checks.CompliancyChecks[i])
	}

	for i := range checks.SuggestedChecks {
		if !shouldRunCheck(checks.SuggestedChecks[i].Name) {
			continue
		}

		if !c.config.RunCISChecks && checks.SuggestedChecks[i].Name == "cis" {
			checks.SuggestedChecks[i].Result = ResultSkipped
			continue
		}

		if !c.config.RunKSChecks && checks.SuggestedChecks[i].Name == "kubescape" {
			checks.SuggestedChecks[i].Result = ResultSkipped
			continue
		}

		wg.Add(1)

		go func(idx int, check Check) {
			defer wg.Done()

			result, err := check.Exec(ctx, c.config)
			results <- checkResult{idx, CheckTypeSuggested, result, err}
		}(i, checks.SuggestedChecks[i])
	}

	// Close 'results' channel if all checks are done
	go func() {
		wg.Wait()
		close(results)
	}()

	for result := range results {
		var check *Check

		switch result.Type {
		case CheckTypeCompliancy:
			check = &checks.CompliancyChecks[result.Idx]

			if result.Err != nil && !errors.Is(result.Err, context.Canceled) {
				logging.Error("Compliancy Check '%s' error: \n    - '%s'\n\n", check.Label, result.Err.Error())
			}
		case CheckTypeSuggested:
			check = &checks.SuggestedChecks[result.Idx]

			if result.Err != nil && !errors.Is(result.Err, context.Canceled) {
				logging.Error("Suggested Check '%s' error: \n    - '%s'\n\n", check.Label, result.Err.Error())
			}
		}

		check.Result = result.Result
	}

	return nil
}

// PrintResults prints the table of checks with the corresponding results
func (c *Checker) PrintResults(checks []Check, compliancyChecks bool) {
	tableOut := &strings.Builder{}
	table := tablewriter.NewWriter(tableOut)

	table.SetHeader([]string{"Category", "Name", "Passed"})
	table.SetAutoWrapText(false)

	totalChecks := 0
	unknownChecks := 0
	skippedChecks := 0
	failedChecks := 0
	passedChecks := 0

	for _, check := range checks {
		totalChecks++

		switch check.Result {
		case ResultUnknown:
			unknownChecks++
		case ResultSkipped:
			skippedChecks++
		case ResultNo:
			failedChecks++
		case ResultYes:
			passedChecks++
		}

		table.Append([]string{string(check.Category), check.Label, check.Result.String()})
	}

	table.Render()

	prefix := "Compliancy checks results:\n"
	if compliancyChecks {
		compliancy := fmt.Sprintf("Results: %d out of %d checks passed, %d checks skipped, %d checks unknown.", passedChecks, totalChecks, skippedChecks, unknownChecks)

		if e2e.IsRunning() {
			logging.Info(color.Green.Sprintf("%s Running in E2E mode: Haven Compliancy not checked.\n", compliancy))
		} else if passedChecks == totalChecks {
			logging.Info(color.Green.Sprintf("%s This is a Haven Compliant cluster.\n", compliancy))

			output.HavenCompliant = true
		} else if failedChecks > 0 {
			logging.Error(color.Red.Sprintf("%s This is NOT a Haven Compliant cluster.\n", compliancy))
		} else {
			logging.Warning(color.Yellow.Sprintf("%s This COULD be a Haven Compliant cluster.\n", compliancy))
		}

		output.CompliancyChecks.Summary.Total = totalChecks
		output.CompliancyChecks.Summary.Passed = passedChecks
		output.CompliancyChecks.Summary.Failed = failedChecks
		output.CompliancyChecks.Summary.Skipped = skippedChecks
		output.CompliancyChecks.Summary.Unknown = unknownChecks
	} else {
		prefix = "Suggested checks results:\n"
	}

	logging.Info(fmt.Sprintf("%s\n%s\n", prefix, tableOut.String()))
}
