package compliancy

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

// telemetryCall calls the standaardenregister.nl endpoint to +1 to enable very basic insights into Haven cli usage.
func telemetryCall(clusterID string, compliant bool) error {
	url := "https://standaardenregister.nl/api/v1/telemetry/"
	data := map[string]interface{}{
		"standard":   1,
		"compliant":  compliant,
		"identifier": clusterID,
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 2 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("Telemetry call failed with status code: %d", resp.StatusCode)
	}

	return nil
}
