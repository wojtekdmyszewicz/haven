package compliancy

// Whitelist has been automatically generated based on the static file
type Whitelist struct {
	Loggers []struct {
		Name    string `yaml:"name"`
		Website string `yaml:"website"`
		Info    string `yaml:"info,omitempty"`
	} `yaml:"loggers"`
	Metrics []struct {
		Name    string `yaml:"name"`
		Website string `yaml:"website"`
	} `yaml:"metrics"`
	CertPods []struct {
		Name    string `yaml:"name"`
		Website string `yaml:"website"`
		Info    string `yaml:"info,omitempty"`
	} `yaml:"certpods"`
	CertCrds []struct {
		Name    string `yaml:"name"`
		Info    string `yaml:"info"`
		Website string `yaml:"website"`
	} `yaml:"certcrds"`
}
