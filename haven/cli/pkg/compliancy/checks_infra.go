// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"errors"
	"strings"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	apiv1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func infraMultiMaster(ctx context.Context, config *Config) (Result, error) {
	// Amazon EKS, Azure AKS and Google cloud GKE control plane does not appear in the nodes list and
	// is high available by default
	if config.HostPlatform == PlatformEKS {
		return ResultYes, nil
	}

	if config.HostPlatform == PlatformAKS {
		return ResultYes, nil
	}

	if config.HostPlatform == PlatformGKE {
		return ResultYes, nil
	}

	nodes, err := config.Kube.CoreV1().Nodes().List(ctx, v1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	masterCount := 0
	for _, n := range nodes.Items {
		if n.Labels["kubernetes.io/role"] == "master" {
			masterCount++
			continue
		}

		if _, exists := n.Labels["node-role.kubernetes.io/control-plane"]; exists {
			masterCount++
			continue
		}

		if _, exists := n.Labels["node-role.kubernetes.io/controlplane"]; exists {
			masterCount++
			continue
		}

		if _, exists := n.Labels["node-role.kubernetes.io/master"]; exists {
			masterCount++
			continue
		}

		if _, exists := n.Labels["node.kubernetes.io/master"]; exists {
			masterCount++
			continue
		}
	}

	if masterCount >= 3 {
		return ResultYes, nil
	}

	if masterCount == 0 {
		return ResultUnknown, nil
	}

	return ResultNo, nil
}

func infraMultiWorker(ctx context.Context, config *Config) (Result, error) {
	nodes, err := config.Kube.CoreV1().Nodes().List(ctx, v1.ListOptions{})

	if err != nil {
		return ResultNo, err
	}

	workerCount := 0
	for _, n := range nodes.Items {
		if "master" != n.Labels["kubernetes.io/role"] {
			workerCount++
		}

		if workerCount >= 3 {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}

func infraSecuredNodes(ctx context.Context, config *Config) (Result, error) {
	// OpenShift uses Red Hat Enterprise Linux CoreOS, an immutable operating system
	// especially for container platforms. As a platform feature, it always has
	// SELinux enabled. On managed services like ARO, this can be manually
	// verified through `oc debug node/$master`, there is no SSH access.
	if config.HostPlatform == PlatformOpenShift {
		return ResultYes, nil
	}

	// Talos Linux. There is practically no userland in Talos (ie, no ssh/cat/busybox/whatever)
	// so the `nsenter` will enter a vacuum and cat commands below will fail and cause false negatives
	// so we test for Talos first
	nodes, err := config.Kube.CoreV1().Nodes().List(ctx, v1.ListOptions{})
	if err != nil {
		return ResultUnknown, err
	}

	if len(nodes.Items) == 0 {
		return ResultUnknown, errors.New("Received empty node list, cannot check OS image")
	}

	if strings.HasPrefix(nodes.Items[0].Status.NodeInfo.OSImage, "Talos ") && strings.Contains(nodes.Items[0].Status.NodeInfo.KernelVersion, "-talos") {
		return ResultYes, nil
	}

	logging.Info("Check node security: Using fallback method with control-plane pod for 10-40 seconds.\n")

	cmdOpts := []kubernetes.RunCommandOption{
		kubernetes.WithNamespace(config.Namespace),
		kubernetes.WithTickInterval(config.GlobalInterval),
		kubernetes.WithShutdownTimeout(config.ShutdownTimeout),
	}

	// AppArmor.
	out, err := kubernetes.ExecCmd(ctx, config.Kube, "cat /sys/module/apparmor/parameters/enabled", cmdOpts...)
	if err == nil {
		if strings.Contains(out, "Y") {
			return ResultYes, nil
		}
	}

	// SELinux. Existence of the target file means SELinux is enabled with at least permissive mode.
	_, err = kubernetes.ExecCmd(ctx, config.Kube, "cat /sys/fs/selinux/enforce", cmdOpts...)
	if err == nil {
		return ResultYes, nil
	}

	// Grsecurity. We rely on the node retrieved earlier to find out if we're running on Talos
	if strings.Contains(nodes.Items[0].Status.NodeInfo.KernelVersion, "grsec") {
		return ResultYes, nil
	}

	// LKRG.
	out, err = kubernetes.ExecCmd(ctx, config.Kube, "lsmod", cmdOpts...)
	if err != nil {
		return ResultUnknown, err
	}

	if strings.Contains(out, "lkrg") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func infraPrivateTopology(ctx context.Context, config *Config) (Result, error) {
	nodes, err := config.Kube.CoreV1().Nodes().List(ctx, v1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, n := range nodes.Items {
		for _, a := range n.Status.Addresses {
			if "ExternalIP" == a.Type && "" != a.Address {
				return ResultNo, nil
			}
		}
	}

	return ResultYes, nil
}

func infraMultiAZ(ctx context.Context, config *Config) (Result, error) {
	nodes, err := config.Kube.CoreV1().Nodes().List(ctx, v1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	zone := ""
	zoneCount := 0

	for _, n := range nodes.Items {
		curZone := n.Labels[apiv1.LabelTopologyZone]

		if zone != curZone {
			zone = curZone
			zoneCount++
		}

		if zoneCount > 1 {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}
