package compliancy

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/v1alpha1"
	"testing"
)

func TestBestEffortPlatformCheckAks(t *testing.T) {
	t.Run("AksByNodeImage", func(t *testing.T) {
		v1Platform := v1alpha1.CompliancyPlatform{}
		expected := PlatformAKS
		labels := make(map[string]string)
		labels[AksImageLabel] = "AKSUbuntu-1804gen2containerd-2021.09.03"

		platform, err := BestEffortLabelCheck(labels, &v1Platform)
		assert.Nil(t, err)
		assert.Equal(t, expected, platform)
		assert.Contains(t, v1Platform.DeterminingLabel, labels[AksImageLabel])
	})

	t.Run("LabelContainsAks", func(t *testing.T) {
		v1Platform := v1alpha1.CompliancyPlatform{}
		expected := PlatformUnknown
		labels := make(map[string]string)
		labels[AksImageLabel] = "LAKSImage2020.21"

		platform, err := BestEffortLabelCheck(labels, &v1Platform)
		assert.Nil(t, err)
		assert.Equal(t, expected, platform)
		assert.Equal(t, v1Platform.DeterminingLabel, "")
	})

	t.Run("AksByAgentRole", func(t *testing.T) {
		v1Platform := v1alpha1.CompliancyPlatform{}
		expected := PlatformAKS
		labels := make(map[string]string)
		labels[AksRole] = AksAgent

		platform, err := BestEffortLabelCheck(labels, &v1Platform)
		assert.Nil(t, err)
		assert.Equal(t, expected, platform)
		assert.Contains(t, v1Platform.DeterminingLabel, labels[AksRole])
	})

	t.Run("AksByRoleWithNode", func(t *testing.T) {
		v1Platform := v1alpha1.CompliancyPlatform{}
		expected := PlatformUnknown
		labels := make(map[string]string)
		labels[AksRole] = "node"

		platform, err := BestEffortLabelCheck(labels, &v1Platform)
		assert.Nil(t, err)
		assert.Equal(t, expected, platform)
		assert.Equal(t, v1Platform.DeterminingLabel, "")
	})
}

func TestBestEffortPlatformCheckEks(t *testing.T) {
	t.Run("EksByLabelKey", func(t *testing.T) {
		v1Platform := v1alpha1.CompliancyPlatform{}
		expected := PlatformEKS
		labels := map[string]string{
			"eks.amazonaws.com/capacityType":    "ON_DEMAND",
			"eks.amazonaws.com/nodegroup-image": "ami-0c44134b965f833c0",
		}

		for key, value := range labels {
			label := map[string]string{key: value}
			platform, err := BestEffortLabelCheck(label, &v1Platform)
			assert.Nil(t, err)
			assert.Equal(t, expected, platform)
			assert.Contains(t, v1Platform.DeterminingLabel, key)
		}
	})

	t.Run("EksByLabelWithEks", func(t *testing.T) {
		v1Platform := v1alpha1.CompliancyPlatform{}
		expected := PlatformUnknown
		labels := make(map[string]string)
		labels["some-eks-label"] = "custom"

		platform, err := BestEffortLabelCheck(labels, &v1Platform)
		assert.Nil(t, err)
		assert.Equal(t, expected, platform)
		assert.Equal(t, v1Platform.DeterminingLabel, "")
	})
}

func TestBestEffortPlatformCheckGKE(t *testing.T) {
	t.Run("GkeByLabelKey", func(t *testing.T) {
		v1Platform := v1alpha1.CompliancyPlatform{}
		expected := PlatformGKE
		labels := map[string]string{
			"cloud.google.com/gke-boot-disk":         "pd-standard",
			"cloud.google.com/gke-container-runtime": "containerd",
			"cloud.google.com/gke-netd-ready":        "true",
			"cloud.google.com/gke-nodepool":          "management-primary-node-pool",
			"cloud.google.com/gke-os-distribution":   "cos",
		}

		for key, value := range labels {
			label := map[string]string{key: value}
			platform, err := BestEffortLabelCheck(label, &v1Platform)
			assert.Nil(t, err)
			assert.Equal(t, expected, platform)
			assert.Contains(t, v1Platform.DeterminingLabel, key)
		}
	})

	t.Run("GkeByLabelWithGke", func(t *testing.T) {
		v1Platform := v1alpha1.CompliancyPlatform{}
		expected := PlatformUnknown
		labels := make(map[string]string)
		labels["some-gke-label"] = "custom"

		platform, err := BestEffortLabelCheck(labels, &v1Platform)
		assert.Nil(t, err)
		assert.Equal(t, expected, platform)
		assert.Equal(t, v1Platform.DeterminingLabel, "")
	})
}
