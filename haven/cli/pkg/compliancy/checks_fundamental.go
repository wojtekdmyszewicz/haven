// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	authorizationv1 "k8s.io/api/authorization/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

/*
	Fundamental checks in case of failure should immediately stop further
	processing as they are critical to properly checking for Haven Compliancy.
*/

func fundamentalHavenVersion(ctx context.Context, config *Config) (Result, error) {
	// Latest major or max 3 months to get there with a warning,
	// but never more than 1 major version behind.
	//
	// This works properly together with our release schedule as published on the website.

	if Version == "undefined" {
		return ResultSkipped, nil
	}

	latestVersion, err := semver.NewVersion(config.HavenReleases.latest().Version)
	if err != nil {
		return ResultNo, fmt.Errorf("Haven latest version formatting issue: %s, Error: %s", config.HavenReleases.latest().Version, err.Error())
	}

	currentVersion, err := semver.NewVersion(Version)
	if err != nil {
		return ResultNo, fmt.Errorf("Haven current version formatting issue: %s, Error: %s", Version, err.Error())
	}

	latestMajor := fmt.Sprintf("v%d.0.0", latestVersion.Major())
	daysBetweenNowLatestMajor := int(time.Since(config.HavenReleases.findByVersion(latestMajor).Date).Hours() / 24)
	daysBetweenNowLatest := int(time.Since(config.HavenReleases.latest().Date).Hours() / 24)

	// "OK"
	if currentVersion.Equal(latestVersion) || currentVersion.GreaterThan(latestVersion) {
		return ResultYes, nil
	}

	// "FAIL" (1)
	if latestVersion.Major()-currentVersion.Major() > 1 {
		return ResultNo, fmt.Errorf("You are currently using an outdated Haven CLI version by more than one major version %s: the latest major version v%d.x.x has been released %d days ago", Version, latestVersion.Major(), daysBetweenNowLatestMajor)
	}

	// "FAIL" (2)
	if latestVersion.Major()-currentVersion.Major() == 1 && daysBetweenNowLatestMajor > 90 {
		return ResultNo, fmt.Errorf("Your Haven CLI version %s is not the latest major version released v%d.x.x. Furthermore the 3 months upgrade window has expired since it was released %d days ago. Please upgrade the Haven CLI and try again.", Version, latestVersion.Major(), daysBetweenNowLatestMajor)
	}

	// "UPGRADE NOTICE"
	logging.Warning("You are currently using an outdated Haven CLI version %s: the latest stable version %s has been released %d days ago", Version, config.HavenReleases.latest().Version, daysBetweenNowLatest)

	return ResultYes, nil
}

func fundamentalClusterAdmin(ctx context.Context, config *Config) (Result, error) {
	sar := &authorizationv1.SelfSubjectAccessReview{
		Spec: authorizationv1.SelfSubjectAccessReviewSpec{
			NonResourceAttributes: &authorizationv1.NonResourceAttributes{
				Verb: "*",
				Path: "*",
			},
		},
	}

	response, err := config.Kube.AuthorizationV1().
		SelfSubjectAccessReviews().
		Create(ctx, sar, v1.CreateOptions{})

	if err != nil {
		return ResultNo, err
	}

	if !response.Status.Allowed {
		return ResultNo, errors.New("Haven Compliancy Checker requires cluster-admin access.")
	}

	return ResultYes, nil
}
