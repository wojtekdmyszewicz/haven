package compliancy

import (
	"fmt"
	"io"
	"math"
	"os"
	"strings"
	"sync/atomic"
	"time"
)

type ProgressBar struct {
	scale        int64
	completed    atomic.Int64
	total        int64
	finish       chan struct{}
	output       string
	totalRunTime time.Duration
	refresh      atomic.Int64
	running      time.Duration
	writer       io.Writer
}

// NewProgressBar creates a new progressbar based on the total and completed input
func NewProgressBar(total, completed int64) *ProgressBar {
	var scale int64
	switch {
	case total > 0 && total <= 50:
		scale = 1
	case total > 50 && total <= 200:
		scale = 3
	case total > 200 && total <= 400:
		scale = 5
	case total > 400 && total <= 1000:
		scale = 10
	case total > 1000 && total <= 2000:
		scale = 20
	case total > 2000:
		scale = 50
	}

	pb := ProgressBar{
		scale:        scale,
		total:        total,
		output:       "",
		finish:       make(chan struct{}),
		writer:       os.Stdout,
		running:      0 * time.Millisecond,
		totalRunTime: 90 * time.Minute,
	}

	pb.completed.Store(completed)
	pb.refresh.Store(int64(30000 * time.Millisecond))

	return &pb
}

func (p *ProgressBar) refresher() {
	for {
		refresh := time.Duration(p.refresh.Load())

		select {
		case <-p.finish:
			return
		case <-time.After(refresh):
			p.running += refresh
			p.createOutput()
			p.printProgress()
		}
	}
}

// SetRefresh can overwrite the interval at which the output is updated and printed
func (p *ProgressBar) SetRefresh(duration time.Duration) {
	p.refresh.Store(int64(duration))
}

// SetTotalRunTime can overwrite the default of 90min runtime
func (p *ProgressBar) SetTotalRunTime(duration time.Duration) {
	p.totalRunTime = duration
	p.createOutput()
}

// SetWriter can overwrite the default os.Stdout writer by any writer of choice
func (p *ProgressBar) SetWriter(writer io.Writer) {
	p.writer = writer
}

// Start starts the progressbar by creating output and then running a loop
func (p *ProgressBar) Start() {
	p.createOutput()
	go p.refresher()
}

// Finish closes the channel which will end the refresher loop
func (p *ProgressBar) Finish() {
	fmt.Fprint(p.writer, "\r"+p.output+"\n")
	close(p.finish)
}

func (p *ProgressBar) createOutput() {
	completed := p.completed.Load()
	progressBarStart := "["
	progressBarEnd := "]"
	progressBarPointer := ">"
	progressBarBody := "="
	fillerBarBody := "∙"

	var totalLenOfBar, lenOfBodyBar float64

	// Fix for strange cases where `total` is zero
	if p.total > 0 {
		totalLenOfBar = math.Round(float64(p.total / p.scale))
		lenOfBodyBar = math.Round(float64(completed / p.scale))
	}

	if lenOfBodyBar == 0 {
		lenOfBodyBar += 1
	}
	bodyBar := strings.Repeat(progressBarBody, int(lenOfBodyBar))

	var remainingBar string
	if p.total > 0 {
		remainingBar = strings.Repeat(fillerBarBody, int(totalLenOfBar-lenOfBodyBar))
	}

	bar := fmt.Sprintf("%s%s%s", bodyBar, progressBarPointer, remainingBar)

	timeRunning := p.running.Round(time.Second)

	// if no tests are completed or the test have not run, we set the time to the default value
	var printableTime time.Duration
	if completed == 0 || p.running.Seconds() == 0 {
		printableTime = p.totalRunTime
		// calculate the total runtime by dividing the number of seconds running by the completed tests
		// this is a guesstimate which will give different results depending on the completed tests
	} else {
		timePerTest := p.running.Seconds() / (float64(completed))
		totalTime := float64(p.total) * timePerTest
		timeRemaining := totalTime - p.running.Seconds()
		asDuration := time.Duration(timeRemaining) * time.Second
		printableTime = asDuration
	}

	// the progress bar should be a one liner, so we can "\r" the previous entry
	progressBar := fmt.Sprintf("    Test %v/%v | %s%s%s | Running: %s | ETA: %s", completed, p.total, progressBarStart, bar, progressBarEnd, timeRunning, printableTime)

	p.output = progressBar
}

func (p *ProgressBar) printProgress() {
	fmt.Fprint(p.writer, "\r"+p.output)
}
