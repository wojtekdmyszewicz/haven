package compliancy

import (
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestProgressBar(t *testing.T) {
	t.Run("DivideByZero", func(t *testing.T) {
		total := int64(0)
		completed := int64(0)
		bar := NewProgressBar(total, completed)
		bar.createOutput()
	})

	t.Run("CanSetRefresh", func(t *testing.T) {
		total := int64(300)
		completed := int64(45)
		bar := NewProgressBar(total, completed)
		refresh := 100 * time.Millisecond
		bar.Start()
		bar.SetRefresh(refresh)
		time.Sleep(10 * time.Millisecond)
		assert.Equal(t, time.Duration(bar.refresh.Load()), refresh)
		bar.Finish()
	})

	t.Run("CanSetTotalRunTime", func(t *testing.T) {
		total := int64(100)
		completed := int64(3)
		bar := NewProgressBar(total, completed)
		totalTime := 10 * time.Second
		refresh := 10 * time.Millisecond
		bar.SetRefresh(refresh)
		bar.Start()
		bar.SetTotalRunTime(totalTime)
		assert.Equal(t, bar.totalRunTime, totalTime)
		bar.Finish()
	})

	t.Run("OututIsAtLeastOneBar", func(t *testing.T) {
		total := int64(500)
		completed := int64(0)
		bar := NewProgressBar(total, completed)
		refresh := 10 * time.Millisecond
		bar.SetRefresh(refresh)
		bar.Start()
		assert.Contains(t, bar.output, strconv.Itoa(int(total)))
		assert.Contains(t, bar.output, strconv.Itoa(int(completed)))
		assert.Contains(t, bar.output, "=")
		assert.Contains(t, bar.output, ">")
		bar.Finish()
	})

	t.Run("BarCanBeFull", func(t *testing.T) {
		total := int64(2500)
		completed := int64(2500)
		bar := NewProgressBar(total, completed)
		refresh := 10 * time.Millisecond
		bar.SetRefresh(refresh)
		bar.Start()
		assert.Contains(t, bar.output, strconv.Itoa(int(total)))
		assert.Contains(t, bar.output, strconv.Itoa(int(completed)))
		assert.Contains(t, bar.output, "=")
		assert.Contains(t, bar.output, ">")
		assert.NotContains(t, bar.output, "∙")
		bar.Finish()
	})
}
