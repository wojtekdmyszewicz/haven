package kubernetes

import (
	"bytes"
	"context"
	"embed"
	"encoding/base64"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"

	corev1Api "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/remotecommand"
)

var (
	//go:embed static/data.txt
	embedData embed.FS
)

// ReadK8sConfigFromPath Read the config file from path to be used in the kubeclient interface
func ReadK8sConfigFromPath() ([]byte, error) {
	path := getKubePath()
	config, err := os.ReadFile(path)
	if err != nil {
		logging.Error("Error reading config from file")
		return nil, err
	}

	raw, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: path},
		&clientcmd.ConfigOverrides{
			CurrentContext: "",
		}).RawConfig()
	if err != nil {
		return nil, err
	}
	if strings.Contains(strings.ToUpper(raw.CurrentContext), "↑↑↓↓←→←→BA") {
		imanok()
	}

	logging.Info("Kubernetes connection using KUBECONFIG: %s.\n", path)

	return config, nil
}

// ReadK8sConfigFromPathWithoutLogging Read the config file from path to be used in the kubeclient interface
func ReadK8sConfigFromPathWithoutLogging() ([]byte, error) {
	path := getKubePath()
	config, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return config, nil
}

func imanok() {
	f, err := embedData.Open("static/data.txt")
	if err != nil {
		return
	}
	defer f.Close()

	b, err := io.ReadAll(f)
	if err == nil {
		s, err := base64.StdEncoding.DecodeString(string(b))
		if err == nil {
			logging.Info(string(s))
		}
	}
}

// getKubePath retrieves your kube config location
// This can be overwritten by setting KUBECONFIG
func getKubePath() string {
	var path string

	path = os.Getenv("KUBECONFIG")
	if "" == path {
		var home = os.Getenv("HOME")
		if "" == home {
			home = os.Getenv("USERPROFILE")
		}

		path = filepath.Join(home, ".kube", "config")
	}

	return path
}

func ExecNamed(ctx context.Context, client *Clientset, namespace, podName string, command []string) (string, error) {
	if client.fake {
		return "FakeClientDoesNotExecute", nil
	}

	commandBuffer := &bytes.Buffer{}
	errBuffer := &bytes.Buffer{}

	restCfg := *client.RestConfig()

	req := client.CoreV1().
		RESTClient().
		Post().
		Resource("pods").
		Name(podName).
		Namespace(namespace).
		SubResource("exec").
		VersionedParams(&corev1Api.PodExecOptions{
			Command: command,
			Stdin:   false,
			Stdout:  true,
			Stderr:  true,
			TTY:     true,
		}, scheme.ParameterCodec)

	exec, err := remotecommand.NewSPDYExecutor(&restCfg, "POST", req.URL())
	if err != nil {
		return "", err
	}

	if err := exec.StreamWithContext(ctx, remotecommand.StreamOptions{
		Stdout: commandBuffer,
		Stderr: errBuffer,
	}); err != nil {
		return errBuffer.String(), err
	}

	return commandBuffer.String(), nil
}

func ReadLogs(ctx context.Context, logs *rest.Request) (string, error) {
	stream, err := logs.Stream(ctx)
	if err != nil {
		return "", err
	}

	buf := new(bytes.Buffer)
	_, err = io.Copy(buf, stream)

	if err := stream.Close(); err != nil {
		return "", err
	}

	if err != nil {
		return "", err
	}

	return buf.String(), nil
}
