/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

//+kubebuilder:subresource:input
//+kubebuilder:subresource:output

// CompliancySpec defines the desired state of Compliancy
type CompliancySpec struct {
	Compliant bool             `json:"compliant"`
	Created   string           `json:"created"`
	Version   string           `json:"version"`
	Input     CompliancyInput  `json:"input"`
	Output    CompliancyOutput `json:"output"`
}

// CompliancyPlatform defines the platform for the Compliancy check
type CompliancyPlatform struct {
	Proxy              bool   `json:"proxy"`
	DeterminingLabel   string `json:"determiningLabel"`
	DeterminedPlatform string `json:"determinedPlatform"`
}

//+kubebuilder:subresource:platform

// CompliancyInput defines the input for the Compliancy check
type CompliancyInput struct {
	Commandline string             `json:"commandline"`
	KubeHost    string             `json:"kubeHost"`
	Platform    CompliancyPlatform `json:"platform"`
}

// ComplicacyConfig contains the configuration for optional checks
type CompliancyConfig struct {
	Cncf      bool `json:"CNCF"`
	Cis       bool `json:"CIS"`
	Kubescape bool `json:"kubescape"`
}

// CompliancySummary contains the result and details of a check
type CompliancyResult struct {
	Name      string `json:"name"`
	Label     string `json:"label"`
	Category  string `json:"category"`
	Rationale string `json:"rationale"`
	Result    string `json:"result"`
}

// CompliancySummary contains the summary of the checks
type CompliancySummary struct {
	Total   int `json:"total"`
	Unknown int `json:"unknown"`
	Skipped int `json:"skipped"`
	Failed  int `json:"failed"`
	Passed  int `json:"passed"`
}

//+kubebuilder:subresource:results
//+kubebuilder:subresource:summary

// CompliancyChecks contains the results of the checks
type CompliancyChecks struct {
	Results []CompliancyResult `json:"results"`
	Summary CompliancySummary  `json:"summary"`
}

// CompliancySuggestedChecks contains the suggested checks results
type CompliancySuggestedChecks struct {
	Results []CompliancyResult `json:"results"`
}

//+kubebuilder:subresource:config
//+kubebuilder:subresource:compliancyChecks
//+kubebuilder:subresource:suggestedChecks

// CompliancyOutput defines the output for the Compliancy check
type CompliancyOutput struct {
	Version          string                    `json:"version"`
	HavenCompliant   bool                      `json:"havenCompliant"`
	StartTS          string                    `json:"startTS"`
	StopTS           string                    `json:"stopTS"`
	Config           CompliancyConfig          `json:"config"`
	CompliancyChecks CompliancyChecks          `json:"compliancyChecks"`
	SuggestedChecks  CompliancySuggestedChecks `json:"suggestedChecks"`
}

//+kubebuilder:object:root=true
//+kubebuilder:resource:scope=Cluster

// Compliancy is the Schema for the compliancies API
type Compliancy struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec CompliancySpec `json:"spec"`
}

//+kubebuilder:object:root=true

// CompliancyList contains a list of Compliancy
type CompliancyList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Compliancy `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Compliancy{}, &CompliancyList{})
}
