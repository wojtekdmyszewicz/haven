package kubernetes

import (
	"context"
	"time"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	extv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	podKind       string            = "Pod"
	podVersion    string            = "v1"
	testNamespace string            = "test-namespace"
	podImage      string            = "testimage"
	podPullPolicy corev1.PullPolicy = "Always"
)

func createPodObject(name, ns string) *corev1.Pod {
	if ns == "" {
		ns = testNamespace
	}
	pod := &corev1.Pod{
		TypeMeta: metav1.TypeMeta{
			Kind:       podKind,
			APIVersion: podVersion,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: ns,
			Labels: map[string]string{
				"app":      name,
				"job-name": name,
			},
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:            name,
					Image:           podImage,
					ImagePullPolicy: podPullPolicy,
				},
			},
		},
		Status: corev1.PodStatus{
			Phase: "Succeeded",
		},
	}

	return pod
}

func createDeploymentObject(name, ns string) *appsv1.Deployment {
	if ns == "" {
		ns = testNamespace
	}

	replicas := int32(2)

	deploy := &appsv1.Deployment{
		TypeMeta: metav1.TypeMeta{
			Kind:       podKind,
			APIVersion: podVersion,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: ns,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas:                &replicas,
			Selector:                nil,
			Template:                corev1.PodTemplateSpec{},
			Strategy:                appsv1.DeploymentStrategy{},
			MinReadySeconds:         0,
			RevisionHistoryLimit:    nil,
			Paused:                  false,
			ProgressDeadlineSeconds: nil,
		},
	}

	return deploy
}

func createCrdObject(name string) *extv1.CustomResourceDefinition {
	crd := &extv1.CustomResourceDefinition{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: extv1.CustomResourceDefinitionSpec{
			Group: "mygroup.example.com",
			Versions: []extv1.CustomResourceDefinitionVersion{
				{
					Name:    "v1beta1",
					Served:  true,
					Storage: true,
					Schema:  allowAllSchema(),
				},
			},
			Names: extv1.CustomResourceDefinitionNames{
				Plural:   name + "s",
				Singular: name,
				Kind:     name,
				ListKind: name + "List",
			},
			Scope: "Cluster",
		},
	}

	return crd
}

func createNodeItem(name string, taints []corev1.Taint, labels map[string]string) *corev1.Node {
	return &corev1.Node{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name:                       name,
			GenerateName:               "",
			Namespace:                  "",
			SelfLink:                   "",
			UID:                        "",
			ResourceVersion:            "",
			Generation:                 0,
			CreationTimestamp:          metav1.Time{},
			DeletionTimestamp:          nil,
			DeletionGracePeriodSeconds: nil,
			Labels:                     labels,
			Annotations:                nil,
			OwnerReferences:            nil,
			Finalizers:                 nil,
			ManagedFields:              nil,
		},
		Spec: corev1.NodeSpec{
			PodCIDR:            "",
			PodCIDRs:           nil,
			ProviderID:         "",
			Unschedulable:      false,
			Taints:             taints,
			ConfigSource:       nil,
			DoNotUseExternalID: "",
		},
		Status: corev1.NodeStatus{
			Capacity:        nil,
			Allocatable:     nil,
			Phase:           "",
			Conditions:      nil,
			Addresses:       nil,
			DaemonEndpoints: corev1.NodeDaemonEndpoints{},
			NodeInfo:        corev1.NodeSystemInfo{},
			Images:          nil,
			VolumesInUse:    nil,
			VolumesAttached: nil,
			Config:          nil,
		},
	}
}

// allowAllSchema doesn't enforce any schema restrictions
func allowAllSchema() *extv1.CustomResourceValidation {
	b := true

	return &extv1.CustomResourceValidation{
		OpenAPIV3Schema: &extv1.JSONSchemaProps{
			XPreserveUnknownFields: &b,
			Type:                   "object",
		},
	}
}

// CreatePodForTest creates a pod for a client used for testing
func CreatePodForTest(name, ns string, client *Clientset) error {
	pod := createPodObject(name, ns)
	_, err := client.CoreV1().Pods(ns).Create(context.TODO(), pod, metav1.CreateOptions{})
	return err
}

// CreateCrdForTest creates a test crd using the fake client
func CreateCrdForTest(name string, client *Clientset) error {
	crd := createCrdObject(name)
	_, err := client.ApiextensionsV1().CustomResourceDefinitions().Create(context.TODO(), crd, metav1.CreateOptions{})
	return err
}

// CreateDeploymentForTest creates a deployment using a fake client
func CreateDeploymentForTest(name, ns string, client *Clientset) error {
	deploy := createDeploymentObject(name, ns)
	_, err := client.AppsV1().Deployments(ns).Create(context.TODO(), deploy, metav1.CreateOptions{})
	return err
}

// CreateNodeForTest creates a node using a fake client
func CreateNodeForTest(name string, taints []corev1.Taint, labels map[string]string, client *Clientset) error {
	node := createNodeItem(name, taints, labels)
	_, err := client.CoreV1().Nodes().Create(context.TODO(), node, metav1.CreateOptions{})
	return err
}

func SetPodStatusToRunning(quit chan struct{}, client *Clientset, ns string) error {
	ticker := time.NewTicker(5 * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			pods, err := client.CoreV1().Pods(ns).List(context.TODO(), metav1.ListOptions{})
			if err != nil {
				return err
			}

			for _, po := range pods.Items {
				po.Status.Phase = corev1.PodRunning
				_, err := client.CoreV1().Pods(ns).Update(context.TODO(), &po, metav1.UpdateOptions{})
				if err != nil {
					return err
				}
			}
		case <-quit:
			break
		}
	}
}
