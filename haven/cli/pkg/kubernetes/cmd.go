package kubernetes

import (
	"context"
	"encoding/base64"
	"errors"
	"strconv"
	"strings"
	"time"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

type RunCommandOption func(opts *runCommandOpts)

type runCommandOpts struct {
	useMaster       bool
	namespace       string
	timeout         time.Duration
	tickInterval    time.Duration
	shutdownTimeout time.Duration
}

func WithMaster() RunCommandOption {
	return func(opts *runCommandOpts) {
		opts.useMaster = true
	}
}

func WithNamespace(namespace string) RunCommandOption {
	return func(opts *runCommandOpts) {
		opts.namespace = namespace
	}
}

func WithTimeout(timeout time.Duration) RunCommandOption {
	return func(opts *runCommandOpts) {
		opts.timeout = timeout
	}
}

func WithShutdownTimeout(shutdownTimeout time.Duration) RunCommandOption {
	return func(opts *runCommandOpts) {
		opts.shutdownTimeout = shutdownTimeout
	}
}

func WithTickInterval(tickInterval time.Duration) RunCommandOption {
	return func(opts *runCommandOpts) {
		opts.tickInterval = tickInterval
	}
}

// ExecCmd takes a string as command input and returns the output from
// running that command from a pod, unless this results in an error.
// It's possible to force scheduling of the pod on a master node when needed.
func ExecCmd(ctx context.Context, kube *Clientset, cmd string, commandOpts ...RunCommandOption) (string, error) {
	opts := &runCommandOpts{
		namespace:       "default",
		timeout:         5 * time.Minute,
		tickInterval:    5 * time.Second,
		shutdownTimeout: 5 * time.Second,
	}

	for _, opt := range commandOpts {
		opt(opts)
	}

	var master apiv1.Node

	if opts.useMaster {
		// Find a master node to work with.
		nodes, err := kube.CoreV1().Nodes().List(ctx, metav1.ListOptions{})
		if err != nil {
			return "", err
		}

		for _, n := range nodes.Items {
			if n.Labels["kubernetes.io/role"] == "master" {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/control-plane"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/controlplane"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/master"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node.kubernetes.io/master"]; exists {
				master = n
				break
			}
		}

		if master.Name == "" {
			return "", errors.New("could not find any master node to run privileged pod on")
		}
	}

	// Create privileged pod. See https://miminar.fedorapeople.org/_preview/openshift-enterprise/registry-redeploy/go_client/executing_remote_processes.html.
	app := "hcc-test-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])

	var tolerations []apiv1.Toleration

	if opts.useMaster {
		for _, taint := range master.Spec.Taints {
			tolerations = append(tolerations, apiv1.Toleration{
				Key:      taint.Key,
				Operator: apiv1.TolerationOpExists,
				Effect:   taint.Effect,
			})
		}
	}

	podConfig := &apiv1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: app,
		},
		Spec: apiv1.PodSpec{
			Containers: []apiv1.Container{
				{
					Name:    app,
					Image:   "busybox",
					Command: []string{"cat"},
					Stdin:   true,
				},
			},
			HostPID:     true,
			NodeName:    master.Name,
			Tolerations: tolerations,
		},
	}

	// Mount /proc and /sys. Some popular CRs will mount both by default:
	// - https://github.com/containerd/containerd/blob/v1.6.4/oci/mounts.go#L29-L63
	// - https://github.com/moby/moby/blob/master/oci/defaults.go#L45-L69
	// But, others don't:
	// - https://github.com/cri-o/cri-o/blob/v1.24.0/server/container_create_linux.go#L507-L537 (conditionally)
	// So, we cannot rely on it and will have to take matters into own mounts
	podConfig.Spec.Volumes = []apiv1.Volume{
		{
			Name: "sysfs",
			VolumeSource: apiv1.VolumeSource{
				HostPath: &apiv1.HostPathVolumeSource{
					Path: "/sys",
				},
			},
		},
		// we could be able to control proc mount through spec.containers[].securityContext.procMount,
		// but on some k8s distro's there are (PS)policies preventing that. This works.
		{
			Name: "proc",
			VolumeSource: apiv1.VolumeSource{
				HostPath: &apiv1.HostPathVolumeSource{
					Path: "/proc",
				},
			},
		},
	}
	podConfig.Spec.Containers[0].VolumeMounts = []apiv1.VolumeMount{
		{
			Name:      "sysfs",
			MountPath: "/sys",
			ReadOnly:  true,
		},
		{
			Name:      "proc",
			MountPath: "/proc",
		},
	}

	pod, err := kube.CoreV1().Pods(opts.namespace).Create(ctx, podConfig, metav1.CreateOptions{})
	if err != nil {
		return "", err
	}

	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), opts.shutdownTimeout)
		defer cancel()

		if err := kube.CoreV1().Pods(pod.Namespace).Delete(ctx, pod.Name, metav1.DeleteOptions{}); err != nil {
			logging.Error("Could not cleanup pod: '%s'", err.Error())
		}
	}()

	ticker := time.NewTicker(opts.tickInterval)
	defer ticker.Stop()

	waitCtx, cancel := context.WithTimeout(ctx, opts.timeout)
	defer cancel()

	for {
		select {
		case <-waitCtx.Done():
			logging.Error("timed out")
			return "", waitCtx.Err()
		case <-ticker.C:
		}

		po, err := kube.CoreV1().Pods(opts.namespace).Get(ctx, pod.Name, metav1.GetOptions{})
		if err != nil {
			continue
		}

		if po.Status.Phase != apiv1.PodRunning {
			continue
		}

		break
	}

	command := []string{"sh", "-c", cmd}

	output, err := ExecNamed(ctx, kube, pod.Namespace, pod.Name, command)
	if err != nil {
		return "", err
	}

	if len(output) != 0 {
		output += "\n" + output
	}

	return output, nil
}
