package haven

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
)

func TestIntegrationDefinitionNotCreatedIfExists(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	kubeConfig, err := kubernetes.ReadK8sConfigFromPathWithoutLogging()
	assert.Nil(t, err)

	clientset, err := kubernetes.NewFromConfig(kubeConfig)
	assert.NoError(t, err)

	crdClient, err := NewCrdConfig(clientset)
	assert.NoError(t, err)

	_, err = crdClient.CreateHavenDefinition()

	assert.NoError(t, err)
}

func TestIntegrationDefinitionCreatedIfNotExists(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	kubeConfig, err := kubernetes.ReadK8sConfigFromPathWithoutLogging()
	assert.Nil(t, err)

	clientset, err := kubernetes.NewFromConfig(kubeConfig)
	assert.NoError(t, err)

	crdClient, err := NewCrdConfig(clientset)
	assert.Nil(t, err)

	_, err = crdClient.CreateHavenDefinition()

	assert.Nil(t, err)
}
