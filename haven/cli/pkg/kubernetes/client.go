package kubernetes

import (
	appsv1Api "k8s.io/api/apps/v1"
	corev1Api "k8s.io/api/core/v1"
	apiextensionsv1Api "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset/typed/apiextensions/v1"
	fakeapiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset/typed/apiextensions/v1/fake"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/dynamic"
	appsv1 "k8s.io/client-go/kubernetes/typed/apps/v1"
	fakeappsv1 "k8s.io/client-go/kubernetes/typed/apps/v1/fake"
	authorizationv1 "k8s.io/client-go/kubernetes/typed/authorization/v1"
	fakeauthorizationv1 "k8s.io/client-go/kubernetes/typed/authorization/v1/fake"
	batchv1 "k8s.io/client-go/kubernetes/typed/batch/v1"
	fakebatchv1 "k8s.io/client-go/kubernetes/typed/batch/v1/fake"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	fakecorev1 "k8s.io/client-go/kubernetes/typed/core/v1/fake"
	rbacv1 "k8s.io/client-go/kubernetes/typed/rbac/v1"
	fakerbacv1 "k8s.io/client-go/kubernetes/typed/rbac/v1/fake"
	storagev1 "k8s.io/client-go/kubernetes/typed/storage/v1"
	fakestoragev1 "k8s.io/client-go/kubernetes/typed/storage/v1/fake"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/testing"
	"k8s.io/client-go/tools/clientcmd"
)

type Clientset struct {
	fake            bool
	config          *rest.Config
	coreV1          corev1.CoreV1Interface
	appsV1          appsv1.AppsV1Interface
	rbacV1          rbacv1.RbacV1Interface
	batchV1         batchv1.BatchV1Interface
	storageV1       storagev1.StorageV1Interface
	discovery       discovery.DiscoveryInterface
	authorizationV1 authorizationv1.AuthorizationV1Interface
	apiextensionsV1 apiextensionsv1.ApiextensionsV1Interface
	dynamic         dynamic.Interface
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}

func NewFakeClientset() *Clientset {
	var fake testing.Fake

	scheme := runtime.NewScheme()

	must(metav1.AddMetaToScheme(scheme))
	must(corev1Api.AddToScheme(scheme))
	must(apiextensionsv1Api.AddToScheme(scheme))
	must(appsv1Api.AddToScheme(scheme))

	codecs := serializer.NewCodecFactory(scheme)
	tracker := testing.NewObjectTracker(scheme, codecs.UniversalDecoder())

	fake.AddReactor("*", "*", testing.ObjectReaction(tracker))

	return &Clientset{
		fake:            true,
		coreV1:          &fakecorev1.FakeCoreV1{Fake: &fake},
		appsV1:          &fakeappsv1.FakeAppsV1{Fake: &fake},
		rbacV1:          &fakerbacv1.FakeRbacV1{Fake: &fake},
		batchV1:         &fakebatchv1.FakeBatchV1{Fake: &fake},
		storageV1:       &fakestoragev1.FakeStorageV1{Fake: &fake},
		authorizationV1: &fakeauthorizationv1.FakeAuthorizationV1{Fake: &fake},
		apiextensionsV1: &fakeapiextensionsv1.FakeApiextensionsV1{Fake: &fake},
	}
}

func NewClientset(c *rest.Config) (*Clientset, error) {
	httpClient, err := rest.HTTPClientFor(c)
	if err != nil {
		return nil, err
	}

	var cs Clientset

	cs.config = c

	cs.coreV1, err = corev1.NewForConfigAndClient(c, httpClient)
	if err != nil {
		return nil, err
	}

	cs.batchV1, err = batchv1.NewForConfigAndClient(c, httpClient)
	if err != nil {
		return nil, err
	}

	cs.storageV1, err = storagev1.NewForConfigAndClient(c, httpClient)
	if err != nil {
		return nil, err
	}

	cs.appsV1, err = appsv1.NewForConfigAndClient(c, httpClient)
	if err != nil {
		return nil, err
	}

	cs.rbacV1, err = rbacv1.NewForConfigAndClient(c, httpClient)
	if err != nil {
		return nil, err
	}

	cs.authorizationV1, err = authorizationv1.NewForConfigAndClient(c, httpClient)
	if err != nil {
		return nil, err
	}

	cs.discovery, err = discovery.NewDiscoveryClientForConfigAndClient(c, httpClient)
	if err != nil {
		return nil, err
	}

	cs.apiextensionsV1, err = apiextensionsv1.NewForConfigAndClient(c, httpClient)
	if err != nil {
		return nil, err
	}

	cs.dynamic, err = dynamic.NewForConfig(c)
	if err != nil {
		return nil, err
	}

	return &cs, nil
}

func (c *Clientset) CoreV1() corev1.CoreV1Interface {
	return c.coreV1
}

func (c *Clientset) BatchV1() batchv1.BatchV1Interface {
	return c.batchV1
}

func (c *Clientset) StorageV1() storagev1.StorageV1Interface {
	return c.storageV1
}

func (c *Clientset) AppsV1() appsv1.AppsV1Interface {
	return c.appsV1
}

func (c *Clientset) AuthorizationV1() authorizationv1.AuthorizationV1Interface {
	return c.authorizationV1
}

func (c *Clientset) RbacV1() rbacv1.RbacV1Interface {
	return c.rbacV1
}

func (c *Clientset) Discovery() discovery.DiscoveryInterface {
	return c.discovery
}

func (c *Clientset) ApiextensionsV1() apiextensionsv1.ApiextensionsV1Interface {
	return c.apiextensionsV1
}

func (c *Clientset) Dynamic() dynamic.Interface {
	return c.dynamic
}

func (c *Clientset) Host() string {
	return c.config.Host
}

func (c *Clientset) RestConfig() *rest.Config {
	return c.config
}

func (c *Clientset) KubeCliConfig(namespace string) (*genericclioptions.ConfigFlags, error) {
	kubeConfig := genericclioptions.NewConfigFlags(false)
	kubeConfig.APIServer = &c.config.Host
	kubeConfig.BearerToken = &c.config.BearerToken
	kubeConfig.CAFile = &c.config.CAFile
	kubeConfig.Namespace = &namespace

	return kubeConfig, nil
}

func NewFromConfig(config []byte) (*Clientset, error) {
	c, err := clientcmd.NewClientConfigFromBytes(config)
	if err != nil {
		return nil, err
	}

	restConfig, err := c.ClientConfig()
	if err != nil {
		return nil, err
	}

	return NewClientset(restConfig)
}
