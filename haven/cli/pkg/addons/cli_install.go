// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package addons

import (
	"os"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"

	"github.com/AlecAivazis/survey/v2"
	cli "github.com/jawher/mow.cli"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func cmdInstall(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to install")

	cmd.Action = func() {
		addon, err := Get(*name)

		if err != nil {
			logging.Error("Error while getting addon: %s\n", err)
			return
		}

		kubePath, _ := kubernetes.ReadK8sConfigFromPath()
		kube, err := kubernetes.NewFromConfig(kubePath)
		if err != nil {
			logging.Error("Error creating kubeClient: %s\n", err)
			return
		}

		values := map[string]interface{}{}
		err = survey.Ask(addon.GetSurveyQuestions(), &values)
		if err != nil {
			logging.Error("Error processing answers: %s\n", err)
			return
		}

		installer := NewInstaller(kube)

		ch := installer.Install(addon, values)

		for event := range ch {
			switch event.Stage {
			case PreInstallStage:
			case AddHelmRepoStage:
				logging.Info("Adding repository %s\n", addon.Chart.Repository.Name)
			case InstallHelmStage:
				logging.Info("Installing %s\n", addon.Name)
			case PostInstallStage:
			default:
				if event.Err != nil {
					logging.Error("Failed to install addon: %s\n", event.Err)
					os.Exit(1)
				}
			}
		}

		logging.Info("Installed addon %s in namespace %s\n", addon.Name, addon.Namespace)
	}
}
