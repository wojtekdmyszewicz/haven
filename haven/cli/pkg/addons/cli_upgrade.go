// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package addons

import (
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"

	cli "github.com/jawher/mow.cli"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func cmdUpgrade(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to upgrade")

	cmd.Action = func() {
		addon, err := Get(*name)
		if err != nil {
			logging.Error("Error while getting addon: %s\n", err)
			return
		}

		kubePath, _ := kubernetes.ReadK8sConfigFromPath()
		kubeImpl, err := kubernetes.NewFromConfig(kubePath)
		if err != nil {
			logging.Error("Error creating kube client %s", err)
			return
		}

		helmClient, err := helm.NewClient(addon.Namespace, kubeImpl)
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		logging.Info("Adding repository %s\n", addon.Chart.Repository.Name)

		_, err = helm.AddRepository(addon.Chart.Repository.Name, addon.Chart.Repository.URL)
		if err != nil {
			logging.Error("Error adding repository: %s\n", err)
		}

		logging.Info("Upgrading %s\n", addon.Name)

		_, err = helmClient.Upgrade(
			addon.Name,
			addon.ChartName(),
			nil,
		)

		if err != nil {
			logging.Error("Error upgrading release: %s\n", err)
			return
		}

		logging.Info("Upgraded addon %s in namespace %s", addon.Name, addon.Namespace)
	}
}
