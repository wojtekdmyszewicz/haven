package addons

import (
	"fmt"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/rand"
)

type InstallStage string

const (
	PreInstallStage  InstallStage = "pre-install"
	AddHelmRepoStage InstallStage = "add-helm-repo"
	InstallHelmStage InstallStage = "helm-install"
	PostInstallStage InstallStage = "post-install"
)

type InstallEvent struct {
	Stage InstallStage
	Err   error
}

func newError(err error) InstallEvent {
	return InstallEvent{Err: err}
}

func stage(stage InstallStage) InstallEvent {
	return InstallEvent{Stage: stage}
}

// Installer is responsible for installing addons
type Installer struct {
	kube *kubernetes.Clientset
}

// NewInstaller creates a new instance of the installer
func NewInstaller(kube *kubernetes.Clientset) Installer {
	return Installer{
		kube: kube,
	}
}

// SimpleInstall installs the addon synchronously and returns an error if it fails
func (i Installer) SimpleInstall(addon *Addon, values map[string]interface{}) error {
	ch := i.Install(addon, values)

	for event := range ch {
		if event.Err != nil {
			return event.Err
		}
	}

	return nil
}

func (i Installer) Install(addon *Addon, values map[string]interface{}) chan InstallEvent {
	ch := make(chan InstallEvent)

	go func() {
		defer close(ch)

		for _, value := range addon.GeneratedValues {
			values[value.Name] = rand.String(value.RandString)
		}

		ch <- stage(PreInstallStage)

		if len(addon.PreInstallYaml) > 0 {
			if err := ApplyYaml(addon.PreInstallYaml, values, i.kube); err != nil {
				ch <- newError(fmt.Errorf("failed to apply pre-install yaml: %w", err))
				return
			}
		}

		ch <- stage(AddHelmRepoStage)

		if _, err := helm.AddRepository(addon.Chart.Repository.Name, addon.Chart.Repository.URL); err != nil {
			ch <- newError(fmt.Errorf("failed to add helm repository: %w", err))
			return
		}

		helmValues, err := addon.GetValues(values)
		if err != nil {
			ch <- newError(fmt.Errorf("failed to compute helm values: %w", err))
			return
		}

		ch <- stage(InstallHelmStage)

		helmClient, err := helm.NewClient(addon.Namespace, i.kube)
		if err != nil {
			ch <- newError(fmt.Errorf("failed to create helm client: %w", err))
			return
		}

		if _, err := helmClient.Install(
			addon.Name,
			addon.ChartName(),
			helmValues,
		); err != nil {
			ch <- newError(fmt.Errorf("failed to install helm chart: %w", err))
			return
		}

		ch <- stage(PostInstallStage)

		if len(addon.PostInstallYaml) > 0 {
			if err := ApplyYaml(addon.PostInstallYaml, values, i.kube); err != nil {
				ch <- newError(fmt.Errorf("failed to apply post-install yaml: %w", err))
				return
			}
		}
	}()

	return ch
}
