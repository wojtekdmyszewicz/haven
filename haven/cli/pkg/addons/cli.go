// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package addons

import (
	cli "github.com/jawher/mow.cli"
)

// CLIConfig renders the command line function
var CLIConfig = func(config *cli.Cmd) {
	config.Command("list", "Show a list of available addons", cmdList)
	config.Command("install", "Install addon", cmdInstall)
	config.Command("upgrade", "Upgrade addon", cmdUpgrade)
	config.Command("uninstall", "Uninstall addon", cmdUninstall)
}
