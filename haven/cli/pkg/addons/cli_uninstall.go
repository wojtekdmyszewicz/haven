// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package addons

import (
	cli "github.com/jawher/mow.cli"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func cmdUninstall(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to uninstall")

	cmd.Action = func() {
		addon, err := Get(*name)
		if err != nil {
			logging.Error("Error while getting addon: %s\n", err)
			return
		}

		kubePath, _ := kubernetes.ReadK8sConfigFromPath()
		kubeImpl, err := kubernetes.NewFromConfig(kubePath)
		if err != nil {
			logging.Error("Error creating kube client %s", err)
			return
		}

		helmClient, err := helm.NewClient(addon.Namespace, kubeImpl)
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		logging.Info("Uninstalling %s\n", addon.Name)

		_, err = helmClient.Uninstall(addon.Name)
		if err != nil {
			logging.Error("Error while uninstalling: %s\n", err)
		}

		logging.Info("Uninstalled addon %s\n", *name)
	}
}
