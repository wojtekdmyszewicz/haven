// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package addons

import (
	"context"
	"fmt"
	"io"
	"strings"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"

	"github.com/AlecAivazis/survey/v2"
	"github.com/pytimer/k8sutil/apply"
	"golang.org/x/crypto/bcrypt"
	"sigs.k8s.io/yaml"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/rand"
)

// Addon is installable on a Haven cluster
type Addon struct {
	Name              string
	Namespace         string
	Description       string
	Questions         []Question
	GeneratedValues   []GeneratedValue
	Chart             Chart
	Charts            []NestedChart
	DefaultValuesFile string
	PreInstallYaml    []InstallYaml
	PostInstallYaml   []InstallYaml
}

func (a *Addon) ChartName() string {
	return fmt.Sprintf("%s/%s", a.Chart.Repository.Name, a.Chart.Name)
}

// GetSurveyQuestions returns questions in the form that survey expects
func (a *Addon) GetSurveyQuestions() []*survey.Question {
	var surveyQuestions []*survey.Question

	for _, question := range a.Questions {
		var defaultValue string
		if question.Default.RandString > 0 {
			defaultValue = rand.String(question.Default.RandString)
		}
		if question.Default.String != "" {
			defaultValue = question.Default.String
		}

		var transform func(s interface{}) interface{}
		if question.Transform.Password == "bcrypt" {
			transform = func(s interface{}) interface{} {
				inputString, ok := s.(string)
				if !ok {
					return ""
				}

				b, err := bcrypt.GenerateFromPassword([]byte(inputString), bcrypt.DefaultCost)
				if err != nil {
					return ""
				}

				return string(b)
			}
		}

		surveyQuestions = append(surveyQuestions, &survey.Question{
			Name: question.Name,
			Prompt: &survey.Input{
				Message: question.Message,
				Help:    question.Help,
				Default: defaultValue,
			},
			Transform: transform,
		})
	}

	return surveyQuestions
}

// GetValues opens the DefaultValuesFile, performs a search-and-replace for customValues and unmarshals the contents
func (a *Addon) GetValues(customValues map[string]interface{}) (map[string]interface{}, error) {
	f, err := embedFiles.Open(a.DefaultValuesFile)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	s := string(b)

	for key, value := range customValues {
		newValue, ok := value.(string)
		if !ok {
			continue
		}

		s = strings.ReplaceAll(s, key, newValue)
	}

	values := make(map[string]interface{})

	err = yaml.Unmarshal([]byte(s), &values)
	if err != nil {
		return nil, err
	}

	return values, nil
}

// AddIndentation is a helper to add indentation for yaml files
func (a *Addon) AddIndentation(input string, indentLength int) string {
	prefix := strings.Repeat(" ", indentLength)
	output := Indent(input, prefix)

	return output
}

// Indent inserts prefix at the beginning of each non-empty line of s.
func Indent(input, prefix string) string {
	return string(IndentBytes([]byte(input), []byte(prefix)))
}

// IndentBytes inserts prefix at the beginning of each non-empty line of b.
func IndentBytes(inputAsBytes, prefix []byte) []byte {
	var out []byte
	lineStart := true
	for _, in := range inputAsBytes {
		if lineStart && in != '\n' {
			out = append(out, prefix...)
		}
		out = append(out, in)
		lineStart = in == '\n'
	}
	return out
}

// ApplyYaml applies embedded yaml from pre- and post install addon configuration,
// taking custom values from survey questions into account.
func ApplyYaml(installYaml []InstallYaml, customValues map[string]interface{}, kube *kubernetes.Clientset) error {
	for _, iy := range installYaml {
		err := ApplySingleYaml(iy, customValues, kube)
		if err != nil {
			return err
		}
	}

	return nil
}

// ApplySingleYaml applies a single install yaml
func ApplySingleYaml(installYaml InstallYaml, customValues map[string]interface{}, kube *kubernetes.Clientset) error {
	logging.Info("Applying yaml: %s", installYaml.Name)

	applyOptions := apply.NewApplyOptions(kube.Dynamic(), kube.Discovery())

	f, err := embedFiles.Open(installYaml.Location)
	if err != nil {
		return err
	}
	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		return err
	}

	yamlcontent := string(b)

	for key, value := range customValues {
		newValue, ok := value.(string)
		if !ok {
			continue
		}

		yamlcontent = strings.ReplaceAll(yamlcontent, key, newValue)
	}

	if err := applyOptions.Apply(context.Background(), []byte(yamlcontent)); err != nil {
		return err
	}

	return nil
}
