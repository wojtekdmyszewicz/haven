// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package addons

import (
	"os"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"

	cli "github.com/jawher/mow.cli"
	"github.com/olekukonko/tablewriter"
	"helm.sh/helm/v3/pkg/release"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func cmdList(cmd *cli.Cmd) {
	cmd.Action = func() {
		table := tablewriter.NewWriter(os.Stdout)
		table.SetAutoWrapText(false)
		table.SetHeader([]string{"Name", "Description", "Installed"})

		kubePath, _ := kubernetes.ReadK8sConfigFromPath()
		kube, err := kubernetes.NewFromConfig(kubePath)
		if err != nil {
			logging.Error("Error creating kubeClient: %s\n", err)
			return
		}

		helmClient, err := helm.NewClient("", kube)
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		installedReleases, err := helmClient.List(helm.ListOpts{})
		if err != nil {
			logging.Error("Error listing releases: %s\n", err)
			return
		}

		addons, err := List()
		if err != nil {
			logging.Error("Error listing addons: %s\n", err)
			return
		}

		for _, addon := range addons {
			installedState := "NO"
			if isAddonInstalled(addon, installedReleases) {
				installedState = "YES"
			}

			table.Append([]string{addon.Name, addon.Description, installedState})
		}

		table.Render()
	}
}

func isAddonInstalled(addon Addon, installedReleases []*release.Release) bool {
	if len(addon.Charts) >= 1 {
		for _, chart := range addon.Charts {
			if chart.MainChart {
				for _, installedRelease := range installedReleases {
					if chart.Name == installedRelease.Chart.Name() {
						return true
					}
				}
			}
		}
		return false
	}

	for _, installedRelease := range installedReleases {
		if addon.Chart.Name == installedRelease.Chart.Name() && addon.Namespace == installedRelease.Namespace {
			return true
		}
	}

	return false
}
