package addon

import (
	"context"
	"errors"
	"fmt"

	"github.com/bufbuild/connect-go"
	rspb "helm.sh/helm/v3/pkg/release"
	"helm.sh/helm/v3/pkg/storage/driver"

	addonv1 "gitlab.com/commonground/haven/haven/haven/cli/gen/pkg/api/addon/v1"
	"gitlab.com/commonground/haven/haven/haven/cli/gen/pkg/api/addon/v1/addonv1connect"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/addons"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/rand"
)

var _ addonv1connect.AddonServiceHandler = (*Service)(nil)

var helmListReleases = func(driver driver.Driver, opts helm.ListOpts) ([]*rspb.Release, error) {
	return helm.ListWithDriver(driver, opts)
}

type Service struct {
	driver driver.Driver
	kube   *kubernetes.Clientset
}

func NewService(driver driver.Driver, kube *kubernetes.Clientset) *Service {
	return &Service{
		driver: driver,
		kube:   kube,
	}
}

func mapAddon(addon *addons.Addon) *addonv1.Addon {
	output := &addonv1.Addon{
		Name:        addon.Name,
		Namespace:   addon.Namespace,
		Description: addon.Description,
		Questions:   []*addonv1.Addon_Question{},
	}

	for _, question := range addon.Questions {
		defaultValue := question.Default.String

		if question.Default.RandString > 0 {
			defaultValue = rand.String(question.Default.RandString)
		}

		output.Questions = append(output.Questions, &addonv1.Addon_Question{
			Name:    question.Name,
			Help:    question.Help,
			Message: question.Message,
			Default: defaultValue,
		})
	}

	return output
}

func mapRelease(release *rspb.Release, addon *addons.Addon) *addonv1.Release {
	var addonResult *addonv1.Addon

	if addon != nil {
		addonResult = mapAddon(addon)
	}

	return &addonv1.Release{
		Name:      release.Name,
		Namespace: release.Namespace,
		Deployed:  release.Info.Status == "deployed",
		Addon:     addonResult,
		Chart: &addonv1.Chart{
			Name:    release.Chart.Metadata.Name,
			Version: release.Chart.Metadata.Version,
			Sources: release.Chart.Metadata.Sources,
		},
	}
}

func (s *Service) findRelease(name, namespace string) (*rspb.Release, error) {
	filter := func(release *rspb.Release) bool {
		return release.Name == name
	}

	releases, err := helmListReleases(s.driver, helm.ListOpts{Namespace: namespace, Filter: filter})
	if err != nil {
		return nil, err
	}

	if len(releases) == 1 {
		return releases[0], nil
	}

	return nil, errors.New("release not found")
}

func (s *Service) ListReleases(ctx context.Context, req *connect.Request[addonv1.ListReleasesRequest]) (*connect.Response[addonv1.ListReleasesResponse], error) {
	releases, err := helmListReleases(s.driver, helm.ListOpts{})
	if err != nil {
		return nil, err
	}

	results := make([]*addonv1.Release, len(releases))

	for i := 0; i < len(releases); i++ {
		results[i] = mapRelease(releases[i], nil)
	}

	return connect.NewResponse(&addonv1.ListReleasesResponse{
		Releases: results,
	}), nil
}

func (s *Service) GetRelease(ctx context.Context, req *connect.Request[addonv1.GetReleaseRequest]) (*connect.Response[addonv1.GetReleaseResponse], error) {
	release, err := s.findRelease(req.Msg.Name, req.Msg.Namespace)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	results, err := addons.List()
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	var addon *addons.Addon

	for _, result := range results {
		if result.Namespace == release.Namespace &&
			result.Chart.Name == release.Chart.Metadata.Name {
			addon = &result
			break
		}
	}

	return connect.NewResponse(&addonv1.GetReleaseResponse{
		Release: mapRelease(release, addon),
	}), nil
}

func (s *Service) InstallAddon(ctx context.Context, req *connect.Request[addonv1.InstallAddonRequest]) (*connect.Response[addonv1.InstallAddonResponse], error) {
	addon, err := addons.Get(req.Msg.Name)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	values := map[string]interface{}{}

	for name, value := range req.Msg.Values {
		values[name] = value
	}

	installer := addons.NewInstaller(s.kube)
	if err := installer.SimpleInstall(addon, values); err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	return connect.NewResponse(&addonv1.InstallAddonResponse{
		Namespace: addon.Namespace,
	}), nil
}

func (s *Service) UpgradeAddon(ctx context.Context, req *connect.Request[addonv1.UpgradeAddonRequest]) (*connect.Response[addonv1.UpgradeAddonResponse], error) {
	addon, err := addons.Get(req.Msg.Name)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	helmClient, err := helm.NewClient(addon.Namespace, s.kube)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, fmt.Errorf("failed to create helm client: %w", err))
	}

	if _, err := helmClient.Upgrade(
		addon.Name,
		addon.ChartName(),
		nil,
	); err != nil {
		return nil, connect.NewError(connect.CodeInternal, fmt.Errorf("failed to uninstall helm chart: %w", err))
	}

	return connect.NewResponse(&addonv1.UpgradeAddonResponse{Namespace: addon.Namespace}), nil
}

func (s *Service) UninstallAddon(ctx context.Context, req *connect.Request[addonv1.UninstallAddonRequest]) (*connect.Response[addonv1.UninstallAddonResponse], error) {
	addon, err := addons.Get(req.Msg.Name)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	helmClient, err := helm.NewClient(addon.Namespace, s.kube)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, fmt.Errorf("failed to create helm client: %w", err))
	}

	if _, err := helmClient.Uninstall(addon.Name); err != nil {
		return nil, connect.NewError(connect.CodeInternal, fmt.Errorf("failed to uninstall helm chart: %w", err))
	}

	return connect.NewResponse(&addonv1.UninstallAddonResponse{Namespace: addon.Namespace}), nil
}

func (s *Service) ListAddons(ctx context.Context, req *connect.Request[addonv1.ListAddonsRequest]) (*connect.Response[addonv1.ListAddonsResponse], error) {
	results, err := addons.List()
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	output := make([]*addonv1.Addon, len(results))

	for i := 0; i < len(results); i++ {
		output[i] = mapAddon(&results[i])
	}

	return connect.NewResponse(&addonv1.ListAddonsResponse{Addons: output}), nil
}
