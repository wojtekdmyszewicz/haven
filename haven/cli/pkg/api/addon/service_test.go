package addon

import (
	"context"
	"errors"
	"testing"

	"github.com/bufbuild/connect-go"
	"github.com/stretchr/testify/assert"
	rspb "helm.sh/helm/v3/pkg/release"
	"helm.sh/helm/v3/pkg/storage/driver"

	addonv1 "gitlab.com/commonground/haven/haven/haven/cli/gen/pkg/api/addon/v1"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
)

type fakeHelm struct {
	releases []*rspb.Release
	err      error
	calls    []helm.ListOpts
}

func (f *fakeHelm) List(driver driver.Driver, opts helm.ListOpts) ([]*rspb.Release, error) {
	f.calls = append(f.calls, opts)

	return f.releases, f.err
}

func TestListReleases(t *testing.T) {
	t.Run("calls the helm-client correctly", func(t *testing.T) {
		mock := &fakeHelm{}
		helmListReleases = mock.List
		svc := NewService(nil, kubernetes.NewFakeClientset())
		response, err := svc.ListReleases(context.Background(), connect.NewRequest(&addonv1.ListReleasesRequest{}))

		assert.NoError(t, err)
		assert.Empty(t, response.Msg.Releases)

		assert.Equal(t, []helm.ListOpts{{}}, mock.calls)
	})

	t.Run("returns an error on failure", func(t *testing.T) {
		mock := &fakeHelm{err: errors.New("random error")}
		svc := NewService(nil, kubernetes.NewFakeClientset())
		helmListReleases = mock.List
		response, err := svc.ListReleases(context.Background(), connect.NewRequest(&addonv1.ListReleasesRequest{}))

		assert.Error(t, err)
		assert.Nil(t, response)
	})
}

func TestGetRelease(t *testing.T) {
	t.Run("calls the helm-client correctly", func(t *testing.T) {
		mock := &fakeHelm{}
		svc := NewService(nil, kubernetes.NewFakeClientset())
		helmListReleases = mock.List
		response, err := svc.GetRelease(context.Background(), connect.NewRequest(&addonv1.GetReleaseRequest{
			Name:      "test",
			Namespace: "test-ns",
		}))

		assert.Error(t, err)
		assert.Nil(t, response)
		assert.Len(t, mock.calls, 1)
		assert.Equal(t, "test-ns", mock.calls[0].Namespace)
		assert.NotEmpty(t, mock.calls[0].Filter)
	})

	t.Run("returns an error on failure", func(t *testing.T) {
		mock := &fakeHelm{err: errors.New("random error")}
		svc := NewService(nil, kubernetes.NewFakeClientset())
		helmListReleases = mock.List
		response, err := svc.GetRelease(context.Background(), connect.NewRequest(&addonv1.GetReleaseRequest{
			Name:      "test",
			Namespace: "test-ns",
		}))

		assert.Error(t, err)
		assert.Nil(t, response)
	})
}
