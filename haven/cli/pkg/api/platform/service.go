package platform

import (
	"context"
	"fmt"

	"github.com/bufbuild/connect-go"
	platformv1 "gitlab.com/commonground/haven/haven/haven/cli/gen/pkg/api/platform/v1"
	"gitlab.com/commonground/haven/haven/haven/cli/gen/pkg/api/platform/v1/platformv1connect"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/compliancy"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
)

var _ platformv1connect.PlatformServiceHandler = (*Service)(nil)

type Service struct {
	kube *kubernetes.Clientset
}

func NewService(kube *kubernetes.Clientset) *Service {
	return &Service{
		kube: kube,
	}
}

func (s *Service) GetPlatform(ctx context.Context, in *connect.Request[platformv1.GetPlatformRequest]) (*connect.Response[platformv1.GetPlatformResponse], error) {
	platform, err := compliancy.BestEffortPlatformCheck(s.kube)
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	version, err := s.kube.Discovery().ServerVersion()
	if err != nil {
		return nil, connect.NewError(connect.CodeInternal, err)
	}

	return connect.NewResponse(&platformv1.GetPlatformResponse{
		Platform: &platformv1.Platform{
			Name:              platform.DeterminedPlatform,
			KubernetesVersion: fmt.Sprintf("%s.%s", version.Major, version.Minor),
		},
	}), nil
}
