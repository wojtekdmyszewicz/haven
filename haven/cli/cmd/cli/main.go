// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2

package main

import (
	"flag"
	"os"

	"github.com/gookit/color"
	cli "github.com/jawher/mow.cli"
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/addons"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/compliancy"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/dashboard"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

var (
	version = "undefined"
)

func main() {
	app := cli.App("haven", "")
	app.ErrorHandling = flag.ExitOnError

	logging.LogFile = app.StringOpt("l log-file", "", "Write logs to file (optional)")
	logging.OutputFormat = app.StringOpt("o output", "text", "Specify output format (text, json)")

	app.Before = func() {
		if "text" != *logging.OutputFormat && "json" != *logging.OutputFormat {
			logging.Fatal("Invalid output format: '%s'. Try 'text' or 'json'\n", *logging.OutputFormat)
		}

		logging.Info(color.Bold.Sprintf("Haven %s - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n", version))

		compliancy.Version = version
	}

	app.Command("check", "Runs compliancy checks", compliancy.CLIConfig)
	app.Command("addons", "Manages addons on a cluster", addons.CLIConfig)
	app.Command("dashboard", "Starts the Haven dashboard", dashboard.CLIConfig)
	app.Command("version", "Prints the Haven CLI version", func(cmd *cli.Cmd) {
		cmd.Action = func() {}
	})

	if err := app.Run(os.Args); err != nil {
		logging.Error("Running app: %s", err)
		os.Exit(1)
	}
}
