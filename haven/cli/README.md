# Haven CLI

The Haven CLI can be used to validate an existing cluster on Haven Compliancy and to manage installation of addons.

## Usage
Try `haven --help`.

Global arguments:
- Enable logging to file with `--log-file <path>`. Logs may include debug level information not printed to screen.
- Specify the output format using `--output <text|json>`. By default output will is set to text. (Currently applies to the Compliancy Checker only)

In general when you output to JSON it's recommended to enable logging to file as well, especially with longer execution times needed by the Compliancy Checker. By tailing the logs you'll see progress during execution.

You can check the current version of your Haven CLI using `haven version`.

### Compliancy Checker
Haven Compliancy Checker runs against existing Kubernetes clusters, checking for Haven Compliancy by executing multiple checks.

See `haven check --help`.

- Opt out of external CNCF checks with `--cncf=false`. *CNCF checks are required for Haven Compliancy*.
- Opt in to external CIS benchmark checks with `--cis`. *CIS checks are optional and not required for Haven Compliancy*.
- Opt in to external Kubescape benchmark checks with `--kubescape`. *Kubescape checks are optional and not required for Haven Compliancy*.
- Opt out of the '+1 runs of Haven Compliancy Checker' message to standaardenregister with `--telemetry=false`. *Telemetry is not required for Haven Compliancy*.

Some examples:
- Recommended standard run: `haven check`.
- Every option made explicit: `haven --output text --log-file output.log check --cncf --cis=false --kubescape=false --telemetry`.
- Output results in JSON format for automation purposes: `haven --output json check`.
- Print to screen the rationale behind the checks and exit: `haven check --rationale`.

### Addons
Community addons are an easy but optional way to deploy basics like logging on your Haven cluster, by using the Haven CLI.

See `haven addons --help`.

For example installing the monitoring addon can be done using: `haven addons install monitoring`.
The Haven CLI will interactively guide the user through required configuration parameters like providing a hostname.

Addons are not required for Haven Compliancy.

### Dashboard
The Haven dashboard allows easy installation of Helm charts through a web interface. Deployments are managed by [Flux](https://toolkit.fluxcd.io/).

Start the dashboard with `haven dashboard`.

Haven Dashboard is part of the Community addons and is not required for Haven Compliancy.

## Development

### Libraries
Kubernetes go libraries are more easily maintained using a script to populate the initial go.mod. When in need to start over, proceed like this:

```bash
rm -rf go.mod go.sum
go mod init gitlab.com/commonground/haven/haven/haven/cli
./initmodules.sh v1.21.4  # Kubernetes version. Stay in line with the version compliancy check: minor release -2.
go mod tidy
```

### CRD's
Haven makes use of a CRD called `compliancy` (plural `complancies`). Each of these resources holds information about a previously ran `haven check` and can be consulted to gain more insight into these.

They are also available in code for your usage as a package.

`../haven/cli/pkg/kubernetes/models/crds/haven`

Currently available version:

1. `v1alpha`

Inside of this package you will find both the definition and the resource itself. Of importance here is to note how to install the compliancy resource to be used in your cluster.

Each struct within the CR needs to be annotated and generated, they also need a metav1.TypeMeta and metav1.ListMeta as they become K8s objects.

The annotation:

`// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object`

This makes sure `controller-gen` can generate kubernetes runtime Objects that can then be used in code. The current generated code is stored in `zz_generated.deepcopy.go`. This file should never be changed manually as there are helpers to generate this for you.

For usage please follow this guideline:

1. Install controller-gen bin by running: `go get sigs.k8s.io/controller-tools/cmd/controller-gen`
2. Generating the helpers methods: `go generate ...` (a helper can be found in `compliancy.go` that uses the controller-gen to generate the deepcopy file)
3. Alternativily you can run `//go:generate controller-gen object paths=$GOFILE` which can be found in `compliancy.go`
4. You can create the definition in your cluster. See `haven/cli/pkg/crd/haven.go` for a working example

You can now use the CRD from code.

Since you have the CRD available to you, you can also use the created interface to communicate with the objects. This can be found in:
`../haven/cli/pkg/kubernetes/crd.go`

Example usage (assumes you have created the definition and resource definition in your kuberentes cluster).

```golang
package main

import (
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"fmt"
	"io/ioutil"
	"k8s.io/client-go/tools/clientcmd"
)

}
func main() {
	// kubeConfig should be the *rest.Config of your own kubeconfig
	config, err := ioutil.ReadFile("YOURHOMEDIR/.kube/config")
	if err != nil {
		fmt.Println(err)
	}

	c, err := clientcmd.NewClientConfigFromBytes(config)
	if err != nil {
		fmt.Println(err)
	}

	restConfig, err := c.ClientConfig()
	if err != nil {
		fmt.Println(err)
	}

	crdClient, err := kubernetes.NewCrdConfig(restConfig)
	if err != nil {
		fmt.Println(err)
	}

	compliancies, err := crdClient.List()
	if err != nil {
		fmt.Println(err)
	}

	for _, compliancy := range compliancies.Items {
		fmt.Println(compliancy.Name)
	}
}
```


## License
Copyright © VNG Realisatie 2019-2023
Licensed under the EUPL
