terraform {
  required_version = "=1.0.1"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.66.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "=2.3.2"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "=2.2.0"
    }
  }
}
