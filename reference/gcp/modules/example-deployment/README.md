# Example deployment

Deploys an example Nginx Hello World deployment to the cluster.

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| *deployment_name* | Example deployment name | <code title="">string</code> |  | <code title="">nginx-test</code> |
| *namespace* | Namespace for example deployment | <code title="">string</code> |  | <code title="">example</code> |
| *nginx_container* | Nginx container to use | <code title="">string</code> |  | <code title="">nginxinc/nginx-unprivileged:latest</code> |

## Outputs

<!-- END TFDOC -->
