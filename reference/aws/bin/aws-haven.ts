import { App, Aspects } from "aws-cdk-lib";
import { KubernetesVersion } from "aws-cdk-lib/aws-eks";
import { AwsSolutionsChecks } from "cdk-nag";
import { AddonStack } from "../lib/stacks/addon.stack";
import {
  Ami,
  Cidr,
  ClusterStack,
  EksApiAccessType,
} from "../lib/stacks/cluster.stack";
import { InfraStack } from "../lib/stacks/infra.stack";

/**
 * Region to deploy the EKS cluster to
 */
const region = "eu-central-1";

/**
 * The security configuration of the EKS API endpoint. The options are
 *
 * `public`
 * The cluster endpoint is accessible from outside of your VPC.
 * Worker node traffic will leave your VPC to connect to the endpoint.
 *
 * `private`
 * The cluster endpoint is only accessible through your VPC.
 * Worker node traffic to the endpoint will stay within your VPC.
 *
 * `public_and_private`
 * The cluster endpoint is accessible from outside of your VPC.
 * Worker node traffic to the endpoint will stay within your VPC.
 *
 * Notes:
 * - When choosing `public_and_private`, make sure to also configure
 *   `eksApiAccessCidr` to the IPs that you want to allow to access the API.
 *   You must ensure that the CIDR blocks that you specify include the addresses
 *   that worker nodes and Fargate pods (if you use them) access the public endpoint from.
 * - We recommend `private` mode for maximum security. To run the haven checker in private
 *   mode, you can create an EC2 instance in the VPC and run the check inside
 *   of that instance.
 */
const eksApiType: EksApiAccessType = "<please select>";

/**
 * Enter your public IPs in CIDR format here.
 * This is used to protect the K8s public endpoint that
 * is needed to run kubectl from your local machine
 *
 * If you prefer to set the endpoint to private, you can change the
 * infrastructure code in `eks-stack.ts` accordingly. In private mode,
 * you will have to run `kubectl` inside the VPC, for example
 * in an EC2 instance.
 *
 * IMPORTANT: Due to a bug in CDK, the IP may not update after
 * the initial deployment. If this is the case, go to the AWS
 * console (EKS > Clusters > (your cluster) > Networking > Manage networking)
 * and update your access CIDR manually.
 *
 * This value is only used when `eksApiType` is set to `public_and_private`.
 *
 * @default [0.0.0.0/32] Block all access
 */
const eksApiAccessCidrs: Cidr[] = ["0.0.0.0/32"];

/**
 * Version of K8s to be used
 *
 * Note: If you change the K8s version, you must also
 * change the `cdk8s-plus-21` package to the correct version.
 *
 * @see https://cdk8s.io/docs/latest/
 * @see https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html
 */
const version = KubernetesVersion.V1_21;

/**
 * Ubuntu for EKS from Canonical
 *
 * Select the appropriate AMI from the "Ubuntu on Amazon Elastic Kubernetes Service (EKS)" catalog
 * - Filter for the correct Kubernetes version (see `version`)
 * - Find the correct deployment region in the list
 * - Select the AMD64 AMI ID (starts with `ami-`)
 * - Optional: Create a copy with encrypted volumes (see README)
 *
 * @see https://cloud-images.ubuntu.com/docs/aws/eks/
 */
const clusterNodeAmi: Ami = "<please configure>";

/**
 * The root node of the infrastructure tree
 */
const app = new App();

/**
 * Apply the standard AWS infrastructure security checker
 */
Aspects.of(app).add(new AwsSolutionsChecks());

/**
 * The CloudFormation stack containing the VPC and EFS
 */
const { vpc, envelopeEncryptionKey } = new InfraStack(app, "HavenInfraStack", {
  env: { region },
});

/**
 * The CloudFormation stack containing the EKS cluster
 */
const { cluster, ubuntuNodeGroup } = new ClusterStack(app, "HavenClusterStack", {
  env: { region },
  vpc,
  clusterNodeAmi,
  clusterName: "haven", // Rename here if needed, must be unique per account+region
  version,
  envelopeEncryptionKey,
  eksApiAccessCidrs,
  eksApiType,
});

/**
 * The CloudFormation stack containing various security related addons
 */
new AddonStack(app, "HavenAddonStack", {
  env: { region },
  cluster,
  nodegroup: ubuntuNodeGroup,
});
