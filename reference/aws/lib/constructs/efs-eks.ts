import {
  ISecurityGroup,
  IVpc,
  Peer,
  Port,
  SecurityGroup,
} from "aws-cdk-lib/aws-ec2";
import { AccessPoint, FileSystem } from "aws-cdk-lib/aws-efs";
import { IGrantable, PolicyStatement } from "aws-cdk-lib/aws-iam";
import { NagSuppressions } from "cdk-nag";
import { Construct, IConstruct } from "constructs";

export interface EfsForEksProps {
  vpc: IVpc;
}

/**
 * Contains an EFS volume that is configured for use by EKS
 */
export class EfsForEks extends Construct {
  readonly efsFileSystem: FileSystem;
  readonly efsSecurityGroup: SecurityGroup;
  readonly efsAccessPoint: AccessPoint;

  get volumeHandle() {
    return `${this.efsFileSystem.fileSystemId}::${this.efsAccessPoint.accessPointId}`;
  }

  constructor(scope: IConstruct, id: string, props: EfsForEksProps) {
    super(scope, id);

    this.efsSecurityGroup = new SecurityGroup(this, "HavenEfsSecurityGroup", {
      vpc: props.vpc,
      description: "EKS EFS Security Group",
    });

    this.efsFileSystem = new FileSystem(this, "HavenEfs", {
      vpc: props.vpc,
      securityGroup: this.efsSecurityGroup,
      encrypted: true,
    });

    this.efsAccessPoint = new AccessPoint(this, "HavenEfsAp", {
      fileSystem: this.efsFileSystem,
    });
  }

  grantSecurityGroupAccess(securityGroup: ISecurityGroup) {
    this.efsSecurityGroup.addIngressRule(
      Peer.securityGroupId(securityGroup.securityGroupId),
      Port.tcp(2049),
      "EKS EFS node access"
    );
  }

  /**
   * Designed to grant an EKS service account access to the EFS volume
   * via the EFS CSI driver
   *
   * @param grantee The service account
   */
  grantAccess(grantee: IGrantable & IConstruct) {
    grantee.grantPrincipal.addToPrincipalPolicy(
      new PolicyStatement({
        actions: [
          "elasticfilesystem:DescribeAccessPoints",
          "elasticfilesystem:DescribeFileSystems",
        ],
        resources: ["*"],
      })
    );

    grantee.grantPrincipal.addToPrincipalPolicy(
      new PolicyStatement({
        actions: ["elasticfilesystem:CreateAccessPoint"],
        resources: ["*"],
        conditions: {
          StringLike: {
            "aws:RequestTag/efs.csi.aws.com/cluster": "true",
          },
        },
      })
    );

    grantee.grantPrincipal.addToPrincipalPolicy(
      new PolicyStatement({
        actions: ["elasticfilesystem:DeleteAccessPoint"],
        resources: ["*"],
        conditions: {
          StringEquals: {
            "aws:ResourceTag/efs.csi.aws.com/cluster": "true",
          },
        },
      })
    );

    NagSuppressions.addResourceSuppressions(
      grantee,
      [
        {
          id: "AwsSolutions-IAM5",
          reason:
            "Star permissions are used for discovery and access which is gated by resource and request tags.",
        },
      ],
      true
    );
  }
}
