import { ICluster, KubernetesManifest } from "aws-cdk-lib/aws-eks";
import { Construct, IConstruct } from "constructs";
import * as yaml from "js-yaml";
import * as request from "sync-request";

export interface MultiManifestProps {
  cluster: ICluster;
  manifestUrl: string;
  manifestPrefix: string;
  dependsOnManifest?: KubernetesManifest;
}

export class MultiManifest extends Construct {
  constructor(scope: IConstruct, id: string, props: MultiManifestProps) {
    super(scope, id);
    const manifests: Record<string, unknown>[] = [];
    yaml.loadAll(
      request.default("GET", props.manifestUrl).getBody().toString(),
      (doc) => {
        manifests.push(doc as Record<string, unknown>);
      }
    );

    let previousManifest: KubernetesManifest | undefined =
      props.dependsOnManifest;

    manifests.forEach((manifest, idx) => {
      const manifestResource = props.cluster.addManifest(
        `${props.manifestPrefix}-${idx}`,
        manifest
      );

      if (previousManifest) {
        manifestResource.node.addDependency(previousManifest);
      }

      previousManifest = manifestResource;
    });
  }
}
