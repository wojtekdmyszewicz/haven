import { ApiObject, Chart, Size } from "cdk8s";
import {
  PersistentVolumeAccessMode,
  PersistentVolumeClaim,
} from "cdk8s-plus-21";
import { IConstruct } from "constructs";
import { EfsForEks } from "../constructs/efs-eks";

export interface EfsVolumeChartProps {
  efs: EfsForEks;
}

export class EfsVolumeChart extends Chart {
  constructor(scope: IConstruct, id: string, props: EfsVolumeChartProps) {
    super(scope, id);

    const efsStorageClass = new ApiObject(this, "EfsStorageClass", {
      kind: "StorageClass",
      apiVersion: "storage.k8s.io/v1",
      metadata: {
        name: "efs-sc",
      },
      provisioner: "efs.csi.aws.com",
    });

    const efsVolume = new ApiObject(this, "EfsVolume", {
      apiVersion: "v1",
      kind: "PersistentVolume",
      metadata: {
        name: "efs-pv",
      },
      spec: {
        capacity: {
          storage: "5Gi",
        },
        volumeMode: "Filesystem",
        accessModes: ["ReadWriteMany"],
        persistentVolumeReclaimPolicy: "Retain",
        storageClassName: "efs-sc",
        csi: {
          driver: "efs.csi.aws.com",
          volumeHandle: props.efs.volumeHandle,
        },
      },
    });

    efsVolume.addDependency(efsStorageClass);

    new PersistentVolumeClaim(this, "EfsClaim", {
      metadata: {
        name: "efs-claim",
      },
      volume: efsVolume,
      accessModes: [PersistentVolumeAccessMode.READ_WRITE_MANY],
      storageClassName: "efs-sc",
      storage: Size.gibibytes(5),
    });
  }
}
