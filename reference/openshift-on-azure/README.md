# Reference Implementation: Haven on Azure

## OpenShift on Azure
Haven can easily be deployed on Azure using [OpenShift](https://www.openshift.com).

## How it works
Follow the steps from the [online documentation](https://haven.commonground.nl/techniek/aan-de-slag/openshift-op-azure) or have a look at the [ansible code](./).

When ready ensure your new cluster is Haven Compliant by using the [Haven Compliancy Checker](../../haven/cli/README.md).

## Addons
Please look into the [Haven CLI](../../haven/cli/README.md) for addons management.

## License
Copyright © VNG Realisatie 2019-2023
Licensed under the EUPL
