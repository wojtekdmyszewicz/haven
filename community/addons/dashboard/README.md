# Dashboard

The Haven Dashboard allows administrators to manage Helm charts on their cluster from a user-friendly user interface.

Haven Dashboard is part of the Community addons and is not required for Haven Compliancy.

![Screenshot of the Haven Dashboard](/uploads/852c5c4d02bbbda7e8ee6897f19aac46/image.png)

## Using the dashboard

See [../../haven/cli/README.md](../cli/README.md) for instructions how to use the dashboard.

## Setup development environment

Start a proxy to the Kubernetes API with:

```bash
kubectl proxy
```

Finally use the following commands to start the UI:

```bash
npm install
npm start
```

Go to https://localhost:3000/ to view the dashboard and start developing.

## License
Copyright © VNG Realisatie 2019-2023
Licensed under the EUPL
