// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { string } from 'prop-types'
import { useParams, useHistory } from 'react-router-dom'
import { Alert, Button, Drawer } from '@commonground/design-system'
import { useTranslation } from 'react-i18next'
import usePromise from '../../hooks/use-promise'
import LoadingMessage from '../../components/LoadingMessage'
import services from '../../services'
import {
  StyledSpecList,
  StyledActionsBar,
  StyledRemoveButton,
} from './index.styles'

const AddonDetailPage = ({ parentUrl }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const { namespace, name } = useParams()
  const [isRemoved, setIsRemoved] = useState(false)
  const [isUpgraded, setIsUpgraded] = useState(false)
  const { isReady, error, result } = usePromise(
    services.addons.getRelease,
    namespace,
    name,
  )

  const handleRemove = async () => {
    if (window.confirm(t('Do you want to remove the addon?'))) {
      await services.addons.uninstall(name)
      setIsRemoved(true)
    }
  }

  const handleUpgrade = async () => {
    if (window.confirm(t('Do you want to upgrade the addon?'))) {
      await services.addons.upgrade(name)
      setIsUpgraded(true)
    }
  }

  return (
    <Drawer noMask closeHandler={() => history.push(parentUrl)}>
      <Drawer.Header title={name} closeButtonLabel={t('Close')} />
      <Drawer.Content>
        {!isReady || (!error && !result) ? (
          <LoadingMessage />
        ) : error ? (
          <Alert variant="error">
            {t('Failed to load the addon.', { name })}
          </Alert>
        ) : isRemoved ? (
          <Alert variant="success">{t('The addon has been removed.')}</Alert>
        ) : isUpgraded ? (
          <Alert variant="success">{t('The addon has been upgraded.')}</Alert>
        ) : result ? (
          <>
            <StyledActionsBar>
              <Button onClick={handleUpgrade}>{t('Upgrade addon')}</Button>
              <StyledRemoveButton onClick={handleRemove} />
            </StyledActionsBar>

            <StyledSpecList alignValuesRight>
              <StyledSpecList.Item
                title={t('Name')}
                value={result.release.name}
              />
              <StyledSpecList.Item
                title={t('Namespace')}
                value={result.release.namespace}
              />
            </StyledSpecList>

            <h3>{t('Chart')}</h3>
            <StyledSpecList alignValuesRight>
              <StyledSpecList.Item
                title={t('Name')}
                value={result.release.chart.name}
              />
              <StyledSpecList.Item
                title={t('Version')}
                value={result.release.chart.version}
              />
            </StyledSpecList>

            {result.release.chart.sources && (
              <>
                <h3>{t('Source')}</h3>
                <StyledSpecList alignValuesRight>
                  {result.release.chart.sources.map((source, index) => (
                    <StyledSpecList.Item
                      key={index}
                      title={t('Name')}
                      value={source}
                    />
                  ))}
                </StyledSpecList>
              </>
            )}
          </>
        ) : null}
      </Drawer.Content>
    </Drawer>
  )
}

AddonDetailPage.propTypes = {
  parentUrl: string,
}

export default AddonDetailPage
