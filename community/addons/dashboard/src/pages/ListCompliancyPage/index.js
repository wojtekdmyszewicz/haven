// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'
import { Route } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Alert } from '@commonground/design-system'
import Table from '../../components/Table'
import PageTemplate from '../../components/PageTemplate'
import EmptyContentMessage from '../../components/EmptyContentMessage'
import LoadingMessage from '../../components/LoadingMessage'
import usePromise from '../../hooks/use-promise'
import services from '../../services'
import CompliancyDetailPage from '../../pages/CompliancyDetailPage'
import Spec from './Spec'
import { StyledActionsBar, StyledCount, StyledAmount } from './index.styles'

const CompliancyRow = ({ metadata, spec }) => (
  <Table.Tr to={`/compliancies/${metadata.name}`} name={metadata.name}>
    <Table.Td>{metadata.name}</Table.Td>
    <Table.Td>{spec.output.version}</Table.Td>
    <Table.Td>{spec ? <Spec spec={spec} /> : null}</Table.Td>
  </Table.Tr>
)

CompliancyRow.propTypes = {
  metadata: object,
  spec: object,
}

const ListCompliancyPage = () => {
  const { t } = useTranslation()
  const { isReady, error, result, reload } = usePromise(
    services.compliancies.list,
  )

  return (
    <PageTemplate>
      <PageTemplate.Header title={t('Compliancies')} />

      <StyledActionsBar>
        <StyledCount>
          <StyledAmount>{result ? result.items.length : 0}</StyledAmount>
          <small>{t('Compliancies')}</small>
        </StyledCount>
      </StyledActionsBar>

      {!isReady ? (
        <LoadingMessage />
      ) : error ? (
        <Alert variant="error">{t('Failed to load compliancies.')}</Alert>
      ) : result != null && result.items.length === 0 ? (
        <EmptyContentMessage>
          {t('There are no compliancies yet.')}
        </EmptyContentMessage>
      ) : result ? (
        <Table withLinks role="grid">
          <thead>
            <Table.TrHead>
              <Table.Th>{t('Name')}</Table.Th>
              <Table.Th>{t('Version')}</Table.Th>
              <Table.Th>{t('Compliant')}</Table.Th>
            </Table.TrHead>
          </thead>
          <tbody>
            {result.items.map((compliancies, i) => (
              <CompliancyRow key={i} {...compliancies} />
            ))}
          </tbody>
        </Table>
      ) : null}
      <Route path="/compliancies/:name">
        <CompliancyDetailPage
          parentUrl="/compliancies"
          refreshHandler={reload}
        />
      </Route>
    </PageTemplate>
  )
}

export default ListCompliancyPage
