// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const StyledWrapper = styled.span`
  line-height: 0;
  vertical-align: bottom;
`
