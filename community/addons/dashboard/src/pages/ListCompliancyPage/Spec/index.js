// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'
import { useTranslation } from 'react-i18next'

import { ReactComponent as IconStatusUp } from './status-up.svg'
import { ReactComponent as IconStatusDown } from './status-down.svg'
import { StyledWrapper } from './index.styles'

const Spec = ({ spec }) => {
  const { t } = useTranslation()
  return (
    <StyledWrapper>
      {
        {
          true: <IconStatusUp title={t('Compliant')} />,
          false: <IconStatusDown title={t('Not Compliant')} />,
        }[spec.compliant]
      }
    </StyledWrapper>
  )
}

Spec.propTypes = {
  spec: object,
}

export default Spec
