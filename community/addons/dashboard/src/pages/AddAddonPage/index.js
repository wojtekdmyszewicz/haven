// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import { Alert } from '@commonground/design-system'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import AddonForm from '../../components/AddonForm'
import PageTemplate from '../../components/PageTemplate'
import services from '../../services'

const AddAddonPage = () => {
  const { t } = useTranslation()
  const [isProcessing, setIsProcessing] = useState(false)
  const [isAdded, setIsAdded] = useState(false)
  const [error, setError] = useState(null)

  const submit = async ({ name, questions }) => {
    setError(false)
    setIsProcessing(true)

    try {
      await services.addons.install(name, questions)
      setIsAdded(true)
    } catch (e) {
      setError(true)
      console.error(e)
    }

    setIsProcessing(false)
  }

  return (
    <PageTemplate>
      <PageTemplate.HeaderWithBackNavigation
        backButtonTo="/addons"
        title={t('Add addon')}
      />

      {error ? (
        <Alert title={t('Failed adding addon')} variant="error">
          {error}
        </Alert>
      ) : null}

      {isAdded && !error ? (
        <Alert variant="success">{t('The addon has been added.')}</Alert>
      ) : null}

      {!isAdded ? (
        <AddonForm
          submitButtonText="Add"
          onSubmitHandler={submit}
          disableForm={isProcessing}
        />
      ) : null}
    </PageTemplate>
  )
}

export default AddAddonPage
