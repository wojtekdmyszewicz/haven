// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { string } from 'prop-types'
import { useParams, useHistory } from 'react-router-dom'
import { Alert, Drawer } from '@commonground/design-system'
import { useTranslation } from 'react-i18next'
import usePromise from '../../hooks/use-promise'
import LoadingMessage from '../../components/LoadingMessage'
import services from '../../services'
import {
  StyledActionsBar,
  StyledRemoveButton,
  StyledSpecList,
} from './index.styles'

const CompliancyDetailPage = ({ parentUrl }) => {
  const { t } = useTranslation()
  const history = useHistory()

  const { name } = useParams()

  const [isRemoved, setIsRemoved] = useState(false)
  const { isReady, error, result } = usePromise(services.compliancies.get, name)

  const handleRemove = () => {
    if (window.confirm(t('Do you want to remove the compliancy?'))) {
      services.compliancies.delete(name)
      setIsRemoved(true)
    }
  }

  return (
    <Drawer noMask closeHandler={() => history.push(parentUrl)}>
      <Drawer.Header title={name} closeButtonLabel={t('Close')} />
      <Drawer.Content>
        {!isReady || (!error && !result) ? (
          <LoadingMessage />
        ) : error ? (
          <Alert variant="error">
            {t('Failed to load the compliancy.', { name })}
          </Alert>
        ) : isRemoved ? (
          <Alert variant="success">
            {t('The compliancy has been removed.')}
          </Alert>
        ) : result ? (
          <>
            <StyledActionsBar>
              <StyledRemoveButton onClick={handleRemove} />
            </StyledActionsBar>

            <StyledSpecList alignValuesRight>
              <StyledSpecList.Item
                title={t('Name')}
                value={result.metadata.name}
              />
              <StyledSpecList.Item
                title={t('CRD Version')}
                value={result.apiVersion}
              />
              <StyledSpecList.Item
                title={t('Haven Version')}
                value={result.spec.output.version}
              />
              <h3>{t('Spec')}</h3>
              <StyledSpecList.Item
                title={t('Created')}
                value={result.spec.created}
              />
              <StyledSpecList.Item
                title={t('Compliant')}
                value={result.spec.compliant.toString()}
              />
              <h3>{t('Input')}</h3>
              <StyledSpecList.Item
                title={t('Platform')}
                value={result.spec.input.platform.determinedPlatform}
              />
              <StyledSpecList.Item
                title={t('Proxy')}
                value={result.spec.input.platform.proxy.toString()}
              />
              <StyledSpecList.Item
                title={t('CLI')}
                value={result.spec.input.commandline}
              />
              <h3>{t('Summary')}</h3>
              <StyledSpecList.Item
                title={t('Total')}
                value={result.spec.output.compliancyChecks.summary.total}
              />
              <StyledSpecList.Item
                title={t('Passed')}
                value={result.spec.output.compliancyChecks.summary.passed}
              />
              <StyledSpecList.Item
                title={t('Failed')}
                value={result.spec.output.compliancyChecks.summary.failed}
              />
              <StyledSpecList.Item
                title={t('Skipped')}
                value={result.spec.output.compliancyChecks.summary.skipped}
              />
              <h3>{t('Config')}</h3>
              <StyledSpecList.Item
                title={t('CIS')}
                value={result.spec.output.config.CIS.toString()}
              />
              <StyledSpecList.Item
                title={t('CNCF')}
                value={result.spec.output.config.CNCF.toString()}
              />
            </StyledSpecList>
            <h3>{t('Results')}</h3>
            {result.spec.output.compliancyChecks.results.map(
              (innerResult, index) => (
                <StyledSpecList alignValuesRight key={index}>
                  <h3>{innerResult.name}</h3>
                  <StyledSpecList.Item
                    title={t('Category')}
                    value={innerResult.category}
                  />
                  <StyledSpecList.Item
                    title={t('Label')}
                    value={innerResult.label}
                  />
                  <StyledSpecList.Item
                    title={t('Rationale')}
                    value={innerResult.rationale}
                  />
                  <StyledSpecList.Item
                    title={t('Result')}
                    value={innerResult.result}
                  />
                </StyledSpecList>
              ),
            )}
          </>
        ) : null}
      </Drawer.Content>
    </Drawer>
  )
}

CompliancyDetailPage.propTypes = {
  parentUrl: string,
}

export default CompliancyDetailPage
