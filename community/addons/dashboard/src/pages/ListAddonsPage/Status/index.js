// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { boolean } from 'prop-types'
import { useTranslation } from 'react-i18next'

import { ReactComponent as IconStatusUp } from './status-up.svg'
import { ReactComponent as IconStatusDown } from './status-down.svg'
import { StyledWrapper } from './index.styles'

const Status = ({ deployed }) => {
  const { t } = useTranslation()

  return (
    <StyledWrapper>
      {deployed ? (
        <IconStatusUp title={t('Deployed')} />
      ) : (
        <IconStatusDown title={t('Not deployed')} />
      )}
    </StyledWrapper>
  )
}

Status.propTypes = {
  deployed: boolean,
}

export default Status
