// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import { Alert, Button } from '@commonground/design-system'
import { boolean, string } from 'prop-types'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Route, Link } from 'react-router-dom'
import EmptyContentMessage from '../../components/EmptyContentMessage'
import LoadingMessage from '../../components/LoadingMessage'
import PageTemplate from '../../components/PageTemplate'
import Table from '../../components/Table'
import useInterval from '../../hooks/use-interval'
import usePromise from '../../hooks/use-promise'
import services from '../../services'
import AddonDetailPage from '../AddonDetailPage'
import Status from './Status'
import {
  StyledActionsBar,
  StyledAmount,
  StyledCount,
  StyledIconPlus,
} from './index.styles'

const AddonRow = ({ name, namespace, deployed }) => (
  <Table.Tr to={`/addons/${namespace}/${name}`} name={name}>
    <Table.Td>{name}</Table.Td>
    <Table.Td>{namespace}</Table.Td>
    <Table.Td>{deployed ? <Status deployed={deployed} /> : null}</Table.Td>
  </Table.Tr>
)

AddonRow.propTypes = {
  name: string,
  namespace: string,
  deployed: boolean,
}

const ListAddonsPage = () => {
  const { t } = useTranslation()
  const { isReady, error, result, reload } = usePromise(
    services.addons.listReleases,
  )

  useInterval(() => {
    reload()
  }, 2000)

  return (
    <PageTemplate>
      <PageTemplate.Header title={t('Addons')} />

      <StyledActionsBar>
        <StyledCount>
          <StyledAmount>{result ? result.releases?.length : 0}</StyledAmount>
          <small>{t('Addons')}</small>
        </StyledCount>
        <Button as={Link} to="/addons/add" aria-label={t('Add addon')}>
          <StyledIconPlus />
          {t('Add addon')}
        </Button>
      </StyledActionsBar>

      {!isReady ? (
        <LoadingMessage />
      ) : error ? (
        <Alert variant="error">{t('Failed to load addons.')}</Alert>
      ) : result != null && result.releases?.length === 0 ? (
        <EmptyContentMessage>
          {t('There are no addons yet.')}
        </EmptyContentMessage>
      ) : result ? (
        <Table withLinks role="grid">
          <thead>
            <Table.TrHead>
              <Table.Th>{t('Name')}</Table.Th>
              <Table.Th>{t('Namespace')}</Table.Th>
              <Table.Th>{t('Status')}</Table.Th>
            </Table.TrHead>
          </thead>
          <tbody>
            {result.releases &&
              result.releases.map((release, i) => (
                <AddonRow key={i} {...release} />
              ))}
          </tbody>
        </Table>
      ) : null}
      <Route path="/addons/:namespace/:name">
        <AddonDetailPage parentUrl="/addons" refreshHandler={reload} />
      </Route>
    </PageTemplate>
  )
}

export default ListAddonsPage
