// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { string } from 'prop-types'
import { Link, useParams, useHistory, useLocation } from 'react-router-dom'
import { Alert, Drawer } from '@commonground/design-system'
import { useTranslation } from 'react-i18next'
import usePromise from '../../hooks/use-promise'
import LoadingMessage from '../../components/LoadingMessage'
import EditButton from '../../components/EditButton'
import services from '../../services'
import {
  StyledActionsBar,
  StyledRemoveButton,
  StyledSpecList,
} from './index.styles'

const RepositoryDetailPage = ({ parentUrl }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const location = useLocation()

  const { namespace, name } = useParams()

  const [isRemoved, setIsRemoved] = useState(false)
  const { isReady, error, result } = usePromise(
    services.helmRepositories.get,
    namespace,
    name,
  )

  const handleRemove = () => {
    if (window.confirm(t('Do you want to remove the repository?'))) {
      services.helmRepositories.delete(namespace, name)
      setIsRemoved(true)
    }
  }

  return (
    <Drawer noMask closeHandler={() => history.push(parentUrl)}>
      <Drawer.Header title={name} closeButtonLabel={t('Close')} />
      <Drawer.Content>
        {!isReady || (!error && !result) ? (
          <LoadingMessage />
        ) : error ? (
          <Alert variant="error">
            {t('Failed to load the repository.', { name })}
          </Alert>
        ) : isRemoved ? (
          <Alert variant="success">
            {t('The repository has been removed.')}
          </Alert>
        ) : result ? (
          <>
            <StyledActionsBar>
              <EditButton as={Link} to={`${location.pathname}/edit`} />
              <StyledRemoveButton onClick={handleRemove} />
            </StyledActionsBar>

            <StyledSpecList alignValuesRight>
              <StyledSpecList.Item
                title={t('Name')}
                value={result.metadata.name}
              />
              <StyledSpecList.Item
                title={t('Namespace')}
                value={result.metadata.namespace}
              />
              <StyledSpecList.Item title={t('URL')} value={result.spec.url} />
            </StyledSpecList>

            <h3>{t('Status')}</h3>

            <StyledSpecList alignValuesRight>
              {result.status.conditions.map((condition, i) => (
                <StyledSpecList.Item
                  key={i}
                  title={condition.lastTransitionTime}
                  value={`${condition.message} (${condition.reason})`}
                />
              ))}
            </StyledSpecList>
          </>
        ) : null}
      </Drawer.Content>
    </Drawer>
  )
}

RepositoryDetailPage.propTypes = {
  parentUrl: string,
}

export default RepositoryDetailPage
