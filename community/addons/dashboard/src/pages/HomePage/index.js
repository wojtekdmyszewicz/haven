// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Alert } from '@commonground/design-system'
import PageTemplate from '../../components/PageTemplate'
import Card from '../../components/Card'
import usePromise from '../../hooks/use-promise'
import services from '../../services'
import LoadingMessage from '../../components/LoadingMessage'
import EmptyContentMessage from '../../components/EmptyContentMessage'
import { StyledCards } from './index.styles'

const HomePage = () => {
  const { t } = useTranslation()
  const { isReady, error, result } = usePromise(services.compliancies.getLatest)
  const compliantComponents = []
  const failedText = 'Some checks on components have failed'
  const passedText = 'All checks on all components passed'
  let overallCompliant = true
  let compliancyLink = '/compliancies/'
  const hidden = true

  if (isReady && result && !error) {
    compliantComponents.push(result.spec.compliant)
    compliancyLink += result.metadata.name
    result.spec.created = new Date(Date.parse(result.spec.created))
      .toLocaleDateString()
      .slice(0, 10)
  }

  let index
  for (index in compliantComponents) {
    if (!compliantComponents[parseInt(index)]) {
      overallCompliant = false
    }
  }

  return (
    <PageTemplate>
      <PageTemplate.Header title={t('Haven')} />
      {/* NOTE: hide this for now until we have multiple status */}
      <h1 hidden={hidden}>
        {overallCompliant ? (
          <Alert variant="success" title="Status">
            {t(passedText)}
          </Alert>
        ) : (
          <Alert variant="warning" title={failedText} />
        )}
      </h1>
      <h2>Status</h2>
      {!isReady ? (
        <LoadingMessage />
      ) : error ? (
        <Alert variant="error">{t('Failed to load compliancies.')}</Alert>
      ) : result == null ? (
        <EmptyContentMessage>
          {t('There are no compliancies yet.')}
        </EmptyContentMessage>
      ) : result ? (
        <StyledCards>
          <Card result={result} link={compliancyLink} />
        </StyledCards>
      ) : null}
    </PageTemplate>
  )
}

export default HomePage
