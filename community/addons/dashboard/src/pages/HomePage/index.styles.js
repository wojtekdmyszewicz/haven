// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//

import styled from 'styled-components'

export const StyledActionsBar = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
`
export const StyledCards = styled.div`
  padding-top: 1em;
  display: flex;
  gap: 1.5em;
  flex-wrap: wrap;
  justify-content: left;
`
