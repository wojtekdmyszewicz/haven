// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { Icon } from '@commonground/design-system'

import { ReactComponent as SvgChevronRight } from './chevron-right.svg'

export const IconChevronRight = (props) => (
  <Icon as={SvgChevronRight} {...props} />
)
