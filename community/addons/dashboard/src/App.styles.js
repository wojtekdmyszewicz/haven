// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledContainer = styled.div`
  height: 100%;
`
