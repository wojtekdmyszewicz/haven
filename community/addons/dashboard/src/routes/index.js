// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import AddAddonPage from '../pages/AddAddonPage'
import AddNamespacePage from '../pages/AddNamespacePage'
import HomePage from '../pages/HomePage'
import ListAddonsPage from '../pages/ListAddonsPage'
import ListCompliancyPage from '../pages/ListCompliancyPage'
import ListNamespacesPage from '../pages/ListNamespacesPage'
import NotFoundPage from '../pages/NotFoundPage'

const Routes = () => {
  return (
    <Switch>
      <Redirect exact path="/" to="/haven" />
      <Route path="/haven" component={HomePage} />
      <Route path="/addons/add" component={AddAddonPage} />
      <Route path="/addons" component={ListAddonsPage} />
      <Route path="/namespaces/add" component={AddNamespacePage} />
      <Route path="/namespaces" component={ListNamespacesPage} />
      <Route path="/compliancies" component={ListCompliancyPage} />
      <Route path="*" component={NotFoundPage} />
    </Switch>
  )
}

export default Routes
