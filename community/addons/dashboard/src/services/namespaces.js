// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
export class NamespacesService {
  async list() {
    const result = await fetch('/api/v1/namespaces')
    if (!result.ok) {
      throw new Error('error while listing namespaces')
    }

    return result.json()
  }

  async get(name) {
    const result = await fetch(`/api/v1/namespaces/${name}`)
    if (!result.ok) {
      throw new Error('error while getting namespace')
    }

    return result.json()
  }

  async create(namespace) {
    const result = await fetch(`/api/v1/namespaces`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(namespace),
    })

    if (!result.ok) {
      throw new Error('error while creating namespace')
    }

    return result.json()
  }

  async delete(name) {
    const result = await fetch(`/api/v1/namespaces/${name}`, {
      method: 'DELETE',
    })
    if (!result.ok) {
      throw new Error('error while deleting namespace')
    }

    return result.json()
  }
}
