// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import AddonService from './addons'
import { NamespacesService } from './namespaces'
import { CompliancyService } from './compliancies'

const services = {
  addons: new AddonService(),
  namespaces: new NamespacesService(),
  compliancies: new CompliancyService(),
}

export default services
