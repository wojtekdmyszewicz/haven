// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
export class CompliancyService {
  async list() {
    const result = await fetch(
      '/apis/haven.commonground.nl/v1alpha1/compliancies',
    )
    if (!result.ok) {
      throw new Error('error while listing compliancies')
    }

    return result.json()
  }

  async getLatest() {
    const result = await fetch(
      '/apis/haven.commonground.nl/v1alpha1/compliancies',
    )

    if (!result.ok) {
      throw new Error('error while listing compliancies')
    }

    const res = await result.json()
    let latest = res.items[0]
    let index
    for (index in res.items) {
      const compliancyTimeNew = Date.parse(
        res.items[parseInt(index)].spec.created,
      )
      const compliancyTimeCurrent = Date.parse(latest.spec.created)
      if (compliancyTimeNew > compliancyTimeCurrent) {
        latest = res.items[parseInt(index)]
      }
    }

    return latest
  }

  async get(name) {
    const result = await fetch(
      `/apis/haven.commonground.nl/v1alpha1/compliancies/${name}`,
    )
    if (!result.ok) {
      throw new Error('error while getting compliancy')
    }

    const res = await result.json()

    if (res.spec.input.commandline.length > 80) {
      const cli = res.spec.input.commandline
      const splitCli = cli.split(' ')
      let command = ''
      let found = false
      for (const element of splitCli) {
        if (element === 'check') {
          found = true
          command += 'haven '
        }

        if (found) {
          command += element
        }
      }

      res.spec.input.commandline = command
    }

    return res
  }

  async delete(name) {
    const result = await fetch(
      `/apis/haven.commonground.nl/v1alpha1/compliancies/${name}`,
      {
        method: 'DELETE',
      },
    )
    if (!result.ok) {
      throw new Error('error while deleting compliancy')
    }

    return result.json()
  }
}
