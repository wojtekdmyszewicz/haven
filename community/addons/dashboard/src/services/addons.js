// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
function connectRequest(method, body) {
  return fetch(`/pkg.api.addon.v1.AddonService/${method}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body || {}),
  })
}

export default class AddonService {
  async listReleases() {
    const result = await connectRequest('ListReleases')
    if (!result.ok) {
      throw new Error('error occured while getting releases list')
    }

    return result.json()
  }

  async list() {
    const result = await connectRequest('ListAddons')
    if (!result.ok) {
      throw new Error('error occured while getting addons list')
    }

    return result.json()
  }

  async getRelease(namespace, name) {
    const result = await connectRequest('GetRelease', {
      name,
      namespace,
    })
    if (!result.ok) {
      throw new Error('error occured while getting release')
    }

    return result.json()
  }

  async install(name, values) {
    const result = await connectRequest('InstallAddon', {
      name,
      values,
    })
    if (!result.ok) {
      throw new Error('error occured while installing addon')
    }

    return result.json()
  }

  async uninstall(name) {
    const result = await connectRequest('UninstallAddon', { name })
    if (!result.ok) {
      throw new Error('error occured while uninstalling addon')
    }

    return result.json()
  }

  async upgrade(name) {
    const result = await connectRequest('UpgradeAddon', { name })
    if (!result.ok) {
      throw new Error('error occured while upgrading addon')
    }

    return result.json()
  }
}
