// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { createProxyMiddleware } = require('http-proxy-middleware')

const getProxyUrl = (proxy) => proxy || 'http://localhost:8001/' // The command `kubectl proxy` listens to 8001 by default

module.exports = function (app) {
  app.use(
    '/api',
    createProxyMiddleware({ target: getProxyUrl(process.env.PROXY) }),
  )
  app.use(
    '/apis',
    createProxyMiddleware({ target: getProxyUrl(process.env.PROXY) }),
  )
}
