// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { PropTypes, string } from 'prop-types'
import { useTranslation } from 'react-i18next'
import { ReactComponent as IconStatusUp } from './icons/status-ok.svg'
import { ReactComponent as IconStatusDown } from './icons/status-failed.svg'
import {
  StyledCard,
  StyledButton,
  StyledHeader,
  StyledRow,
} from './index.styles'

export const Card = ({ result, link }) => {
  const { t } = useTranslation()
  return (
    <StyledCard>
      <StyledHeader>
        <p>Compliancy Checker</p>

        <div>
          {result.spec.compliant ? (
            <IconStatusUp fill="#45C15F" />
          ) : (
            <IconStatusDown fill="#FF4141" />
          )}
        </div>
      </StyledHeader>

      {result.spec?.created && (
        <StyledRow>
          <strong>{t('Uitgevoerd')}</strong>{' '}
          <span>{t(result.spec.created)}</span>
        </StyledRow>
      )}

      {result.spec.output.version && (
        <StyledRow>
          <strong>{t('Versie')}</strong>
          <span>{result.spec.output.version}</span>
        </StyledRow>
      )}

      <StyledRow>
        <strong>{t('Resultaten')}</strong>{' '}
        <span>
          {result.spec.output.compliancyChecks.summary.passed} /{' '}
          {result.spec.output.compliancyChecks.summary.total}{' '}
          {t('checks geslaagd')}
        </span>
      </StyledRow>

      <StyledButton as="a" href={link}>
        {t('Details')}
      </StyledButton>
    </StyledCard>
  )
}

Card.propTypes = {
  // TODO: Use imported result type!
  // eslint-disable-next-line react/forbid-prop-types
  result: PropTypes.any,
  link: string,
}

export default Card
