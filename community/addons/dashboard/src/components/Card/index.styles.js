// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Button, mediaQueries } from '@commonground/design-system'

export const StyledCard = styled.div`
  background-color: #565656;
  box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.25);
  border-radius: 0.5em;
  color: ${(p) => p.theme.tokens.colorPaletteGray50};
  display: flex;
  flex-direction: column;
  padding: ${(p) => p.theme.tokens.spacing06};
  transition: width 0.3s;
  width: 30rem;

  ${mediaQueries.smDown`
    margin: ${(p) => p.theme.tokens.spacing05};
    padding: ${(p) => p.theme.tokens.spacing05};
    width: 25rem;
  `}
`

export const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: ${(p) => p.theme.tokens.spacing05};

  > p {
    font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
    color: #ffffff;
    margin: 0;
    line-height: 125%;
    font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
  }

  > div {
    height: 28px;

    > svg {
      width: 100%;
      height: 100%;
    }
  }
`

export const StyledRow = styled.p`
  display: flex;
  justify-content: space-between;
  margin: 0;

  > * {
    max-width: 60%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  :last-of-type {
    margin-bottom: ${(p) => p.theme.tokens.spacing05};
  }
`

export const StyledIcon = styled.svg`
  width: ${(p) => p.theme.tokens.spacing07};
  height: ${(p) => p.theme.tokens.spacing07};
  display: block;
  margin: 0 auto;
  color: white;
`

export const StyledButton = styled(Button)`
  margin-top: auto;
  margin-right: auto;
`
