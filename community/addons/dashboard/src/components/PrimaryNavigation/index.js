// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { useTranslation } from 'react-i18next'
import {
  StyledHomeLink,
  StyledLink,
  StyledIcon,
  StyledNav,
} from './index.styles'

import { ReactComponent as IconLogo } from './icons/haven.svg'
import { ReactComponent as IconAddons } from './icons/addons.svg'
import { ReactComponent as IconNamespaces } from './icons/namespaces.svg'
import { ReactComponent as IconCompliancies } from './icons/compliancies.svg'

const PrimaryNavigation = () => {
  const { t } = useTranslation()
  return (
    <StyledNav>
      <StyledHomeLink to="/" title={t('Dashboard')} aria-label={t('Dashboard')}>
        <StyledIcon as={IconLogo} />
        {t('Haven')}
      </StyledHomeLink>

      <StyledLink to="/addons" title={t('Addons')} aria-label={t('Addons')}>
        <StyledIcon as={IconAddons} />
        {t('Addons')}
      </StyledLink>

      <StyledLink
        to="/namespaces"
        title={t('Namespaces')}
        aria-label={t('Namespaces')}
      >
        <StyledIcon as={IconNamespaces} />
        {t('Namespaces')}
      </StyledLink>

      <StyledLink
        to="/compliancies"
        title={t('Compliancies')}
        aria-label={t('Compliancies')}
      >
        <StyledIcon as={IconCompliancies} />
        {t('Compliancies')}
      </StyledLink>
    </StyledNav>
  )
}

export default PrimaryNavigation
