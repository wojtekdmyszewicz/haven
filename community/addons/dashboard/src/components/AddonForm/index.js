// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { bool, object, func, string } from 'prop-types'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'
import {
  Button,
  Fieldset,
  Alert,
  Select,
  TextInput,
} from '@commonground/design-system'
import LoadingMessage from '../../components/LoadingMessage'
import usePromise from '../../hooks/use-promise'
import services from '../../services'

import { Form, Question } from './index.styles'

const DEFAULT_INITIAL_VALUES = {}

const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  questions: Yup.object(),
})

const AddonForm = ({
  initialValues,
  disableForm,
  onSubmitHandler,
  submitButtonText,
}) => {
  const { t } = useTranslation()
  const [questions, setQuestions] = useState([])
  const { isReady, error, result } = usePromise(services.addons.list)
  const options =
    isReady && !error
      ? result.addons.map((addon) => ({
          label: addon.description,
          value: addon.name,
        }))
      : []

  function updateAddon(option) {
    const addon =
      result.addons.filter((addon) => addon.name === option.value)[0] || null

    if (!addon) {
      return
    }

    const questions = addon.questions || []

    setQuestions(questions)
  }

  function submit(values) {
    const defaultQuestionValues = {}

    questions
      .filter((question) => !!question.default)
      .forEach((question) => {
        defaultQuestionValues[question.name] = question.default
      })

    onSubmitHandler({
      name: values.name,
      questions: {
        ...defaultQuestionValues,
        ...values.questions,
      },
    })
  }

  return (
    <>
      {!isReady || (!error && !result) ? (
        <LoadingMessage />
      ) : error ? (
        <Alert variant="error">{t('Failed to load available addons.')}</Alert>
      ) : result ? (
        <Formik
          initialValues={{ ...DEFAULT_INITIAL_VALUES, ...initialValues }}
          validationSchema={validationSchema}
          onSubmit={submit}
        >
          {({ handleSubmit, handleChange, handleBlur, values }) => (
            <Form onSubmit={handleSubmit}>
              <Fieldset>
                <Select
                  name="name"
                  size="l"
                  options={options}
                  disabled={!!initialValues}
                  onChange={updateAddon}
                >
                  {t('Name')}
                </Select>
              </Fieldset>

              {questions.map((question) => (
                <Question key={question.name}>
                  <TextInput
                    type="text"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    name={`questions.${question.name}`}
                    value={
                      (values.questions && values.questions[question.name]) ||
                      question.default
                    }
                  >
                    {question.message}
                    {question.help && (
                      <p>
                        <small>{question.help}</small>
                      </p>
                    )}
                  </TextInput>
                </Question>
              ))}

              <Button type="submit" disabled={disableForm}>
                {submitButtonText}
              </Button>
            </Form>
          )}
        </Formik>
      ) : null}
    </>
  )
}

AddonForm.propTypes = {
  initialValues: object,
  disableForm: bool,
  onSubmitHandler: func,
  submitButtonText: string,
}

export default AddonForm
