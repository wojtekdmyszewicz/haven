// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { bool, object, func, string } from 'prop-types'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'
import { Button, Fieldset, TextInput } from '@commonground/design-system'

import { Form } from './index.styles'

// DO NOT use any of these values as they are just placeholders
const DEFAULT_INITIAL_VALUES = {
  apiVersion: 'haven.commonground.nl/v1alpha1',
  metadata: {
    name: 'hallo-haven',
  },
  spec: {
    compliant: true,
    created: '2022-01-31 11:01:20.016006 +0000 UTC',
    input: {
      commandline: 'haven check',
      platform: {
        determinedPlatform: 'Microsoft Azure AKS',
        determiningLabel:
          'kubernetes.azure.com/node-image-version\u003dAKSUbuntu-1804gen2containerd-2021.09.03',
        proxy: true,
      },
    },
    output: {
      compliancyChecks: {
        results: [
          {
            category: 'Fundamental',
            label: 'Self test: does HCC have cluster-admin',
            name: 'clusteradmin',
            rationale:
              'In order for the Haven Compliancy Checker to function properly elevated privileges are required.',
            result: 'YES',
          },
          {
            category: 'Infrastructure',
            label: 'Multiple availability zones in use',
            name: 'multiaz',
            rationale:
              'Running a cluster on a single availability zone means a higher risk of downtime when that single zone runs into problems.',
            result: 'YES',
          },
          {
            category: 'Infrastructure',
            label: 'Running at least 3 master nodes',
            name: 'hamasters',
            rationale: 'This ensures a highly available control plane.',
            result: 'YES',
          },
          {
            category: 'Infrastructure',
            label: 'Running at least 3 worker nodes',
            name: 'haworkers',
            rationale: 'This enables running highly available workloads.',
            result: 'YES',
          },
          {
            category: 'Infrastructure',
            label: 'Nodes have SELinux, Grsecurity, AppArmor or LKRG enabled',
            name: 'nodehardening',
            rationale:
              'Security matters on every layer of a system and should an attacker break out of a deployment onto a node increased node security will help prevent further escalation.',
            result: 'YES',
          },
          {
            category: 'Infrastructure',
            label: 'Private networking topology',
            name: 'privatenetworking',
            rationale:
              'Not directly exposing masters or workers to the public internet can increase the security of the cluster.',
            result: 'YES',
          },
          {
            category: 'Cluster',
            label:
              'Kubernetes version is latest stable or max 2 minor versions behind',
            name: 'kubernetesversion',
            rationale:
              'This allows cluster users to access new functionality quickly and encourages a well-implemented update mechanism.',
            result: 'YES',
          },
          {
            category: 'Cluster',
            label: 'Role Based Access Control is enabled',
            name: 'rbac',
            rationale:
              'Basic security option which is enabled by default in order to control who can do what on a cluster.',
            result: 'YES',
          },
          {
            category: 'Cluster',
            label: 'Basic auth is disabled',
            name: 'basicauth',
            rationale:
              'Basic authentication is hard to maintain. We encourage to use OpenID Connect for user authentication.',
            result: 'YES',
          },
          {
            category: 'Cluster',
            label: 'ReadWriteMany persistent volumes support',
            name: 'rwxvolumes',
            rationale:
              'This ensures that storage can be created which can be used by highly available deployments.',
            result: 'YES',
          },
          {
            category: 'Cluster',
            label: 'LoadBalancer service type support',
            name: 'loadbalancers',
            rationale:
              'This ensures that Layer 4 loadbalancers can automatically be created from the Kubernetes API.',
            result: 'YES',
          },
          {
            category: 'External',
            label: 'CNCF Kubernetes Conformance',
            name: 'cncf',
            rationale:
              'Cloud Native Computing Foundation\u0027s Kubernetes checks ensure the Kubernetes cluster adheres to the standard Kubernetes API\u0027s.',
            result: 'YES',
          },
          {
            category: 'Deployment',
            label: 'Automated HTTPS certificate provisioning',
            name: 'autocerts',
            rationale:
              'This makes it easy for engineers to expose an Ingress with a valid SSL certificate which automatically renews.',
            result: 'YES',
          },
          {
            category: 'Deployment',
            label: 'Log aggregation is running',
            name: 'logs',
            rationale:
              'In order to be in control of the workload on a cluster it\u0027s mandatory to aggregate all container logs.',
            result: 'YES',
          },
          {
            category: 'Deployment',
            label: 'Metrics-server is running',
            name: 'metrics',
            rationale:
              'In order to be in control of a cluster it\u0027s mandatory to have eyes an ears on the cluster resources.',
            result: 'YES',
          },
        ],
        summary: {
          failed: 0,
          passed: 15,
          skipped: 0,
          total: 15,
          unknown: 0,
        },
      },
      config: {
        CIS: true,
        CNCF: true,
      },
      havenCompliant: true,
      startTS: '2022-01-31T11:58:53.194597+01:00',
      stopTS: '2022-01-31T12:01:20.014644+01:00',
      suggestedChecks: {
        results: [
          {
            category: 'Deployment',
            label: 'Haven Dashboard is provisioned',
            name: 'dashboard',
            rationale:
              'The community dashboard from Haven allows for an easy way to manage deployments.',
            result: 'NO',
          },
          {
            category: 'External',
            label: 'CIS Kubernetes Security Benchmark',
            name: 'cis',
            rationale:
              'Center for Internet Security\u0027s Kubernetes benchmark assists in following the best practices of securing a cluster.',
            result: 'YES',
          },
        ],
      },
      version: 'Haven demo-version',
    },
    version: 'Haven v1alpha1',
  },
}

const validationSchema = Yup.object().shape({
  metadata: Yup.object().shape({
    name: Yup.string().required(),
    namespace: Yup.string().required(),
  }),
})

const CompliancyForm = ({
  initialValues,
  disableForm,
  onSubmitHandler,
  submitButtonText,
}) => {
  const { t } = useTranslation()

  return (
    <Formik
      initialValues={{ ...DEFAULT_INITIAL_VALUES, ...initialValues }}
      validationSchema={validationSchema}
      onSubmit={onSubmitHandler}
    >
      {({ values, handleSubmit }) => (
        <Form onSubmit={handleSubmit}>
          <Fieldset>
            <TextInput name="metadata.name" size="l" disabled={!!initialValues}>
              {t('Name')}
            </TextInput>
            <TextInput
              name="metadata.namespace"
              size="l"
              disabled={!!initialValues}
            >
              {t('Namespace')}
            </TextInput>
          </Fieldset>

          <Fieldset>
            <TextInput name="spec.chart.spec.chart" size="l">
              {t('Chart')}
            </TextInput>

            <TextInput name="spec.chart.spec.version" size="l">
              {t('Version')}
            </TextInput>

            <TextInput name="spec.chart.spec.sourceRef.name" size="l">
              {t('Repository name')}
            </TextInput>

            <TextInput name="spec.chart.spec.sourceRef.namespace" size="l">
              {t('Repository namespace')}
            </TextInput>
          </Fieldset>

          <Button type="submit" disabled={disableForm}>
            {submitButtonText}
          </Button>
        </Form>
      )}
    </Formik>
  )
}

CompliancyForm.propTypes = {
  initialValues: object,
  disableForm: bool,
  onSubmitHandler: func,
  submitButtonText: string,
}

export default CompliancyForm
