// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  html,
  body,
  #root {
    height: 100vh;
  }

  #root {
    display: flex;
    flex-direction: column;
  }

  button,
  a {
    border: 2px solid transparent;

    &:focus {
      outline: none;
      border-color: ${(p) => p.theme.colorBorderDropdownFocus};
    }
  }
  @media (max-width: 600px) {
    html {
      font-size: 12px;
    }
  }
`
