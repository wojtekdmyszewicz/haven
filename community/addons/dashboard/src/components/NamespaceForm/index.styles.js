// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Form = styled.form`
  margin-bottom: ${(p) => p.theme.tokens.spacing10};
`
