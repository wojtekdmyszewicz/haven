// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
import React from 'react'
import { bool, object, func, string } from 'prop-types'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'
import { Button, Fieldset, TextInput } from '@commonground/design-system'

import { Form } from './index.styles'

const DEFAULT_INITIAL_VALUES = {
  apiVersion: 'v1',
  kind: 'Namespace',
  metadata: {
    name: 'hallo-wereld',
  },
}

const validationSchema = Yup.object().shape({
  metadata: Yup.object().shape({
    name: Yup.string().required(),
  }),
})

const ReleaseForm = ({
  initialValues,
  disableForm,
  onSubmitHandler,
  submitButtonText,
}) => {
  const { t } = useTranslation()

  return (
    <Formik
      initialValues={{ ...DEFAULT_INITIAL_VALUES, ...initialValues }}
      validationSchema={validationSchema}
      onSubmit={onSubmitHandler}
    >
      {({ handleSubmit }) => (
        <Form onSubmit={handleSubmit}>
          <Fieldset>
            <TextInput name="metadata.name" size="l" disabled={!!initialValues}>
              {t('Name')}
            </TextInput>
          </Fieldset>

          <Button type="submit" disabled={disableForm}>
            {submitButtonText}
          </Button>
        </Form>
      )}
    </Formik>
  )
}

ReleaseForm.propTypes = {
  initialValues: object,
  disableForm: bool,
  onSubmitHandler: func,
  submitButtonText: string,
}

export default ReleaseForm
