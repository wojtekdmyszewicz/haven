// Copyright © VNG Realisatie 2019-2023
// Licensed under the EUPL
//
module.exports = {
  plugins: ['react', 'prettier'],
  extends: [],
  settings: {
    react: {
      version: 'detect',
    },
  },
  rules: {
    'react/react-in-jsx-scope': 'off',
  },
}
