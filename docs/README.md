# Documentation
This folder contains the online documentation of Haven.

Use the following commands to start the watch server:

```bash
npm install
npm run dev
```

Go to http://localhost:8000/ to view the documentation.

## License
Copyright © VNG Realisatie 2019-2023<br />
Licensed under EUPL v1.2
