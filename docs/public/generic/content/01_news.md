---
newsLink1Text: Technische documentatie
newsLink1Href: /techniek
newsLink2Text: Neem contact op
newsLink2Href: /contact
---

## Aan de slag met Haven?

In onze technische documentatie wordt de standaard toegelicht en beschreven hoe u Haven kunt installeren op uw huidige IT infrastructuur. Bovendien hebben we een handreiking programma van eisen beschikbaar gesteld om het inkopen van Haven te vereenvoudigen. Of neem contact met ons op, we helpen u graag op weg!
