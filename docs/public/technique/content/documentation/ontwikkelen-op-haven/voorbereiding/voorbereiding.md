---
title: "Voorbereiding"
path: "/voorbereiding"
---

Voordat je aan de slag kunt gaan heb je eerst toegang nodig tot een Haven cluster. De beheerder geeft je toegang en een link waarmee je kan inloggen. Zorg verder dat je [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) op je computer hebt geïnstalleerd voordat je verder gaat.

## Kubectl

Met kubectl kun je toegang krijgen tot het cluster via de command line.

1. Ga naar de inloglink die je van de beheerder hebt gekregen.
1. Log in met je account, daarna kom je terug op een pagina met instructies.
1. Voer de instructies uit om kubectl te configureren met het cluster.

Je kunt nu met kubectl verbinding maken met het cluster, probeer bijvoorbeeld maar eens het volgende om een lijst van alle nodes in het cluster te zien:

```bash
$ kubectl top nodes
```

![Schermafbeelding van het commando kubectl top nodes](/technique/content/documentation/ontwikkelen-op-haven/voorbereiding/kubectl-top-nodes.png)

## Kubernetes Dashboard

Het Kubernetes dashboard geeft je toegang tot het cluster via een grafische interface.

1. Volg eerst de instructies hierboven om kubectl te configureren.
2. Afhankelijk van je opgezette Kubernetes cluster kan het zijn dat er geen `dashboard` deployed is. Dit kun je controleren door:

    ```bash
    $ kubectl get pods --all-namespaces | grep dashboard
    ```
   
3. Wanneer het bovenstaande commando geen resultaat oplevert is het noodzakelijk eerst het `dashboard` te installeren. Zie daarvoor stap 4. Wanneer er wel resultaat is draait er al een `dashboard` ga dan naar stap 5.
4. Volg de instructies op https://github.com/kubernetes/dashboard.
5. Draai vervolgens het volgende commando om een proxy op te zetten naar het cluster:

    ```bash
    $ kubectl proxy
    ```

6. Zoek de namespace waar het `dashboard` draait en vervang in het commando bij stap 7. `${DASHBOARD_NAMESPACE}` door de naam van de namespace. Je kunt daarvoor het volgende commando gebruiken:
    
   ```bash
    $ kubectl get pods --all-namespaces | grep dashboard | awk '{ print $1 }'
    ```
   
7. Ga op je lokale machine naar de volgende link (en vervang ook hier de placeholder door de namespace): http://localhost:8001/api/v1/namespaces/${DASHBOARD_NAMESPACE}/services/https:kubernetes-dashboard:/proxy.

![Schermafbeelding van het Kubernetes dashboard](/technique/content/documentation/ontwikkelen-op-haven/voorbereiding/kubernetes-dashboard.png)

Je weet nu hoe je met kubectl verbinding kunt maken met het cluster en hoe je het Kubernetes dashboard kunt gebruiken om een grafische weergave van het cluster te krijgen. In de <a href="/techniek/hallo-wereld">volgende stap</a> rollen we een nieuwe applicatie uit op het cluster.

&ensp;

---

*De ontwikkelen op Haven pagina's dienen ter illustratie en staan los van Haven Compliancy*
