---
title: "Aan de slag"
path: "/aan-de-slag"
specialPage: "aan-de-slag"
description: "Gemeenten en leveranciers kunnen eenvoudig een Haven omgeving inrichten naar wens bij een (cloud) provider en/of on-premise. De nieuwe omgeving kan vervolgens worden getoetst met de Haven Compliancy Checker. Een groeiend aantal Referentie Implementaties maken het nog makkelijker om met Haven aan de slag te gaan."
filterTitle: Haven compliant oplossingen
filterOption1: Alles
filterOption2: Cloud
filterOption3: On-premise
itemSelectPlaceholder: Installeerbaar op alle platformen
items:
  - title: Amazon Elastic Kubernetes Service (EKS)
    environments: [Cloud]
    platforms: [Amazon Web Services]
    description: "Amazon Elastic Kubernetes Service (Amazon EKS) is a managed container service to run and scale Kubernetes applications in the cloud or on-premises."
    img: /technique/content/documentation/eenvoudig-beheer/aan-de-slag/aws/logo-aws.svg
    refUrl: /techniek/aan-de-slag/aws
    contactUrl: https://aws.amazon.com/eks/
  - title: Edgeless Systems Constellation
    environments: [Cloud]
    platforms: [Microsoft Azure, Google Cloud Platform, Amazon Web Services, OpenStack]
    description: "Constellation is the secure, always-encrypted Kubernetes that shields all clusters from the infrastructure and makes sure that cloud admins and hackers cannot access your data."
    img: /technique/content/documentation/eenvoudig-beheer/aan-de-slag/constellation/logo-edgeless.svg
    refUrl: /techniek/aan-de-slag/constellation
    contactUrl: https://www.edgeless.systems/products/constellation/
  - title: Google Kubernetes Engine (GKE)
    environments: [Cloud]
    platforms: [Google Cloud Platform]
    description: "Google Kubernetes Engine (GKE) is a managed, production-ready environment for running containerized applications."
    img: /technique/content/documentation/eenvoudig-beheer/aan-de-slag/google/logo-google.svg
    refUrl: /techniek/aan-de-slag/gcp
    contactUrl: https://cloud.google.com/kubernetes-engine
  - title: Microsoft Azure Kubernetes Service (AKS)
    environments: [Cloud]
    platforms: [Microsoft Azure]
    description: "Deploy and manage containerized applications more easily with a fully managed Kubernetes service."
    img: /technique/content/documentation/eenvoudig-beheer/aan-de-slag/azure/logo-azure.svg
    refUrl: /techniek/aan-de-slag/azure
    contactUrl: https://azure.microsoft.com/nl-nl/services/kubernetes-service/
  - title: Kops op OpenStack
    environments: [Cloud]
    platforms: [OpenStack]
    description: "Kops will not only help you create, destroy, upgrade and maintain production-grade, highly available, Kubernetes cluster, but it will also provision the necessary cloud infrastructure."
    img: /technique/content/documentation/eenvoudig-beheer/aan-de-slag/openstack/logo-openstack.svg
    refUrl: /techniek/aan-de-slag/openstack
    contactUrl: https://www.openstack.org/
  - title: Red Hat OpenShift
    environments: [Cloud, On-premise]
    platforms: [Red Hat OpenShift, Microsoft Azure, Amazon Web Services, Google Cloud Platform]
    description: Red Hat® OpenShift® is an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy.
    img: /technique/content/documentation/eenvoudig-beheer/aan-de-slag/openshift/logo-redhat.svg
    refUrl: /techniek/aan-de-slag/openshift
    contactUrl: https://www.redhat.com/en/technologies/cloud-computing/openshift
  - title: SUSE Rancher Kubernetes Engine (RKE)
    environments: [Cloud, On-premise]
    platforms: [Amazon Web Services, Microsoft Azure, OpenStack, VMware vSphere]
    description: "RKE is a CNCF-certified Kubernetes distribution that solves common installation complexities of Kubernetes by removing most host dependencies, presenting a stable path for deployment, upgrades & rollbacks."
    img: /technique/content/documentation/eenvoudig-beheer/aan-de-slag/rancher/logo-rancher.svg
    contactUrl: https://www.suse.com/products/suse-rancher/
  - title: VMware Tanzu Kubernetes Grid
    environments: [Cloud, On-premise]
    platforms: [VMware vSphere]
    description: Reliably deploy and run containerized workloads across private and public clouds.
    img: /technique/content/documentation/eenvoudig-beheer/aan-de-slag/vmware/logo-vmware.svg
    refUrl: /techniek/aan-de-slag/vmware
    contactUrl: https://www.vmware.com/
---

De benoeming van een oplossing op deze pagina betekent dat er is geverifieerd dat het mogelijk is om er een Haven Compliant omgeving op in te richten. Er worden geen voorkeuren en/of aanbevelingen mee uitgesproken. Deze lijst is niet volledig. Het indienen van een [Merge Request](https://gitlab.com/commonground/haven/haven/-/merge_requests) om een nieuwe oplossing voor te stellen wordt gewaardeerd. Bekijk hiervoor het [template](https://gitlab.com/commonground/haven/haven/-/blob/master/reference/template.md).

Gebruik van een [Referentie Implementatie](https://gitlab.com/commonground/haven/haven/-/tree/master/reference) is raadzaam, maar niet verplicht. Ook bij gebruik van een Referentie Implementatie en/of een van de benoemde oplossingen blijft het belangrijk om het cluster te valideren met de Haven Compliancy Checker. De specifieke inrichting bepaalt namelijk of het cluster echt voldoet aan de standaard.

&ensp;

---

*Gelukt? Tijd voor de [Haven Compliancy Checker](/techniek/compliancy-checker)!*
