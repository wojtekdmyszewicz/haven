---
title: "Dashboard"
path: "/dashboard"
---

Het Haven Dashboard biedt een gebruikersvriendelijke interface voor beheerders om eenvoudig installaties op een cluster te beheren. In deze sectie laten we zien hoe je het dashboard kunt gebruiken.

![Schermafbeelding van het Haven Dashboard](/technique/content/documentation/community/dashboard/schermafbeelding-dashboard.png)

## Installeren

Het dashboard is onderdeel van de Haven CLI. We bieden de Haven CLI aan voor meerdere besturingssystemen, waaronder macOS, Linux en Windows.

1. Download de [gewenste versie van de Haven CLI](https://gitlab.com/commonground/haven/haven/-/packages)
1. Pak de download uit (`unzip haven-v9.0.0-darwin-amd64.zip`)
1. Verplaats de binary in de uitgepakte map naar het gewenste pad (`mv darwin-amd64/haven /usr/local/bin/haven`)

Ook dient het cluster te beschikken over een installatie van Flux CD. Download de laatste versie van Flux met:

```bash
$ curl -O https://toolkit.fluxcd.io/install.sh
```

Controleer de inhoud van het script en installeer Flux op je lokale machine met:

```bash
$ sudo bash install.sh
```

Installeer vervolgens Flux op het cluster met:

```bash
$ flux install
```

## Het dashboard starten

Start vervolgens het dashboard met:

```bash
$ haven dashboard
```

Het dashboard opent automatisch in browser.

&ensp;

---

*De Community addons inclusief het Haven Dashboard staan los van Haven Compliancy en zijn niet verplicht.*
