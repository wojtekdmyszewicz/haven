## Publicaties

- Augustus 2022: [Amazon: AWS biedt nu een Haven compatible configuratie voor Kubernetes](https://ibestuur.nl/partner-aws/aws-biedt-nu-een-haven-compatibele-configuratie-voor-kubernetes)
- Juli 2022: [Wigo4it: Haven compliancy](https://www.wigo4it.nl/nieuws/haven-compliancy-bij-wigo4it/)
- April 2022: [VNG: Bestuur VNG verklaart Haven tot Standaard](https://vng.nl/nieuws/bestuur-vng-verklaart-haven-tot-standaard)
- Februari 2022: [VMware: ITQ: De gemeentelijke IT van de toekomst: morgen beginnen](https://itq.eu/nl/de-gemeentelijke-it-omgeving-van-de-toekomst-morgen-beginnen-commonground_haven/)
- Februari 2022: [Emerce: Software ontwikkelen voor gemeenten op één generieke infrastructuur](https://www.emerce.nl/cases/software-ontwikkelen-voor-gemeenten-op-een-generieke-infrastructuur)
- Januari 2022: [iBestuur: Haven maakt samen organiseren makkelijker](https://ibestuur.nl/partner-vng-realisatie/haven-maakt-samen-organiseren-makkelijker)
- Januari 2022: [Centric: Adoptie Common Ground-standaard Haven](https://www.centricpblo.nl/Common-Ground)
- Augustus 2021: [Microsoft: Tilburg and Equalit: establishing common ground between municipalities with cloud-based container technology](https://customers.microsoft.com/en-us/story/1409000171800990776-tilburg-government-azure-en-netherlands)
- Oktober 2020: [VNG Realisatie: Met Haven komt Common Ground tot leven](https://publicaties.vngrealisatie.nl/2020/e-magazine/10/met-haven-komt-common-ground-tot-leven/)
- Februari 2019: [Common Ground: Haven: platform-onafhankelijke cloud hosting](https://commonground.nl/cms/view/2f6018e6-8003-4960-9024-a3aea4325e32/haven)

Ontbreekt er een publicatie? [Laat het ons svp weten.](/contact)
