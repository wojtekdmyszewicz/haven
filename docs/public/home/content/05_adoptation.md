---
mainLinkText: ''
mainLinkHref: ''
adoptationText: ''
organizationImages:
  - src: home/content/utrecht.svg
    alt: Gemeente Utrecht
    url: https://utrecht.nl
  - src: home/content/equalit.svg
    alt: Equalit
    url: https://www.equalit.nl
  - src: home/content/wigo4it.svg
    alt: Wigo4it
    url: https://www.wigo4it.nl/
  - src: home/content/centric.svg
    alt: Centric
    url: https://www.centricpblo.nl/Common-Ground
# NOTE: maximum of three reviews
reviews:
  - quote: 'Samen organiseren betekent ook dat je als gemeenten zoveel mogelijk deelt. Het gebruiken van Haven vind ik een no-brainer: het is totaal beleidsarm en door het te gebruiken besparen we als gemeenten landelijk veel kosten die we door onnodige verschillen in onze IT-infrastructuur nu wel maken.'
    quotee: Mariëlle van der Zanden
    organization: Gemeente Utrecht - CIO
  - quote: 'De samenwerking rondom Haven is voor Microsoft een volgende stap in hoe we als bedrijfsleven samen met de overheid streven naar een verantwoorde, veilige en innovatieve overheid. Om te zorgen dat de dienstverlening richting de maatschappij optimaal kan profiteren van de voordelen van technologie.'
    quotee: Anna van den Breemer
    organization: Microsoft - Government lead
  - quote: 'Signalen is geoptimaliseerd voor Haven, op die manier creëren we een generiek landelijk platform waarop we Signalen eenvoudig beschikbaar maken voor gemeenten en actief bijdragen aan het Common Ground ecosysteem.'
    quotee: Jacco Brouwer
    organization: Signalen - Programmamanager
---

## Wie gebruikt het?

Steeds meer organisaties zijn aan de slag met Haven. Enkele voorbeelden:
