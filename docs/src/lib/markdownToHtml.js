// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { remark } from 'remark'
import html from 'remark-html'
import prism from 'remark-prism'

export default async function markdownToHtml(markdown) {
  const result = await remark()
    .use(html, { sanitize: false })
    .use(prism, { transformInlineCode: true })
    .process(markdown)

  return result.toString()
}
