// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { createMedia } from '@artsy/fresnel'
import defaultTheme from '@commonground/design-system/dist/themes/default'

const fresnel = createMedia({
  breakpoints: defaultTheme.breakpoints,
})

export const mediaStyles = fresnel.createMediaStyle()
export const { Media, MediaContextProvider: MediaProvider } = fresnel
