// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import defaultTheme from '@commonground/design-system/dist/themes/default'

const breakpoints = defaultTheme.breakpoints

const tokens = {
  ...defaultTheme.tokens,

  containerWidth: '992px',

  colors: {
    colorPaletteGray200: '#EEEEEE',
    colorPaletteGray300: '#E0E0E0',
  },
}

const theme = {
  ...defaultTheme,
  tokens,
  colorAlternateSection: '#f1f1f1',
  listIconSize: '2.5rem',
  colorCollapsibleBorder: tokens.colors.colorPaletteGray300,

  breakpoints: Object.values(breakpoints)
    .splice(1)
    .map((bp) => `${bp}px`),
}

export default theme
