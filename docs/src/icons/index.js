// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import IconBox from './box.svg'
import IconBuilding from './building.svg'
import IconCheck from './check.svg'
import IconExternalLink from './external-link.svg'
import IconFileCopy from './file-copy.svg'
import IconGitlab from './gitlab.svg'
import IconGroup from './group.svg'
import IconHammer from './hammer.svg'
import IconHome from './home.svg'
import IconInfo from './info.svg'
import IconLightning from './flashlight.svg'
import IconMail from './mail.svg'
import IconOpenArm from './open-arm.svg'
import IconPlug from './plug.svg'
import IconSpy from './spy.svg'
import IconTools from './tools.svg'
import IconEarth from './earth-line.svg'
import IconFileList from './file-list-3-line.svg'
import IconQuotationMark from './quotation-mark.svg'
import IconTable from './table.svg'
import IconCloseSyntax from './close-syntax.svg'
import IconMouseDrag from './mouse-drag.svg'
import IconSearchPerson from './search-person.svg'
import IconFlexible from './flexible.svg'
import IconEuro from './euro.svg'
import IconRecycle from './recycle.svg'
import IconFootsteps from './footsteps.svg'

const icons = {
  flexible: IconFlexible,
  euro: IconEuro,
  footsteps: IconFootsteps,
  closeSyntax: IconCloseSyntax,
  mouseDrag: IconMouseDrag,
  searchPerson: IconSearchPerson,
  table: IconTable,
  box: IconBox,
  building: IconBuilding,
  check: IconCheck,
  externalLInk: IconExternalLink,
  fileCopy: IconFileCopy,
  gitlab: IconGitlab,
  group: IconGroup,
  hammer: IconHammer,
  home: IconHome,
  info: IconInfo,
  lightning: IconLightning,
  mail: IconMail,
  openArm: IconOpenArm,
  plug: IconPlug,
  recycle: IconRecycle,
  spy: IconSpy,
  tools: IconTools,
  earth: IconEarth,
  fileList: IconFileList,
  quotationMark: IconQuotationMark,
}

export {
  IconBox,
  IconBuilding,
  IconCheck,
  IconExternalLink,
  IconFileCopy,
  IconGitlab,
  IconGroup,
  IconHammer,
  IconHome,
  IconInfo,
  IconLightning,
  IconMail,
  IconOpenArm,
  IconPlug,
  IconRecycle,
  IconSpy,
  IconTools,
  IconEarth,
  IconFileList,
  IconQuotationMark,
  IconFlexible,
  IconEuro,
  IconFootsteps,
  IconCloseSyntax,
  IconMouseDrag,
  IconSearchPerson,
  IconTable,
}

export const getIcon = (icon) => {
  if (Object.keys(icons).includes(icon)) {
    // Above check makes it input-safe, so ignore eslint
    // eslint-disable-next-line security/detect-object-injection
    return icons[icon]
  }

  if (!process.env.NEXT_PUBLIC_PRODUCTION) {
    console.warn(`Icon "${icon}" not found`)
  }

  return icons.tools
}
