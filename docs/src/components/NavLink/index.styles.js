// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import BaseIcon from '@commonground/design-system/dist/components/Icon'
import { IconExternalLink as ExternalLink } from '../../icons'

export const StyledIcon = styled(BaseIcon)`
  margin-left: ${(p) => p.theme.tokens.spacing03};
  margin-right: 0;
`

export const IconExternalLink = styled(ExternalLink)`
  width: ${(p) => p.theme.tokens.iconSizeSmall};
  height: ${(p) => p.theme.tokens.iconSizeSmall};
  transform: translateY(-2px);
  fill: ${(p) => p.theme.tokens.colorPaletteGray700};
`
