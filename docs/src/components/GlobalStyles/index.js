// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//

import { createGlobalStyle } from 'styled-components'
import { mobileNavigationHeight } from '@commonground/design-system/dist/components/PrimaryNavigation'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export default createGlobalStyle`
  html,body {
  min-height: 100%;
}

#__next {
  display: flex;
  flex-direction: column;
  min-height: 100vh;

  ${mediaQueries.smDown`
    min-height: calc(100vh - ${mobileNavigationHeight});
    margin-bottom: ${mobileNavigationHeight};
  `}
}

nav {
  flex-shrink: 0;
}

main {
  flex: 1 0 auto;
}

footer {
  flex-shrink: 0;
}
`
