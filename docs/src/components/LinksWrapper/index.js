// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export default styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  text-align: left;

  ${mediaQueries.mdUp`
    flex-direction: row;

    a {
      margin-right: ${(p) => p.theme.tokens.spacing06};
    }
  `}
`
