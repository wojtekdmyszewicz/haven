// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string } from 'prop-types'
import { Container, Row } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import LinksWrapper from 'src/components/LinksWrapper'
import LinkButton from 'src/components/LinkButton'
import { Section, StyledCol } from './index.styles'

const News = ({
  content,
  newsLink1Text,
  newsLink1Href,
  newsLink2Text,
  newsLink2Href,
}) => (
  <Section alternate omitArrow>
    <Container>
      <Row>
        <StyledCol>
          <HtmlContent content={content} />
          <LinksWrapper>
            <LinkButton href={newsLink1Href} text={newsLink1Text} />
            <LinkButton href={newsLink2Href} text={newsLink2Text} />
          </LinksWrapper>
        </StyledCol>
      </Row>
    </Container>
  </Section>
)

News.propTypes = {
  content: string,
  imageLink: string,
  imageAlt: string,
  newsLink1Text: string,
  newsLink1Href: string,
  newsLink2Text: string,
  newsLink2Href: string,
}

export default News
