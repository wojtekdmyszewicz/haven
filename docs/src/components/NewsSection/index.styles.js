// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import { Col } from 'src/components/Grid'
import BaseSection from 'src/components/Section'

export const StyledCol = styled(Col)`
  ${mediaQueries.mdUp`
    flex: 0.7;
  `}
`

export const Section = styled(BaseSection)`
  position: relative;
  background-image: url('/generic/news-bg-large.svg');

  ${mediaQueries.mdDown`
    padding-top: 0;
    background-position: center center;
  `}

  ${mediaQueries.mdUp`
    padding-bottom: ${(p) => p.theme.tokens.spacing11};
    background-position: right center;
  `}
`
