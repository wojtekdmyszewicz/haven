// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { number } from 'prop-types'
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const Container = styled.div`
  width: 100%;
  max-width: ${(p) => p.theme.tokens.containerWidth};
  padding: 0 ${(p) => p.theme.tokens.spacing05};
  margin: 0 auto;
`

export const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -${(p) => p.theme.tokens.spacing05};
`

export const Col = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex: 0 1 auto;
  padding: 0 ${(p) => p.theme.tokens.spacing05};
  width: ${(p) => p.width}%;

  ${(p) => mediaQueries.xs`
    &:not(:last-child) {
      margin-bottom: ${p.width[0] === 1 ? p.theme.tokens.spacing05 : 0};
    }
  `}
`

Col.propTypes = {
  width: number,
}

Col.defaultProps = {
  width: 100,
}

export const CenterCol = styled.div`
  max-width: 720px;
  margin-left: auto;
  margin-right: auto;
  text-align: center;

  ${mediaQueries.smDown`
    h2 {
      text-align: center;
    }
  `}
`
