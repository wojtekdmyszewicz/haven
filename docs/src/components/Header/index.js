// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { bool } from 'prop-types'
import PrimaryNavigation from '@commonground/design-system/dist/components/PrimaryNavigation'
import { useRouter } from 'next/router'
import { IconHome, IconBox, IconInfo, IconMail } from 'src/icons'
import NavLink from '../NavLink'
import {
  StyledIcon,
  LogoWrapper,
  StyledHavenLogo,
  NavigationWrapper,
  StyledContainer,
} from './index.styles'

const Header = ({ homepage }) => {
  const { asPath } = useRouter()
  const pathname = asPath.split('/').slice(0, 2).join('/')

  const HomeIcon = () => <StyledIcon as={IconHome} />
  const AboutIcon = () => <StyledIcon as={IconInfo} />
  const TechIcon = () => <StyledIcon as={IconBox} />
  const ContactIcon = () => <StyledIcon as={IconMail} />

  return (
    <>
      <LogoWrapper homepage={homepage}>
        <StyledContainer>
          <StyledHavenLogo />
          <span>Haven</span>
        </StyledContainer>
      </LogoWrapper>

      <NavigationWrapper>
        <PrimaryNavigation
          LinkComponent={NavLink}
          pathname={pathname}
          mobileMoreText="Meer"
          items={[
            {
              name: 'Home',
              to: '/',
              Icon: HomeIcon,
            },
            {
              name: 'Over Haven',
              to: '/over-haven',
              Icon: AboutIcon,
            },
            {
              name: 'Techniek',
              to: '/techniek',
              Icon: TechIcon,
            },
            {
              name: 'Contact',
              to: '/contact',
              Icon: ContactIcon,
            },
          ]}
        />
      </NavigationWrapper>
    </>
  )
}

Header.propTypes = {
  homepage: bool,
}

Header.defaultProps = {
  homepage: false,
}

export default Header
