// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import Icon from '@commonground/design-system/dist/components/Icon'
import HavenLogo from '../Logo'
import { Container } from '../Grid'

export const StyledIcon = styled(Icon)`
  fill: ${(p) => p.theme.tokens.colorPaletteGray600};
`

export const StyledContainer = styled(Container)`
  display: flex;
  align-items: center;
  min-height: 56px;

  > span {
    padding-left: 1em;
    color: #000;
    text-decoration: none;
    font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
    font-size: 1.25rem;
  }
`

export const LogoWrapper = styled.header`
  ${(p) => `background: ${p.theme.tokens.colorBackground};`}
`

export const StyledHavenLogo = styled(HavenLogo)`
  height: 40px;
  margin: ${(p) => p.theme.tokens.spacing07} 0;
`

export const NavigationWrapper = styled.div`
  position: relative;
  z-index: 10; /* higher than homepage hero items */
  background-color: ${(p) => p.theme.tokens.colorBrand1};
`
