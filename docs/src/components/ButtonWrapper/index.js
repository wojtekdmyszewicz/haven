// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export default styled.div`
  margin: ${(p) => p.theme.tokens.spacing06} 0;

  ${mediaQueries.xs`
    display: flex;
    flex-direction: column;
    align-items: center;
  `}

  & > * {
    margin: ${(p) => p.theme.tokens.spacing03};
  }
`
