// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import BaseIcon from '@commonground/design-system/dist/components/Icon'
import { IconExternalLink as ExternalLink } from 'src/icons'
import Button from '@commonground/design-system/dist/components/Button'

export const StyledButton = styled(Button)`
  padding: 0;
  min-height: 0;
`

export const ExternalIcon = styled(BaseIcon)`
  margin-left: ${(p) => p.theme.tokens.spacing03};
  margin-right: 0;
`

export const PrefixIcon = styled(BaseIcon)`
  fill: #0b71a1;
  vertical-align: middle;
`

export const IconExternalLink = styled(ExternalLink)`
  width: ${(p) => p.theme.tokens.iconSizeSmall};
  height: ${(p) => p.theme.tokens.iconSizeSmall};
  fill: ${(p) => p.theme.tokens.colorPaletteGray700};
`
