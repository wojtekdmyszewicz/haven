// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { string } from 'prop-types'
import styled from 'styled-components'

const Content = styled.div`
  h2 {
    margin-bottom: ${(p) => p.theme.tokens.spacing06};
  }

  a[target='_blank'] {
    position: relative;
  }

  a[target='_blank']:after {
    position: relative;
    top: 2px;
    margin-left: ${(p) => p.theme.tokens.spacing01};
    content: url('/generic/external-link.svg');
  }
`

const HtmlContent = ({ content, ...props }) => (
  <Content dangerouslySetInnerHTML={{ __html: content }} {...props} />
)

HtmlContent.propTypes = {
  content: string,
}

HtmlContent.defaultProps = {
  content: '',
}

export default HtmlContent
