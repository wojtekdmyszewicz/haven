// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import BaseSection from 'src/components/Section'
import { Col, Row } from 'src/components/Grid'

export const Section = styled(BaseSection)`
  &::before {
    border-top-color: #bfc8d9;

    ${mediaQueries.mdDown`
      border-top-color: #b7c3d8;
    `}
  }
`

export const StyledRow = styled(Row)`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`

export const ContentCol = styled(Col)`
  ${mediaQueries.mdUp`
    width: 66.66%;
  `}
`

export const ImageCol = styled(Col)`
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding-top: 3rem;
  width: 100%;

  ${mediaQueries.smUp`
    order: 1;
    width: 33.33%;
  `}
`

export const Image = styled.img`
  object-fit: contain;
  width: 100%;
  max-width: 112px;

  ${mediaQueries.smUp`
    max-width: 200px;
  `}
`
