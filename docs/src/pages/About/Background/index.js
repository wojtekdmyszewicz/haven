// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string } from 'prop-types'
import { Container } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import { Section, StyledRow, ImageCol, Image, ContentCol } from './index.styles'

const Background = ({ content, imageLink, imageAlt }) => (
  <Section>
    <Container>
      <StyledRow>
        <ImageCol>
          <Image src={imageLink} alt={imageAlt} />
        </ImageCol>
        <ContentCol>
          <HtmlContent content={content} />
        </ContentCol>
      </StyledRow>
    </Container>
  </Section>
)

Background.propTypes = {
  content: string,
  imageLink: string,
  imageAlt: string,
}

export default Background
