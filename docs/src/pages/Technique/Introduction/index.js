// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string } from 'prop-types'
import { Container } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import { SectionIntro } from 'src/components/Section'
import { Section } from './index.styles'

const Introduction = ({ content }) => (
  <Section omitArrow>
    <Container>
      <SectionIntro>
        <HtmlContent content={content} />
      </SectionIntro>
    </Container>
  </Section>
)

Introduction.propTypes = {
  content: string,
}

export default Introduction
