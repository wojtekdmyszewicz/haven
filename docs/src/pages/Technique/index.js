// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import Header from 'src/components/Header'
import { object } from 'prop-types'
import Head from 'next/head'
import Footer from 'src/components/Footer'
import News from 'src/components/NewsSection'
import Introduction from './Introduction'
import Documentation from './Documentation'

export default function Technique({ sections }) {
  return (
    <>
      <Head>
        <title>Haven - Techniek - {sections.technique.title}</title>
        <meta name="description" content={sections.meta.description} />
        <meta name="author" content={sections.meta.author} />
      </Head>

      <Header />

      <main>
        <Introduction {...sections.introduction} />
        <Documentation {...sections.technique} />
        <News {...sections.news} />
      </main>

      <Footer />
    </>
  )
}

Technique.propTypes = {
  sections: object.isRequired,
}
