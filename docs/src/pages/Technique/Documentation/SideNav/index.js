// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React, { Fragment } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import menu from '../../../../../public/technique/content/menu.json'
import { StyledSideNav, StyledLi } from './index.styles'

const SideNav = () => {
  const router = useRouter()
  return (
    <StyledSideNav>
      {menu.map((category, i) => (
        <Fragment key={i}>
          <b>{category.title}</b>
          <ul>
            {category.children.map((child, j) => (
              <StyledLi
                key={j}
                isActive={(router.query.id?.[0] || '') === child.link}
              >
                <Link href={`/techniek/${child.link}`} activeClassName="active">
                  <a>{child.title}</a>
                </Link>
              </StyledLi>
            ))}
          </ul>
        </Fragment>
      ))}

      <hr />
    </StyledSideNav>
  )
}

export default SideNav
