// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled, { css } from 'styled-components'
import { mediaQueries } from '@commonground/design-system'

export const StyledSideNav = styled.div`
  width: 100%;
  ${mediaQueries.smUp`
    width: 25%;
  `}

  padding-top: ${(p) => p.theme.tokens.spacing08};
  padding-bottom: ${(p) => p.theme.tokens.spacing08};

  ${mediaQueries.smUp`
    padding-left: ${(p) => p.theme.tokens.spacing04};
    padding-right: ${(p) => p.theme.tokens.spacing06};
  `}

  ul {
    padding: 0;
    list-style: none;
    margin-top: ${(p) => p.theme.tokens.spacing02};
    margin-bottom: ${(p) => p.theme.tokens.spacing06};
  }

  hr {
    margin: 0;
  }

  ${mediaQueries.smUp`
    hr {
      display: none;
    }
  `}
`

export const StyledLi = styled.li`
  margin-bottom: ${(p) => p.theme.tokens.spacing01};
  position: relative;

  > a {
    color: #000;
    text-decoration: none;
    width: 100%;
    display: block;
    cursor: pointer;
    position: relative;
  }

  a.active {
    color: #0b71a1;
  }

  a:hover {
    color: #0b71a1;
  }

  ${(p) =>
    p.isActive &&
    css`
      ::before {
        content: '';
        border-radius: 4px;
        left: -${(p) => p.theme.tokens.spacing03};
        top: -${(p) => p.theme.tokens.spacing01};
        bottom: -${(p) => p.theme.tokens.spacing01};
        right: -${(p) => p.theme.tokens.spacing03};
        position: absolute;
        background: #eeeeee;
      }

      a {
        color: #0b71a1;
      }
    `}
`
