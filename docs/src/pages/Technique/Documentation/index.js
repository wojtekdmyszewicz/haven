// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string, arrayOf, shape, any } from 'prop-types'
import { Container, Row, Col } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import SideNav from 'src/pages/Technique/Documentation/SideNav'
import { Section, ContentWrapper, StyledHtmlContent } from './index.styles'
import Solutions from './Solutions'
import Standard from './Standard'

const Documentation = ({ title, content, specialPage, ...props }) => {
  const replaceWithNumber = (content) => {
    return content
      .replace(/\{{compliancy}}/g, props.checks.compliancychecks.length)
      .replace(/\{{suggested}}/g, props.checks.suggestedchecks.length)
  }

  return (
    <Section>
      <Container>
        <Row>
          <Col>
            <SideNav />
            {!specialPage && (
              <ContentWrapper>
                <h1>{title}</h1>
                <HtmlContent content={content} />
              </ContentWrapper>
            )}
            {specialPage === 'aan-de-slag' && (
              <ContentWrapper>
                <h1>{title}</h1>
                <p>{props.description}</p>
                <h2>{props.filterTitle}</h2>
                <Solutions
                  filterOption1={props.filterOption1}
                  filterOption2={props.filterOption2}
                  filterOption3={props.filterOption3}
                  itemSelectPlaceholder={props.itemSelectPlaceholder}
                  items={props.items}
                />
                <StyledHtmlContent content={content} />
              </ContentWrapper>
            )}
            {specialPage === 'checks' && (
              <ContentWrapper>
                <h1>{title}</h1>
                <HtmlContent content={replaceWithNumber(content)} />
                <Standard checks={props.checks} />
              </ContentWrapper>
            )}
          </Col>
        </Row>
      </Container>
    </Section>
  )
}

Documentation.propTypes = {
  content: string,
  title: string,
  specialPage: string,
  description: string,
  filterTitle: string,
  filterOption1: string,
  filterOption2: string,
  filterOption3: string,
  itemSelectPlaceholder: string,
  items: arrayOf(
    shape({
      title: string,
      platform: string,
      img: string,
      refUrl: string,
      contactUrl: string,
    }),
  ),
  checks: any,
}

export default Documentation
