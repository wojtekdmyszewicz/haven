// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React, { useEffect, useState } from 'react'
import { string, arrayOf, shape, array } from 'prop-types'
import LinksWrapper from 'src/components/LinksWrapper'
import LinkButton from 'src/components/LinkButton'
import {
  Container,
  StyledSelectComponent,
  StyledButton,
  CardWrapper,
  ContentWrapper,
  ImageWrapper,
} from './index.styles'

const Solutions = ({
  filterOption1,
  filterOption2,
  filterOption3,
  itemSelectPlaceholder,
  items,
}) => {
  const allPlatforms = [...new Set(items.map((item) => item.platforms).flat())]

  const [options, setOptions] = useState([
    {
      value: null,
      label: itemSelectPlaceholder,
    },
    ...allPlatforms.map((platform) => ({
      value: platform,
      label: platform,
      isDisabled: false,
    })),
  ])

  const [visibleItems, setVisibleItems] = useState(items)
  const [activeFilters, setActiveFilters] = useState({})

  useEffect(() => {
    handleFilter()
  }, [])

  const handleFilter = (value, type) => {
    const newActiveFilters = value
      ? { ...activeFilters, [type]: value }
      : type
      ? (() => {
          const { [type]: omit, ...res } = activeFilters
          return res
        })()
      : activeFilters

    setActiveFilters(newActiveFilters)

    const options = [
      {
        value: null,
        label: itemSelectPlaceholder,
      },
      ...allPlatforms
        .map((platform) => ({
          value: platform,
          label: platform,
          isDisabled: !items.filter(
            (item) =>
              item.platforms.includes(platform) &&
              (newActiveFilters.environments
                ? item.environments.includes(newActiveFilters.environments)
                : true),
          ).length,
        }))
        .sort((a, b) => a.label.localeCompare(b.label)),
    ]

    setOptions(options)

    const newItems = items.filter((item) =>
      Object.entries(newActiveFilters).every(([filterType, filterValue]) =>
        item[`${filterType}`]?.includes(filterValue),
      ),
    )

    setVisibleItems(newItems)
  }

  const createSupportedPlatformsString = (environments) => {
    return `
      - ${environments.slice(0, 1)}
      ${environments.slice(1).map((item) => `+ ${item}`)}
    `
  }

  return (
    <>
      <Container>
        <StyledButton
          isActive={!activeFilters.environments}
          variant="secondary"
          onClick={() => handleFilter(null, 'environments')}
        >
          {filterOption1}
        </StyledButton>
        <StyledButton
          isActive={activeFilters.environments === filterOption2}
          variant="secondary"
          onClick={() => handleFilter(filterOption2, 'environments')}
          disabled={
            !items.some((item) =>
              activeFilters.platforms
                ? item.platforms.includes(activeFilters.platforms) &&
                  item.environments.includes(filterOption2)
                : true,
            )
          }
        >
          {filterOption2}
        </StyledButton>
        <StyledButton
          isActive={activeFilters.environments === filterOption3}
          variant="secondary"
          onClick={() => handleFilter(filterOption3, 'environments')}
          disabled={
            !items.some((item) =>
              activeFilters.platforms
                ? item.platforms.includes(activeFilters.platforms) &&
                  item.environments.includes(filterOption3)
                : true,
            )
          }
        >
          {filterOption3}
        </StyledButton>
        <StyledSelectComponent
          id="option"
          instanceId="option"
          name="option"
          placeholder={itemSelectPlaceholder}
          options={options}
          onChange={(option) => handleFilter(option.value, 'platforms')}
          value={
            activeFilters.platforms
              ? {
                  value: activeFilters.platforms,
                  label: activeFilters.platforms,
                }
              : null
          }
        />
      </Container>

      {visibleItems.map((item, i) => (
        <CardWrapper key={i}>
          <ContentWrapper>
            <h3>{item.title}</h3>
            <span>{createSupportedPlatformsString(item.environments)}</span>
            <p>{item.description}</p>
            <LinksWrapper>
              <LinkButton href={item.refUrl} text="Referentie Implementatie" />
              <LinkButton href={item.contactUrl} text="Contactgegevens" />
            </LinksWrapper>
          </ContentWrapper>
          <ImageWrapper>
            <img src={item.img} />
          </ImageWrapper>
        </CardWrapper>
      ))}
    </>
  )
}

Solutions.propTypes = {
  filterOption1: string,
  filterOption2: string,
  filterOption3: string,
  itemSelectPlaceholder: string,
  items: arrayOf(
    shape({
      title: string,
      platforms: array,
      environments: array,
      img: string,
      refUrl: string,
      contactUrl: string,
    }),
  ),
}

export default Solutions
