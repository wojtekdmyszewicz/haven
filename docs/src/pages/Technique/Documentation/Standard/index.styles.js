// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'

export const Category = styled.p`
  color: ${(p) => p.theme.tokens.colorPaletteGray600};
  margin: ${(p) => p.theme.tokens.spacing08} 0
    ${(p) => p.theme.tokens.spacing04};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
`

export const Label = styled.span`
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
`

export const Rationale = styled.p`
  margin-bottom: ${(p) => p.theme.tokens.spacing06};
`

export const Hr = styled.hr`
  margin: ${(p) => p.theme.tokens.spacing07} 0;
  border-top-color: ${(p) => p.theme.tokens.colorPaletteGray100};
  border-bottom-color: white;
`
