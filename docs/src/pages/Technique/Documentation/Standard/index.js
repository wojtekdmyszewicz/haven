// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { any } from 'prop-types'
import { Category, Label, Rationale, Hr } from './index.styles'

const translations = {
  compliancychecks: 'Verplichte checks',
  suggestedchecks: 'Voorgestelde checks',
}

const Standard = ({ checks }) => {
  if (!checks.compliancychecks.length) return <></>

  const checksPerCategory = (checks) => {
    const categories = []

    checks?.forEach((check) => {
      if (!categories[check.category]) {
        categories[check.category] = []
      }

      categories[check.category].push(check)
    })

    return categories
  }

  const checksByCategories = Object.keys(checks).map((key) => {
    return {
      title: `${translations[`${key}`]} (${checks[`${key}`]?.length})`,
      categories: { ...checksPerCategory(checks[`${key}`]) },
    }
  })

  return (
    <>
      {checksByCategories.map(({ title, categories }, i) => (
        <React.Fragment key={i}>
          <Hr />
          <h3>{title}</h3>
          {Object.keys(categories).map((categoryName, j) => (
            <React.Fragment key={j}>
              <Category>{categoryName.toUpperCase()}</Category>
              {categories[`${categoryName}`].map((category, k) => (
                <React.Fragment key={k}>
                  <Label>{category.label}</Label>
                  <Rationale>{category.rationale}</Rationale>
                </React.Fragment>
              ))}
            </React.Fragment>
          ))}
        </React.Fragment>
      ))}
    </>
  )
}

Standard.propTypes = {
  checks: any,
}

export default Standard
