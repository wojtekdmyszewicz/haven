// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import BaseSection from 'src/components/Section'
import HtmlContent from 'src/components/HtmlContent'

export const Section = styled(BaseSection)`
  padding: ${(p) => p.theme.tokens.spacing09} 0;

  ::before {
    border-top-color: #252525;
  }

  ${mediaQueries.mdUp`
    padding: ${(p) => p.theme.tokens.spacing10} 0;
  `}
`

export const ContentWrapper = styled.div`
  flex: 1;
  width: 100%;

  img {
    max-width: 100%;
  }

  ${mediaQueries.smUp`
    width: 75%;
  `}

  ${mediaQueries.smUp`
    padding-top: ${(p) => p.theme.tokens.spacing08};
  `}

  padding-bottom: ${(p) => p.theme.tokens.spacing08};
  overflow-wrap: break-word;

  code {
    font-size: 0.9em;
  }

  code[class*='language-'] {
    padding: 0.2em;
  }
`

export const StyledHtmlContent = styled(HtmlContent)`
  margin-top: ${(p) => p.theme.tokens.spacing10};
`
