// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import BaseSection from 'src/components/Section'

export const Section = styled(BaseSection)`
  background-image: linear-gradient(161deg, #e0e4ea 0%, #b1bfd5 94%);

  ${mediaQueries.mdUp`
    padding: ${(p) =>
      `${p.theme.tokens.spacing09} 0 ${p.theme.tokens.spacing12}`};
    `}
`
