// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import BaseSection from 'src/components/Section'
import { Col } from 'src/components/Grid'

export const Section = styled(BaseSection)`
  padding-bottom: ${(p) => p.theme.tokens.spacing09};

  ${mediaQueries.mdUp`
    padding-top: 0;
  `}
`

export const Block = styled.div`
  ${mediaQueries.mdUp`
    height: 100%;
    padding: ${(p) =>
      `${p.theme.tokens.spacing07} ${p.theme.tokens.spacing07} 0 ${p.theme.tokens.spacing07}`};
    background-color: ${(p) => p.theme.tokens.colorBackground};
    margin-top: -${(p) => p.theme.tokens.spacing09};

    h2 {
      margin-top: 0;
    }
  `}
`

export const StyledCol = styled(Col)`
  ${mediaQueries.mdUp`
    display: block;
    flex: 1;
  `}
`

export const List = styled.ul`
  padding: 0;
  list-style-type: none;
`

export const Item = styled.li`
  margin: 0 0 ${(p) => p.theme.tokens.spacing03} 0;
`
