// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { shape, string, func } from 'prop-types'
import { Container, Row } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import { getIcon } from 'src/icons'
import LinkButton from 'src/components/LinkButton'
import { Section, Block, List, Item, StyledCol } from './index.styles'

const IntroBlocks = ({ blocks }) => (
  <Section omitArrow>
    <Container>
      <Row>
        {Object.values(blocks).map(
          (
            {
              content,
              link1Icon,
              link1Text,
              link1Href,
              link2Icon,
              link2Text,
              link2Href,
            },
            i,
          ) => (
            <StyledCol key={i}>
              <Block>
                <HtmlContent content={content} />
                <List>
                  <Item>
                    <LinkButton
                      href={link1Href}
                      text={link1Text}
                      prefixIcon={getIcon(link1Icon)}
                    />
                  </Item>
                  {link2Href && link2Text && link2Icon && (
                    <Item>
                      <LinkButton
                        href={link2Href}
                        text={link2Text}
                        prefixIcon={getIcon(link2Icon)}
                      />
                    </Item>
                  )}
                </List>
              </Block>
            </StyledCol>
          ),
        )}
      </Row>
    </Container>
  </Section>
)

IntroBlocks.propTypes = {
  blocks: shape({
    content: string,
    link1Icon: func,
    link1Text: string,
    link1Href: string,
    link2Icon: func,
    link2Text: string,
    link2Href: string,
  }),
}

export default IntroBlocks
