// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import { Col } from 'src/components/Grid'
import BaseSection from 'src/components/Section'

export const Image = styled.img`
  max-width: 102px;
  margin-right: ${(p) => p.theme.tokens.spacing04};

  ${mediaQueries.mdUp`
    width: 100%;
    max-width: 102px;
    margin-right: ${(p) => p.theme.tokens.spacing07};
  `}
`

export const Section = styled(BaseSection)`
  padding-bottom: 6rem;

  ${mediaQueries.mdUp`
    padding-bottom: 292px;
  `}
`

export const StyledCol = styled(Col)`
  flex-wrap: nowrap;
  align-items: center;

  h2 {
    margin-top: 0;
  }

  p {
    margin: 0;
  }
`
