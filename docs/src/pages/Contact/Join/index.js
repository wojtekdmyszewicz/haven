// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string } from 'prop-types'
import { Container, Row } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import LinksWrapper from 'src/components/LinksWrapper'
import LinkButton from 'src/components/LinkButton'
import { StyledCol, Image, Section } from './index.styles'

const Join = ({
  content,
  imageLink,
  imageAlt,
  link1Text,
  link1Href,
  link2Text,
  link2Href,
}) => (
  <Section omitArrow>
    <Container>
      <Row>
        <StyledCol>
          <Image src={imageLink} alt={imageAlt} />
          <HtmlContent content={content} />

          <LinksWrapper>
            {link1Href && link1Text && (
              <LinkButton href={link1Href} text={link1Text} />
            )}
            {link2Href && link2Text && (
              <LinkButton href={link2Href} text={link2Text} />
            )}
          </LinksWrapper>
        </StyledCol>
      </Row>
    </Container>
  </Section>
)

Join.propTypes = {
  content: string,
  imageLink: string,
  imageAlt: string,
  link1Text: string,
  link1Href: string,
  link2Text: string,
  link2Href: string,
}

export default Join
