// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { object } from 'prop-types'
import Head from 'next/head'
import Header from 'src/components/Header'
import Footer from 'src/components/Footer'
import Introduction from './Introduction'
import IntroBlocks from './IntroBlocks'
import Join from './Join'

const Contact = ({ sections }) => (
  <>
    <Head>
      <title>Haven - Contact</title>
      <meta name="description" content={sections.meta.description} />
    </Head>

    <Header />

    <main>
      <Introduction {...sections.introduction} />
      <IntroBlocks blocks={sections.intro} />
      <Join {...sections.join} />
    </main>

    <Footer />
  </>
)

Contact.propTypes = {
  sections: object.isRequired,
}

export default Contact
