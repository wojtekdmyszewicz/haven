// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'

export const Title = styled.h1`
  width: 100%;
  margin-top: ${(p) => p.theme.tokens.spacing08};

  ${mediaQueries.mdUp`
    margin-top: ${(p) => p.theme.tokens.spacing08};
  `}
`

export const SubTitle = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
`
