// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import Header from 'src/components/Header'
import { object } from 'prop-types'
import Head from 'next/head'
import Footer from 'src/components/Footer'
import { Row, Col, Container } from 'src/components/Grid'
import { Title, SubTitle } from './index.styles'

export default function NotFound({ sections }) {
  return (
    <>
      <Head>
        <title>Haven - 404</title>
        <meta name="description" content={sections.meta.description} />
        <meta name="author" content={sections.meta.author} />
      </Head>

      <Header homepage />

      <main>
        <Container>
          <Row>
            <Col>
              <Title>{sections.introduction.siteTitle}</Title>
              <SubTitle>{sections.introduction.siteSubTitle}</SubTitle>
            </Col>
          </Row>
        </Container>
      </main>

      <Footer />
    </>
  )
}

NotFound.propTypes = {
  sections: object.isRequired,
}
