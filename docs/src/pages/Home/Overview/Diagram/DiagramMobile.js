// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string } from 'prop-types'

const DiagramMobile = ({ diagramA11yText }) => {
  return (
    <svg
      width="305px"
      height="453px"
      viewBox="0 0 305 453"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <title>{diagramA11yText}</title>
      <defs>
        <radialGradient
          cx="33.084698%"
          cy="30.7020471%"
          fx="33.084698%"
          fy="30.7020471%"
          r="54.4013302%"
          gradientTransform="translate(0.330847,0.307020),scale(1.000000,0.988127),rotate(48.716831),translate(-0.330847,-0.307020)"
          id="radialGradient-1"
        >
          <stop stopColor="#FFFFFF" offset="0%"></stop>
          <stop stopColor="#767676" offset="100%"></stop>
        </radialGradient>
        <radialGradient
          cx="11.8063337%"
          cy="0%"
          fx="11.8063337%"
          fy="0%"
          r="142.623577%"
          gradientTransform="translate(0.118063,0.000000),scale(1.000000,0.988127),rotate(45.200029),translate(-0.118063,-0.000000)"
          id="radialGradient-2"
        >
          <stop stopColor="#575757" offset="0%"></stop>
          <stop stopColor="#D2D2D2" offset="100%"></stop>
        </radialGradient>
        <ellipse
          id="path-3"
          cx="23.5119048"
          cy="98.6675127"
          rx="2.82142857"
          ry="2.85532995"
        ></ellipse>
        <ellipse
          id="path-4"
          cx="23.5119048"
          cy="98.6675127"
          rx="2.82142857"
          ry="2.85532995"
        ></ellipse>
        <ellipse
          id="path-5"
          cx="23.5119048"
          cy="98.6675127"
          rx="2.82142857"
          ry="2.85532995"
        ></ellipse>
        <linearGradient
          x1="52.6683569%"
          y1="55.2612469%"
          x2="39.0165698%"
          y2="77.9387465%"
          id="linearGradient-6"
        >
          <stop stopColor="#000000" offset="0%"></stop>
          <stop stopColor="#000000" stopOpacity="0" offset="100%"></stop>
        </linearGradient>
        <polygon
          id="path-7"
          points="30.3797325 0 64.2381501 0 63.974107 38.43329 103 60.8 58.4861331 114 1.46371804e-12 80.4333333 7.533002e-13 38.43329"
        ></polygon>
        <filter
          x="-25.1%"
          y="-19.9%"
          width="150.2%"
          height="139.8%"
          filterUnits="objectBoundingBox"
          id="filter-9"
        >
          <feOffset
            dx="0"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetOuter1"
          ></feOffset>
          <feGaussianBlur
            stdDeviation="4"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          ></feGaussianBlur>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 1 0"
            type="matrix"
            in="shadowBlurOuter1"
            result="shadowMatrixOuter1"
          ></feColorMatrix>
          <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="SourceGraphic"></feMergeNode>
          </feMerge>
        </filter>
        <polygon
          id="path-10"
          points="0 32.8582563 2.54808063 29.5916819 2.54808063 64.0728206 0 67.6915897"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.3%"
          width="139.2%"
          height="102.6%"
          filterUnits="objectBoundingBox"
          id="filter-11"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-12"
          points="8.9182822 21.2355153 11.4663628 17.8804781 11.4663628 51.5313741 8.9182822 55.2823813"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.3%"
          width="139.2%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-13"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-14"
          points="17.8365644 9.42492299 20.384645 6.02512556 20.384645 38.5334793 17.8365644 42.2835112"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.4%"
          width="139.2%"
          height="102.8%"
          filterUnits="objectBoundingBox"
          id="filter-15"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-16"
          points="4.4591411 27.1527781 7.00722173 23.7956056 7.00722173 57.651717 4.4591411 61.3582563"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.3%"
          width="139.2%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-17"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-18"
          points="13.3774233 15.3663638 15.9255039 12.0091913 15.9255039 44.9850503 13.3774233 48.6915897"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.4%"
          width="139.2%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-19"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-20"
          points="22.2957055 3.40716889 24.8437861 -2.59902003e-14 24.8437861 31.9656759 22.2957055 35.6722152"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.4%"
          width="139.2%"
          height="102.8%"
          filterUnits="objectBoundingBox"
          id="filter-21"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-22"
          points="30.3797325 0 64.2381501 0 63.974107 38.43329 103 60.8 58.4861331 114 1.46371804e-12 80.4333333 7.533002e-13 38.43329"
        ></polygon>
        <filter
          x="-25.1%"
          y="-19.9%"
          width="150.2%"
          height="139.8%"
          filterUnits="objectBoundingBox"
          id="filter-24"
        >
          <feOffset
            dx="0"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetOuter1"
          ></feOffset>
          <feGaussianBlur
            stdDeviation="4"
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
          ></feGaussianBlur>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 1 0"
            type="matrix"
            in="shadowBlurOuter1"
            result="shadowMatrixOuter1"
          ></feColorMatrix>
          <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="SourceGraphic"></feMergeNode>
          </feMerge>
        </filter>
        <polygon
          id="path-25"
          points="0 32.8582563 2.54808063 29.5916819 2.54808063 64.0728206 0 67.6915897"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.3%"
          width="139.2%"
          height="102.6%"
          filterUnits="objectBoundingBox"
          id="filter-26"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-27"
          points="8.9182822 21.2355153 11.4663628 17.8804781 11.4663628 51.5313741 8.9182822 55.2823813"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.3%"
          width="139.2%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-28"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-29"
          points="17.8365644 9.42492299 20.384645 6.02512556 20.384645 38.5334793 17.8365644 42.2835112"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.4%"
          width="139.2%"
          height="102.8%"
          filterUnits="objectBoundingBox"
          id="filter-30"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-31"
          points="4.4591411 27.1527781 7.00722173 23.7956056 7.00722173 57.651717 4.4591411 61.3582563"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.3%"
          width="139.2%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-32"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-33"
          points="13.3774233 15.3663638 15.9255039 12.0091913 15.9255039 44.9850503 13.3774233 48.6915897"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.4%"
          width="139.2%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-34"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-35"
          points="22.2957055 3.40716889 24.8437861 -2.59902003e-14 24.8437861 31.9656759 22.2957055 35.6722152"
        ></polygon>
        <filter
          x="-19.6%"
          y="-1.4%"
          width="139.2%"
          height="102.8%"
          filterUnits="objectBoundingBox"
          id="filter-36"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-37"
          points="30.3797325 0 64.2381501 0 63.974107 38.43329 103 60.8 58.4861331 114 1.46371804e-12 80.4333333 7.533002e-13 38.43329"
        ></polygon>
        <polygon
          id="path-39"
          points="0 32.681233 2.52 29.4322572 2.52 63.7276293 0 67.3269023"
        ></polygon>
        <filter
          x="-19.8%"
          y="-1.3%"
          width="139.7%"
          height="102.6%"
          filterUnits="objectBoundingBox"
          id="filter-40"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-41"
          points="8.82 21.1211093 11.34 17.7841473 11.34 51.2537496 8.82 54.9845483"
        ></polygon>
        <filter
          x="-19.8%"
          y="-1.3%"
          width="139.7%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-42"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-43"
          points="17.64 9.37414636 20.16 5.99266529 20.16 38.3258807 17.64 42.0557095"
        ></polygon>
        <filter
          x="-19.8%"
          y="-1.4%"
          width="139.7%"
          height="102.8%"
          filterUnits="objectBoundingBox"
          id="filter-44"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-45"
          points="4.41 27.006493 6.93 23.6674072 6.93 57.3411193 4.41 61.0276897"
        ></polygon>
        <filter
          x="-19.8%"
          y="-1.3%"
          width="139.7%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-46"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-47"
          points="13.23 15.2835778 15.75 11.944492 15.75 44.7426941 13.23 48.4292645"
        ></polygon>
        <filter
          x="-19.8%"
          y="-1.4%"
          width="139.7%"
          height="102.7%"
          filterUnits="objectBoundingBox"
          id="filter-48"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <polygon
          id="path-49"
          points="22.05 3.38881282 24.57 -2.58501785e-14 24.57 31.7934613 22.05 35.4800317"
        ></polygon>
        <filter
          x="-19.8%"
          y="-1.4%"
          width="139.7%"
          height="102.8%"
          filterUnits="objectBoundingBox"
          id="filter-50"
        >
          <feOffset
            dx="-1"
            dy="0"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.25 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
        <rect
          id="path-51"
          x="0"
          y="0"
          width="10.755102"
          height="20.2166701"
        ></rect>
        <polygon
          id="path-53"
          points="8.85714286 37.2744854 10.755102 36.6427145 10.755102 56.8593846 8.85714286 56.8593846"
        ></polygon>
        <polygon
          id="path-55"
          points="173.344628 168 207.063729 168 233.344628 207 199.344628 207"
        ></polygon>
        <filter
          x="-0.8%"
          y="-1.3%"
          width="101.7%"
          height="102.6%"
          filterUnits="objectBoundingBox"
          id="filter-56"
        >
          <feOffset
            dx="1"
            dy="1"
            in="SourceAlpha"
            result="shadowOffsetInner1"
          ></feOffset>
          <feComposite
            in="shadowOffsetInner1"
            in2="SourceAlpha"
            operator="arithmetic"
            k2="-1"
            k3="1"
            result="shadowInnerInner1"
          ></feComposite>
          <feColorMatrix
            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0"
            type="matrix"
            in="shadowInnerInner1"
          ></feColorMatrix>
        </filter>
      </defs>
      <g
        id="Page-1"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g id="Group-45-Copy" transform="translate(-8.000000, -42.000000)">
          <g id="Group-10-Copy" transform="translate(8.655372, 42.182937)">
            <g id="Group-9" transform="translate(0.000000, 54.817063)">
              <path
                d="M76.3446282,304.600143 C45.3673649,288.462971 20.2610413,285.702808 1.0256573,296.319652 C-8.5530274,301.606545 52.7095827,308.828483 29.5134211,315.428477 C14.0493134,319.828472 4.99304905,322.239713 2.34462821,322.662199"
                id="Line-4-Copy-5"
                stroke="#000000"
                strokeWidth="0.503778338"
                strokeLinecap="square"
              ></path>
              <path
                d="M278.344628,258 C242.988137,261.649484 207.959917,266.322455 173.259968,272.018914 C138.560019,277.715374 81.5882392,282.709069 2.34462821,287"
                id="Line-4-Copy-6"
                stroke="#000000"
                strokeWidth="0.503778338"
                strokeLinecap="square"
              ></path>
              <g id="Server" transform="translate(53.344628, 208.000000)">
                <rect
                  id="Rectangle"
                  fill="#A9A6A6"
                  x="0"
                  y="38.071066"
                  width="47.0238095"
                  height="86.928934"
                ></rect>
                <rect
                  id="Rectangle-Copy"
                  fill="#A9A6A6"
                  x="7.52380952"
                  y="45.6852792"
                  width="31.9761905"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-2"
                  fill="#8D8686"
                  x="7.52380952"
                  y="45.6852792"
                  width="1.25396825"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-3"
                  fill="#C9C9C9"
                  x="38.8730159"
                  y="45.6852792"
                  width="1"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-26"
                  fill="#8D8686"
                  x="7.52380952"
                  y="45.6852792"
                  width="31.9761905"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-29"
                  fill="#8D8686"
                  x="7.52380952"
                  y="52.0304569"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-30"
                  fill="#8D8686"
                  x="7.52380952"
                  y="59.6446701"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-31"
                  fill="#8D8686"
                  x="7.52380952"
                  y="67.893401"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-33"
                  fill="#8D8686"
                  x="14.4206349"
                  y="105.964467"
                  width="18.1825397"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-34"
                  fill="#8D8686"
                  x="16.3015873"
                  y="109.137056"
                  width="14.4206349"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-35"
                  fill="#8D8686"
                  x="16.9285714"
                  y="112.309645"
                  width="13.1666667"
                  height="1"
                ></rect>
                <polygon
                  id="Rectangle-Copy-28"
                  fill="#C9C9C9"
                  points="8.77777778 73.6040609 39.5 73.6040609 39.5 74.8730964 7.52380952 74.8730964"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-25"
                  fill="#686868"
                  transform="translate(63.011905, 62.500000) scale(-1, 1) translate(-63.011905, -62.500000) "
                  points="47.0238095 1.0280405e-12 79 38.071066 79 125 47.0238095 76.7766497"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-5"
                  fill="#C9C9C9"
                  transform="translate(39.500000, 19.035533) scale(1, -1) translate(-39.500000, -19.035533) "
                  points="0 0 47.0238095 0 79 38.071066 31.9761905 38.071066"
                ></polygon>
                <path
                  d="M18.3973914,81.2182741 L32.6031746,81.2182741 L32.6031746,84.3908629 L25.7063492,84.3908629 C24.6114293,85.775958 22.60098,86.0111915 21.2158849,84.9162717 C21.0209218,84.762153 20.8445948,84.5858261 20.6904762,84.3908629 L20.6904762,84.3908629 L20.6904762,84.3908629 L15.047619,84.3908629 L18.3973914,81.2182741 Z"
                  id="Rectangle-Copy-32"
                  fill="#C9C9C9"
                ></path>
                <path
                  d="M15.047619,81.2182741 L32.6031746,81.2182741 L32.6031746,83.1218274 L26.3333333,83.1218274 C25.2384135,84.5069225 23.2279641,84.742156 21.842869,83.6472361 C21.6479059,83.4931175 21.471579,83.3167905 21.3174603,83.1218274 L21.3174603,83.1218274 L21.3174603,83.1218274 L16.3226797,83.1218274 L15.047619,84.3908629 L15.047619,81.2182741 Z"
                  id="Rectangle"
                  fill="#545454"
                ></path>
                <g id="Oval">
                  <use
                    fill="url(#radialGradient-1)"
                    fillRule="evenodd"
                    xlinkHref="#path-3"
                  ></use>
                  <ellipse
                    stroke="url(#radialGradient-2)"
                    strokeWidth="1.00755668"
                    cx="23.5119048"
                    cy="98.6675127"
                    rx="3.32520691"
                    ry="3.35910829"
                  ></ellipse>
                  <ellipse
                    stroke="#686767"
                    strokeWidth="1.00755668"
                    strokeLinejoin="square"
                    cx="23.5119048"
                    cy="98.6675127"
                    rx="2.31765023"
                    ry="2.35155161"
                  ></ellipse>
                </g>
              </g>
              <g id="Server" transform="translate(111.344628, 208.000000)">
                <rect
                  id="Rectangle"
                  fill="#A9A6A6"
                  x="0"
                  y="38.071066"
                  width="47.0238095"
                  height="86.928934"
                ></rect>
                <rect
                  id="Rectangle-Copy"
                  fill="#A9A6A6"
                  x="7.52380952"
                  y="45.6852792"
                  width="31.9761905"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-2"
                  fill="#8D8686"
                  x="7.52380952"
                  y="45.6852792"
                  width="1.25396825"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-3"
                  fill="#C9C9C9"
                  x="38.8730159"
                  y="45.6852792"
                  width="1"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-26"
                  fill="#8D8686"
                  x="7.52380952"
                  y="45.6852792"
                  width="31.9761905"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-29"
                  fill="#8D8686"
                  x="7.52380952"
                  y="52.0304569"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-30"
                  fill="#8D8686"
                  x="7.52380952"
                  y="59.6446701"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-31"
                  fill="#8D8686"
                  x="7.52380952"
                  y="67.893401"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-33"
                  fill="#8D8686"
                  x="14.4206349"
                  y="105.964467"
                  width="18.1825397"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-34"
                  fill="#8D8686"
                  x="16.3015873"
                  y="109.137056"
                  width="14.4206349"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-35"
                  fill="#8D8686"
                  x="16.9285714"
                  y="112.309645"
                  width="13.1666667"
                  height="1"
                ></rect>
                <polygon
                  id="Rectangle-Copy-28"
                  fill="#C9C9C9"
                  points="8.77777778 73.6040609 39.5 73.6040609 39.5 74.8730964 7.52380952 74.8730964"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-25"
                  fill="#686868"
                  transform="translate(63.011905, 62.500000) scale(-1, 1) translate(-63.011905, -62.500000) "
                  points="47.0238095 1.0280405e-12 79 38.071066 79 125 47.0238095 76.7766497"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-5"
                  fill="#C9C9C9"
                  transform="translate(39.500000, 19.035533) scale(1, -1) translate(-39.500000, -19.035533) "
                  points="0 0 47.0238095 0 79 38.071066 31.9761905 38.071066"
                ></polygon>
                <path
                  d="M18.3973914,81.2182741 L32.6031746,81.2182741 L32.6031746,84.3908629 L25.7063492,84.3908629 C24.6114293,85.775958 22.60098,86.0111915 21.2158849,84.9162717 C21.0209218,84.762153 20.8445948,84.5858261 20.6904762,84.3908629 L20.6904762,84.3908629 L20.6904762,84.3908629 L15.047619,84.3908629 L18.3973914,81.2182741 Z"
                  id="Rectangle-Copy-32"
                  fill="#C9C9C9"
                ></path>
                <path
                  d="M15.047619,81.2182741 L32.6031746,81.2182741 L32.6031746,83.1218274 L26.3333333,83.1218274 C25.2384135,84.5069225 23.2279641,84.742156 21.842869,83.6472361 C21.6479059,83.4931175 21.471579,83.3167905 21.3174603,83.1218274 L21.3174603,83.1218274 L21.3174603,83.1218274 L16.3226797,83.1218274 L15.047619,84.3908629 L15.047619,81.2182741 Z"
                  id="Rectangle"
                  fill="#545454"
                ></path>
                <g id="Oval">
                  <use
                    fill="url(#radialGradient-1)"
                    fillRule="evenodd"
                    xlinkHref="#path-4"
                  ></use>
                  <ellipse
                    stroke="url(#radialGradient-2)"
                    strokeWidth="1.00755668"
                    cx="23.5119048"
                    cy="98.6675127"
                    rx="3.32520691"
                    ry="3.35910829"
                  ></ellipse>
                  <ellipse
                    stroke="#686767"
                    strokeWidth="1.00755668"
                    strokeLinejoin="square"
                    cx="23.5119048"
                    cy="98.6675127"
                    rx="2.31765023"
                    ry="2.35155161"
                  ></ellipse>
                </g>
              </g>
              <g id="Server" transform="translate(170.344628, 208.000000)">
                <rect
                  id="Rectangle"
                  fill="#A9A6A6"
                  x="0"
                  y="38.071066"
                  width="47.0238095"
                  height="86.928934"
                ></rect>
                <rect
                  id="Rectangle-Copy"
                  fill="#A9A6A6"
                  x="7.52380952"
                  y="45.6852792"
                  width="31.9761905"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-2"
                  fill="#8D8686"
                  x="7.52380952"
                  y="45.6852792"
                  width="1.25396825"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-3"
                  fill="#C9C9C9"
                  x="38.8730159"
                  y="45.6852792"
                  width="1"
                  height="29.1878173"
                ></rect>
                <rect
                  id="Rectangle-Copy-26"
                  fill="#8D8686"
                  x="7.52380952"
                  y="45.6852792"
                  width="31.9761905"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-29"
                  fill="#8D8686"
                  x="7.52380952"
                  y="52.0304569"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-30"
                  fill="#8D8686"
                  x="7.52380952"
                  y="59.6446701"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-31"
                  fill="#8D8686"
                  x="7.52380952"
                  y="67.893401"
                  width="24.452381"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-33"
                  fill="#8D8686"
                  x="14.4206349"
                  y="105.964467"
                  width="18.1825397"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-34"
                  fill="#8D8686"
                  x="16.3015873"
                  y="109.137056"
                  width="14.4206349"
                  height="1"
                ></rect>
                <rect
                  id="Rectangle-Copy-35"
                  fill="#8D8686"
                  x="16.9285714"
                  y="112.309645"
                  width="13.1666667"
                  height="1"
                ></rect>
                <polygon
                  id="Rectangle-Copy-28"
                  fill="#C9C9C9"
                  points="8.77777778 73.6040609 39.5 73.6040609 39.5 74.8730964 7.52380952 74.8730964"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-25"
                  fill="#686868"
                  transform="translate(63.011905, 62.500000) scale(-1, 1) translate(-63.011905, -62.500000) "
                  points="47.0238095 1.0280405e-12 79 38.071066 79 125 47.0238095 76.7766497"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-5"
                  fill="#C9C9C9"
                  transform="translate(39.500000, 19.035533) scale(1, -1) translate(-39.500000, -19.035533) "
                  points="0 0 47.0238095 0 79 38.071066 31.9761905 38.071066"
                ></polygon>
                <path
                  d="M18.3973914,81.2182741 L32.6031746,81.2182741 L32.6031746,84.3908629 L25.7063492,84.3908629 C24.6114293,85.775958 22.60098,86.0111915 21.2158849,84.9162717 C21.0209218,84.762153 20.8445948,84.5858261 20.6904762,84.3908629 L20.6904762,84.3908629 L20.6904762,84.3908629 L15.047619,84.3908629 L18.3973914,81.2182741 Z"
                  id="Rectangle-Copy-32"
                  fill="#C9C9C9"
                ></path>
                <path
                  d="M15.047619,81.2182741 L32.6031746,81.2182741 L32.6031746,83.1218274 L26.3333333,83.1218274 C25.2384135,84.5069225 23.2279641,84.742156 21.842869,83.6472361 C21.6479059,83.4931175 21.471579,83.3167905 21.3174603,83.1218274 L21.3174603,83.1218274 L21.3174603,83.1218274 L16.3226797,83.1218274 L15.047619,84.3908629 L15.047619,81.2182741 Z"
                  id="Rectangle"
                  fill="#545454"
                ></path>
                <g id="Oval">
                  <use
                    fill="url(#radialGradient-1)"
                    fillRule="evenodd"
                    xlinkHref="#path-5"
                  ></use>
                  <ellipse
                    stroke="url(#radialGradient-2)"
                    strokeWidth="1.00755668"
                    cx="23.5119048"
                    cy="98.6675127"
                    rx="3.32520691"
                    ry="3.35910829"
                  ></ellipse>
                  <ellipse
                    stroke="#686767"
                    strokeWidth="1.00755668"
                    strokeLinejoin="square"
                    cx="23.5119048"
                    cy="98.6675127"
                    rx="2.31765023"
                    ry="2.35155161"
                  ></ellipse>
                </g>
              </g>
              <g id="Group-3" transform="translate(41.344628, 157.000000)">
                <polygon
                  id="Rectangle-Copy-36"
                  fill="#000000"
                  opacity="0.200000003"
                  points="11.4146341 69.1362561 176.039157 69.1362561 176.039157 99.5815799 128.114132 99.5815799 128.097561 106 117.336065 99.5815799 69.770247 99.5815799 69.7560976 106 58.9756098 99.5815799 11.4146341 99.5815799"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-37"
                  fill="url(#linearGradient-6)"
                  opacity="0.200000003"
                  transform="translate(191.935398, 78.818313) scale(-1, 1) translate(-191.935398, -78.818313) "
                  points="207.870796 69.3844783 207.870796 99.3819471 185.325867 112.818313 176 99.3819471 176 44.8183134"
                ></polygon>
                <rect
                  id="Rectangle-Copy-6"
                  fill="#FFBC2C"
                  x="0"
                  y="61.5249252"
                  width="187.414634"
                  height="30.4453238"
                ></rect>
                <polygon
                  id="Rectangle-Copy-21"
                  fill="#C38E1D"
                  transform="translate(207.707317, 45.985124) scale(-1, 1) translate(-207.707317, -45.985124) "
                  points="187.414634 3.26743961e-12 228 61.5249252 228 91.9702489 187.414634 26.6396583"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-7"
                  fill="#F3D593"
                  transform="translate(114.000000, 30.762463) scale(1, -1) translate(-114.000000, -30.762463) "
                  points="4.6686249e-13 -1.70765971e-15 187.414634 0 228 61.5249252 41.8536585 61.5249252"
                ></polygon>
              </g>
              <g
                id="Group-41-Copy"
                transform="translate(78.344628, 127.000000)"
              >
                <mask id="mask-8" fill="white">
                  <use xlinkHref="#path-7"></use>
                </mask>
                <use
                  id="Rectangle"
                  fill="#D8D8D8"
                  opacity="0"
                  xlinkHref="#path-7"
                ></use>
                <g id="Group" mask="url(#mask-8)">
                  <g
                    transform="translate(0.272091, 0.000000)"
                    id="Container-Copy-3"
                    filter="url(#filter-9)"
                  >
                    <g id="Group-6" transform="translate(34.399088, 0.000000)">
                      <polygon
                        id="Rectangle-Copy-309"
                        fill="#005282"
                        transform="translate(14.651464, 40.216667) scale(-1, 1) translate(-14.651464, -40.216667) "
                        points="0 8.94105623e-13 29.3029272 38 29.3029272 80.4333333 0 38.6333333"
                      ></polygon>
                      <g id="Group-8" transform="translate(2.548081, 5.612684)">
                        <g id="Rectangle-Copy-311">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-10"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-11)"
                            xlinkHref="#path-10"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-313">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-12"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-13)"
                            xlinkHref="#path-12"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-315">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-14"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-15)"
                            xlinkHref="#path-14"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-312">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-16"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-17)"
                            xlinkHref="#path-16"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-314">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-18"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-19)"
                            xlinkHref="#path-18"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-316">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-20"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-21)"
                            xlinkHref="#path-20"
                          ></use>
                        </g>
                      </g>
                    </g>
                    <rect
                      id="Rectangle-Copy-5"
                      fill="#0B82B5"
                      x="0"
                      y="38"
                      width="34.3990885"
                      height="42.4333333"
                    ></rect>
                    <rect
                      id="Rectangle-Copy"
                      fill="#000000"
                      opacity="0.300000012"
                      x="1.27404031"
                      y="39.9"
                      width="31.2139877"
                      height="1"
                    ></rect>
                    <polygon
                      id="Rectangle-Copy-3"
                      fill="#000000"
                      opacity="0.300000012"
                      transform="translate(16.881034, 59.216667) rotate(-270.000000) translate(-16.881034, -59.216667) "
                      points="-1.80229918 58.8981566 35.5643675 58.8981566 35.5643675 59.5351767 -1.80229918 59.5351767"
                    ></polygon>
                    <polygon
                      id="Rectangle-Copy-4"
                      fill="#000000"
                      opacity="0.300000012"
                      transform="translate(1.592550, 59.216667) rotate(-270.000000) translate(-1.592550, -59.216667) "
                      points="-17.0907829 58.8981566 20.2758837 58.8981566 20.2758837 59.5351767 -17.0907829 59.5351767"
                    ></polygon>
                    <polygon
                      id="Rectangle-Copy-6"
                      fill="#000000"
                      opacity="0.300000012"
                      transform="translate(32.169518, 59.216667) rotate(-270.000000) translate(-32.169518, -59.216667) "
                      points="13.4861846 58.8981566 50.8528513 58.8981566 50.8528513 59.5351767 13.4861846 59.5351767"
                    ></polygon>
                    <polygon
                      id="Rectangle-Copy-21"
                      fill="#56BCDF"
                      transform="translate(31.851008, 19.000000) scale(1, -1) translate(-31.851008, -19.000000) "
                      points="0 0 34.3990885 0 63.7020157 38 29.9399474 38"
                    ></polygon>
                    <rect
                      id="Rectangle-Copy-2"
                      fill="#000000"
                      opacity="0.600000024"
                      x="1.27404031"
                      y="77.9"
                      width="31.2139877"
                      height="1"
                    ></rect>
                    <text
                      id="Appli--catie"
                      fontFamily="SourceSansPro-Regular, Source Sans Pro"
                      fontSize="8.0604534"
                      fontWeight="normal"
                      line-spacing="7.55667506"
                      fill="#FFFFFF"
                    >
                      <tspan x="7.83656439" y="59.4573333">
                        Appli-
                      </tspan>
                      <tspan x="9.65016641" y="67.0140084">
                        catie
                      </tspan>
                    </text>
                  </g>
                </g>
              </g>
              <g
                id="Group-41-Copy-2"
                transform="translate(126.344628, 127.000000)"
              >
                <mask id="mask-23" fill="white">
                  <use xlinkHref="#path-22"></use>
                </mask>
                <use
                  id="Rectangle"
                  fill="#D8D8D8"
                  opacity="0"
                  xlinkHref="#path-22"
                ></use>
                <g id="Group" mask="url(#mask-23)">
                  <g
                    transform="translate(0.272091, 0.000000)"
                    id="Container-Copy-3"
                    filter="url(#filter-24)"
                  >
                    <g id="Group-6" transform="translate(34.399088, 0.000000)">
                      <polygon
                        id="Rectangle-Copy-309"
                        fill="#B81A1A"
                        transform="translate(14.651464, 40.216667) scale(-1, 1) translate(-14.651464, -40.216667) "
                        points="0 8.94105623e-13 29.3029272 38 29.3029272 80.4333333 0 38.6333333"
                      ></polygon>
                      <g id="Group-8" transform="translate(2.548081, 5.612684)">
                        <g id="Rectangle-Copy-311">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-25"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-26)"
                            xlinkHref="#path-25"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-313">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-27"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-28)"
                            xlinkHref="#path-27"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-315">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-29"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-30)"
                            xlinkHref="#path-29"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-312">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-31"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-32)"
                            xlinkHref="#path-31"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-314">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-33"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-34)"
                            xlinkHref="#path-33"
                          ></use>
                        </g>
                        <g id="Rectangle-Copy-316">
                          <use
                            fillOpacity="0.2"
                            fill="#000000"
                            fillRule="evenodd"
                            xlinkHref="#path-35"
                          ></use>
                          <use
                            fill="black"
                            fillOpacity="1"
                            filter="url(#filter-36)"
                            xlinkHref="#path-35"
                          ></use>
                        </g>
                      </g>
                    </g>
                    <rect
                      id="Rectangle-Copy-5"
                      fill="#E64444"
                      x="0"
                      y="38"
                      width="34.3990885"
                      height="42.4333333"
                    ></rect>
                    <rect
                      id="Rectangle-Copy"
                      fill="#000000"
                      opacity="0.300000012"
                      x="1.27404031"
                      y="39.9"
                      width="31.2139877"
                      height="1"
                    ></rect>
                    <polygon
                      id="Rectangle-Copy-3"
                      fill="#000000"
                      opacity="0.300000012"
                      transform="translate(16.881034, 59.216667) rotate(-270.000000) translate(-16.881034, -59.216667) "
                      points="-1.80229918 58.8981566 35.5643675 58.8981566 35.5643675 59.5351767 -1.80229918 59.5351767"
                    ></polygon>
                    <polygon
                      id="Rectangle-Copy-4"
                      fill="#000000"
                      opacity="0.300000012"
                      transform="translate(1.592550, 59.216667) rotate(-270.000000) translate(-1.592550, -59.216667) "
                      points="-17.0907829 58.8981566 20.2758837 58.8981566 20.2758837 59.5351767 -17.0907829 59.5351767"
                    ></polygon>
                    <polygon
                      id="Rectangle-Copy-6"
                      fill="#000000"
                      opacity="0.300000012"
                      transform="translate(32.169518, 59.216667) rotate(-270.000000) translate(-32.169518, -59.216667) "
                      points="13.4861846 58.8981566 50.8528513 58.8981566 50.8528513 59.5351767 13.4861846 59.5351767"
                    ></polygon>
                    <polygon
                      id="Rectangle-Copy-21"
                      fill="#F47474"
                      transform="translate(31.851008, 19.000000) scale(1, -1) translate(-31.851008, -19.000000) "
                      points="0 0 34.3990885 0 63.7020157 38 29.9399474 38"
                    ></polygon>
                    <rect
                      id="Rectangle-Copy-2"
                      fill="#000000"
                      opacity="0.600000024"
                      x="1.27404031"
                      y="77.9"
                      width="31.2139877"
                      height="1"
                    ></rect>
                    <text
                      id="Appli--catie"
                      fontFamily="SourceSansPro-Regular, Source Sans Pro"
                      fontSize="8.0604534"
                      fontWeight="normal"
                      line-spacing="7.55667506"
                      fill="#FFFFFF"
                    >
                      <tspan x="7.83656439" y="59.4573333">
                        Appli-
                      </tspan>
                      <tspan x="9.65016641" y="67.0140084">
                        catie
                      </tspan>
                    </text>
                  </g>
                </g>
              </g>
              <g id="Group-42" transform="translate(126.344628, 127.000000)">
                <mask id="mask-38" fill="white">
                  <use xlinkHref="#path-37"></use>
                </mask>
                <use
                  id="Rectangle"
                  fill="#D8D8D8"
                  opacity="0"
                  xlinkHref="#path-37"
                ></use>
              </g>
              <g
                id="Container-Copy-4"
                transform="translate(173.344628, 76.000000)"
              >
                <g id="Group-6" transform="translate(34.020000, 0.000000)">
                  <polygon
                    id="Rectangle-Copy-309"
                    fill="#C38E1D"
                    transform="translate(14.490000, 40.000000) scale(-1, 1) translate(-14.490000, -40.000000) "
                    points="0 8.89288643e-13 28.98 37.7952756 28.98 80 0 38.4251969"
                  ></polygon>
                  <g id="Group-8" transform="translate(2.520000, 5.582446)">
                    <g id="Rectangle-Copy-311">
                      <use
                        fillOpacity="0.2"
                        fill="#000000"
                        fillRule="evenodd"
                        xlinkHref="#path-39"
                      ></use>
                      <use
                        fill="black"
                        fillOpacity="1"
                        filter="url(#filter-40)"
                        xlinkHref="#path-39"
                      ></use>
                    </g>
                    <g id="Rectangle-Copy-313">
                      <use
                        fillOpacity="0.2"
                        fill="#000000"
                        fillRule="evenodd"
                        xlinkHref="#path-41"
                      ></use>
                      <use
                        fill="black"
                        fillOpacity="1"
                        filter="url(#filter-42)"
                        xlinkHref="#path-41"
                      ></use>
                    </g>
                    <g id="Rectangle-Copy-315">
                      <use
                        fillOpacity="0.2"
                        fill="#000000"
                        fillRule="evenodd"
                        xlinkHref="#path-43"
                      ></use>
                      <use
                        fill="black"
                        fillOpacity="1"
                        filter="url(#filter-44)"
                        xlinkHref="#path-43"
                      ></use>
                    </g>
                    <g id="Rectangle-Copy-312">
                      <use
                        fillOpacity="0.2"
                        fill="#000000"
                        fillRule="evenodd"
                        xlinkHref="#path-45"
                      ></use>
                      <use
                        fill="black"
                        fillOpacity="1"
                        filter="url(#filter-46)"
                        xlinkHref="#path-45"
                      ></use>
                    </g>
                    <g id="Rectangle-Copy-314">
                      <use
                        fillOpacity="0.2"
                        fill="#000000"
                        fillRule="evenodd"
                        xlinkHref="#path-47"
                      ></use>
                      <use
                        fill="black"
                        fillOpacity="1"
                        filter="url(#filter-48)"
                        xlinkHref="#path-47"
                      ></use>
                    </g>
                    <g id="Rectangle-Copy-316">
                      <use
                        fillOpacity="0.2"
                        fill="#000000"
                        fillRule="evenodd"
                        xlinkHref="#path-49"
                      ></use>
                      <use
                        fill="black"
                        fillOpacity="1"
                        filter="url(#filter-50)"
                        xlinkHref="#path-49"
                      ></use>
                    </g>
                  </g>
                </g>
                <rect
                  id="Rectangle-Copy-5"
                  fill="#FFBC2C"
                  x="0"
                  y="37.7952756"
                  width="34.02"
                  height="42.2047244"
                ></rect>
                <rect
                  id="Rectangle-Copy"
                  fill="#000000"
                  opacity="0.300000012"
                  x="1.26"
                  y="39.6850394"
                  width="30.87"
                  height="1"
                ></rect>
                <polygon
                  id="Rectangle-Copy-3"
                  fill="#000000"
                  opacity="0.300000012"
                  transform="translate(16.695000, 58.897638) rotate(-270.000000) translate(-16.695000, -58.897638) "
                  points="-1.88767717 58.5826378 35.2776772 58.5826378 35.2776772 59.2126378 -1.88767717 59.2126378"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-4"
                  fill="#000000"
                  opacity="0.300000012"
                  transform="translate(1.575000, 58.897638) rotate(-270.000000) translate(-1.575000, -58.897638) "
                  points="-17.0076772 58.5826378 20.1576772 58.5826378 20.1576772 59.2126378 -17.0076772 59.2126378"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-6"
                  fill="#000000"
                  opacity="0.300000012"
                  transform="translate(31.815000, 58.897638) rotate(-270.000000) translate(-31.815000, -58.897638) "
                  points="13.2323228 58.5826378 50.3976772 58.5826378 50.3976772 59.2126378 13.2323228 59.2126378"
                ></polygon>
                <polygon
                  id="Rectangle-Copy-21"
                  fill="#F3D593"
                  transform="translate(31.500000, 18.897638) scale(1, -1) translate(-31.500000, -18.897638) "
                  points="0 0 34.02 0 63 37.7952756 29.61 37.7952756"
                ></polygon>
                <rect
                  id="Rectangle-Copy-2"
                  fill="#000000"
                  opacity="0.600000024"
                  x="1.26"
                  y="77.480315"
                  width="30.87"
                  height="1"
                ></rect>
                <text
                  id="CG-appli--catie"
                  fontFamily="SourceSansPro-Regular, Source Sans Pro"
                  fontSize="8.0604534"
                  fontWeight="normal"
                  line-spacing="7.55667506"
                  fill="#212121"
                >
                  <tspan x="12.7462972" y="54.9740157">
                    CG
                  </tspan>
                  <tspan x="7.64" y="62.5306908">
                    appli-
                  </tspan>
                  <tspan x="9.29239295" y="70.0873659">
                    catie
                  </tspan>
                </text>
              </g>
              <g id="Group-33" transform="translate(174.344628, 0.000000)">
                <line
                  x1="2.51686361e-13"
                  y1="113.402884"
                  x2="32.8979592"
                  y2="64.7565213"
                  id="Line-4"
                  stroke="#000000"
                  strokeWidth="0.503778338"
                  strokeLinecap="square"
                ></line>
                <g id="Group-39" transform="translate(28.153061, 0.000000)">
                  <line
                    x1="2.84693878"
                    y1="4.54600394"
                    x2="2.84693878"
                    y2="37.782649"
                    id="Line-4-Copy-4"
                    stroke="#000000"
                    strokeWidth="0.503778338"
                    strokeLinecap="square"
                  ></line>
                  <line
                    x1="7.2755102"
                    y1="0.31588547"
                    x2="7.2755102"
                    y2="37.782649"
                    id="Line-4-Copy-7"
                    stroke="#000000"
                    strokeWidth="0.503778338"
                    strokeLinecap="square"
                  ></line>
                  <path
                    d="M4.13889803,59.3862863 L4.13889803,56.8593846 L5.30820971,56.8593846 C5.30820971,58.6796383 5.30820971,59.5971207 5.30820971,59.611832 C5.30820971,60.0611539 4.91843915,60.3675138 4.13889803,60.5309117 C2.6016044,60.7178824 1.80140138,61.6170138 1.80140138,63.1675024 C1.80140138,64.8464384 2.80730411,65.8276544 4.5284808,65.8276544 C5.24881935,65.8276544 5.91149965,65.9694025 6.4397739,65.5692397 C6.96756672,64.8844848 7.38689585,64.4249796 7.69776129,64.1907242 C8.09579008,63.8907861 8.82727672,63.7903986 8.85182794,63.8143473 C8.92170924,63.8825136 8.30248332,64.6477347 6.99415018,66.1100106 C6.32250948,66.6458412 5.46347946,66.9677196 4.5284808,66.9677196 C2.37681514,66.9677196 0.632653061,65.2663624 0.632653061,63.1675024 C0.632653061,61.1970898 2.17033627,59.5766772 4.13889803,59.3862863 Z"
                    id="Shape"
                    fill="#000000"
                    fillRule="nonzero"
                    transform="translate(4.744898, 61.913552) scale(-1, 1) translate(-4.744898, -61.913552) "
                  ></path>
                  <g id="Group-38" transform="translate(0.000000, 36.642715)">
                    <g id="Group-37">
                      <mask id="mask-52" fill="white">
                        <use xlinkHref="#path-51"></use>
                      </mask>
                      <use
                        id="Rectangle"
                        fill="#424242"
                        xlinkHref="#path-51"
                      ></use>
                      <g id="Group-29" mask="url(#mask-52)" fill="#FFBC2C">
                        <g transform="translate(5.024523, 8.820594) rotate(-30.000000) translate(-5.024523, -8.820594) translate(-13.403369, -17.930822)">
                          <polygon
                            id="Rectangle"
                            transform="translate(17.713522, 15.424899) rotate(30.000000) translate(-17.713522, -15.424899) "
                            points="2.60450849 23.4185854 32.1907641 6.33695275 32.8225351 7.43121211 3.23627943 24.5128448"
                          ></polygon>
                          <polygon
                            id="Rectangle-Copy-38"
                            transform="translate(17.628762, 18.098585) rotate(30.000000) translate(-17.628762, -18.098585) "
                            points="2.51974905 26.0922721 32.1060047 9.01063946 32.7377756 10.1048988 3.15151999 27.1865315"
                          ></polygon>
                          <polygon
                            id="Rectangle-Copy-39"
                            transform="translate(18.091897, 21.088158) rotate(30.000000) translate(-18.091897, -21.088158) "
                            points="2.98288324 29.0818443 32.5691389 12.0002116 33.2009098 13.094471 3.61465417 30.1761037"
                          ></polygon>
                          <polygon
                            id="Rectangle-Copy-40"
                            transform="translate(18.007137, 23.761844) rotate(30.000000) translate(-18.007137, -23.761844) "
                            points="2.8981238 31.755531 32.4843794 14.6738984 33.1161504 15.7681577 3.52989474 32.8497904"
                          ></polygon>
                          <polygon
                            id="Rectangle-Copy-41"
                            transform="translate(18.470271, 26.751417) rotate(30.000000) translate(-18.470271, -26.751417) "
                            points="3.36125798 34.7451032 32.9475136 17.6634705 33.5792846 18.7577299 3.99302892 35.8393626"
                          ></polygon>
                          <polygon
                            id="Rectangle-Copy-42"
                            transform="translate(18.385512, 29.425103) rotate(30.000000) translate(-18.385512, -29.425103) "
                            points="3.27649854 37.4187899 32.8627542 20.3371573 33.4945251 21.4314166 3.90826948 38.5130493"
                          ></polygon>
                          <polygon
                            id="Rectangle-Copy-43"
                            transform="translate(18.848646, 32.414675) rotate(30.000000) translate(-18.848646, -32.414675) "
                            points="3.73963273 40.4083621 33.3258884 23.3267294 33.9576593 24.4209888 4.37140367 41.5026215"
                          ></polygon>
                          <polygon
                            id="Rectangle-Copy-44"
                            transform="translate(18.763887, 35.088362) rotate(30.000000) translate(-18.763887, -35.088362) "
                            points="3.65487329 43.0820488 33.2411289 26.0004162 33.8728999 27.0946755 4.28664423 44.1763082"
                          ></polygon>
                          <polygon
                            id="Rectangle-Copy-45"
                            transform="translate(19.227021, 38.077934) rotate(30.000000) translate(-19.227021, -38.077934) "
                            points="4.11800747 46.071621 33.7042631 28.9899883 34.336034 30.0842477 4.74977841 47.1658804"
                          ></polygon>
                        </g>
                      </g>
                    </g>
                    <rect
                      id="Rectangle"
                      fill="#D8D8D8"
                      x="0"
                      y="0"
                      width="8.85714286"
                      height="1"
                    ></rect>
                  </g>
                  <polygon
                    id="Rectangle-Copy-46"
                    fill="#9B9B9B"
                    points="0 36.6427145 10.755102 36.6427145 10.755102 37.2744854 0 37.2744854"
                  ></polygon>
                  <mask id="mask-54" fill="white">
                    <use xlinkHref="#path-53"></use>
                  </mask>
                  <use id="Rectangle" fill="#212121" xlinkHref="#path-53"></use>
                </g>
                <line
                  x1="33.5306122"
                  y1="113.402884"
                  x2="32.8979592"
                  y2="64.7565213"
                  id="Line-4-Copy"
                  stroke="#000000"
                  strokeWidth="0.503778338"
                  strokeLinecap="square"
                ></line>
                <line
                  x1="61.890457"
                  y1="76.2783298"
                  x2="32.8979592"
                  y2="64.7565213"
                  id="Line-4-Copy-2"
                  stroke="#000000"
                  strokeWidth="0.503778338"
                  strokeLinecap="square"
                ></line>
                <line
                  x1="31.968771"
                  y1="75.8125128"
                  x2="32.8979592"
                  y2="64.7565213"
                  id="Line-4-Copy-3"
                  stroke="#000000"
                  strokeWidth="0.503778338"
                  strokeLinecap="square"
                ></line>
              </g>
              <g
                id="Rectangle-Copy-22"
                opacity="0.100000001"
                transform="translate(203.344628, 187.500000) scale(1, -1) translate(-203.344628, -187.500000) "
              >
                <use
                  fill="#000000"
                  fillRule="evenodd"
                  xlinkHref="#path-55"
                ></use>
                <use
                  fill="black"
                  fillOpacity="1"
                  filter="url(#filter-56)"
                  xlinkHref="#path-55"
                ></use>
              </g>
              <polygon
                id="Rectangle-Copy-24"
                fill="#000000"
                opacity="0.200000003"
                transform="translate(216.344628, 169.500000) scale(1, -1) translate(-216.344628, -169.500000) "
                points="199.344628 168 231.208484 168 233.344628 171 199.344628 171"
              ></polygon>
              <polygon
                id="Rectangle-Copy-23"
                fill="#000000"
                opacity="0.300000012"
                transform="translate(186.344628, 187.500000) scale(1, -1) translate(-186.344628, -187.500000) "
                points="173.344628 168 176.348437 168 199.344628 203.828887 199.344628 207"
              ></polygon>
              <text
                id="Haven"
                fontFamily="SourceSansPro-SemiBold, Source Sans Pro"
                fontSize="8.0604534"
                fontWeight="500"
                fill="#000000"
              >
                <tspan x="124.821958" y="238">
                  Haven
                </tspan>
              </text>
            </g>
            <text
              id="Bestaande-IT-infrast"
              fontFamily="SourceSansPro-Regular, Source Sans Pro"
              fontSize="16"
              fontWeight="normal"
              fill="#000000"
            >
              <tspan x="95.8966282" y="428.612381">
                Bestaande IT infrastructuur,{' '}
              </tspan>
              <tspan x="97.7366282" y="448.612381">
                zowel cloud als on-premise
              </tspan>
            </text>
            <text
              id="(Common-Ground)-appl"
              fontFamily="SourceSansPro-Regular, Source Sans Pro"
              fontSize="16"
              fontWeight="normal"
              fill="#000000"
            >
              <tspan x="166.122442" y="16">
                (Common Ground){' '}
              </tspan>
              <tspan x="192.186442" y="36">
                applicaties
              </tspan>
            </text>
            <path
              id="Line-2"
              d="M280.224501,54.8034665 L281.186731,55.1022686 L281.03733,55.5833839 C278.843746,62.6473717 274.892683,68.5249205 269.190982,73.2056438 C265.360223,76.3504466 260.741697,78.9530916 255.337344,81.014853 L255.07,81.114 L256.251539,84.9861335 L246.32481,83.2889528 L253.615319,76.3413737 L254.776,80.149 L254.976492,80.0741306 C260.283702,78.0496114 264.808117,75.5001063 268.551676,72.4268896 C273.937808,68.0052275 277.712591,62.4955182 279.882277,55.888265 L280.0751,55.2845818 L280.224501,54.8034665 Z"
              fill="#757575"
              fillRule="nonzero"
            ></path>
            <text
              id="Haven-zorgt-ervoor-d"
              fontFamily="SourceSansPro-Regular, Source Sans Pro"
              fontSize="16"
              fontWeight="normal"
              fill="#000000"
            >
              <tspan x="24.3606282" y="64.6466017">
                Haven zorgt{' '}
              </tspan>
              <tspan x="19.5766282" y="84.6466017">
                ervoor dat de{' '}
              </tspan>
              <tspan x="27.9446282" y="104.646602">
                applicaties{' '}
              </tspan>
              <tspan x="14.8966282" y="124.646602">
                werken met de{' '}
              </tspan>
              <tspan x="21.3846282" y="144.646602">
                bestaande IT{' '}
              </tspan>
              <tspan x="28.8966282" y="164.646602">
                infrastruur
              </tspan>
            </text>
            <path
              id="Line"
              d="M20.9238719,175.104408 L21.8602344,175.476424 L21.6742264,175.944605 C15.0676502,192.573328 11.7677141,206.210086 11.7677141,216.843679 C11.7677141,225.37589 16.0646047,233.380858 24.697005,240.872814 L24.699,240.873 L27.1017417,237.702974 L31.5466972,246.739719 L21.6441219,244.906856 L24.089,241.678 L24.0412166,241.637757 C15.2000617,233.965899 10.7601574,225.696458 10.7601574,216.843679 C10.7601574,206.248699 13.9745203,192.782894 20.3968828,176.435632 L20.737864,175.572589 L20.9238719,175.104408 Z"
              fill="#757575"
              fillRule="nonzero"
            ></path>
            <path
              id="Line-5"
              d="M244.417484,375.549917 L244.476,379.606 L244.793814,379.627578 C249.568139,379.991901 254.080588,381.023772 258.329409,382.723381 C264.90921,385.355426 270.851045,389.586446 276.15246,395.411174 L276.663501,395.979833 L276.997879,396.35664 L276.244265,397.025397 L275.909886,396.648589 C270.58123,390.643786 264.597234,386.315809 257.955197,383.658868 C253.802433,381.997684 249.39005,380.988713 244.716296,380.632148 L244.491,380.616 L244.549554,384.586735 L235.48448,380.199845 L244.417484,375.549917 Z"
              fill="#757575"
              fillRule="nonzero"
            ></path>
          </g>
        </g>
      </g>
    </svg>
  )
}

DiagramMobile.propTypes = {
  diagramA11yText: string,
}

export default DiagramMobile
