// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import Header from 'src/components/Header'
import { object } from 'prop-types'
import Head from 'next/head'
import Footer from 'src/components/Footer'
import News from 'src/components/NewsSection'
import Introduction from './Introduction'
import Symptoms from './Symptoms'
import Solutions from './Solutions'
import Overview from './Overview'
import Adoptation from './Adoptation'

export default function Home({ sections }) {
  return (
    <>
      <Head>
        <title>Haven - Homepage</title>
        <meta name="description" content={sections.meta.description} />
        <meta name="author" content={sections.meta.author} />
      </Head>

      <Header homepage />

      <main>
        <Introduction {...sections.introduction} />
        <Symptoms {...sections.symptoms} />
        <Solutions {...sections.solutions} />
        <Overview {...sections.overview} />
        <Adoptation {...sections.adoptation} />
        <News {...sections.news} />
      </main>

      <Footer />
    </>
  )
}

Home.propTypes = {
  sections: object.isRequired,
}
