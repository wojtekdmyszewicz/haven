// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string } from 'prop-types'
import Section, { SectionIntro } from 'src/components/Section'
import { Container, CenterCol } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import ButtonWrapper from 'src/components/ButtonWrapper'
import Button from '@commonground/design-system/dist/components/Button'
import { getIcon } from 'src/icons'
import Link from 'next/link'

import { List, Item, StyledIcon, StyledHtmlContent } from './index.styles'

const Solutions = ({
  content,
  button1Text,
  button1Link,
  button2Text,
  button2Link,
  ...props
}) => {
  const solutions = Object.entries(props)
    .filter(([key]) => key.substring(0, 8) === 'solution')
    .map(([, solution]) => solution)

  return (
    <Section>
      <Container>
        <CenterCol>
          <HtmlContent as={SectionIntro} content={content} />

          {solutions.length && (
            <List>
              {solutions.map((solution, i) => (
                <Item key={i}>
                  <StyledIcon as={getIcon(solution.icon)} />
                  <StyledHtmlContent content={solution.content} />
                </Item>
              ))}
            </List>
          )}

          <ButtonWrapper>
            {button1Text && (
              <Link passHref href={button1Link}>
                <Button as="a" variant="secondary">
                  {button1Text}
                </Button>
              </Link>
            )}
            {button2Text && (
              <Link passHref href={button2Link}>
                <Button as="a" variant="secondary">
                  {button2Text}
                </Button>
              </Link>
            )}
          </ButtonWrapper>
        </CenterCol>
      </Container>
    </Section>
  )
}

Solutions.propTypes = {
  content: string.isRequired,
  button1Text: string,
  button1Link: string,
  button2Text: string,
  button2Link: string,
}

export default Solutions
