// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import styled from 'styled-components'
import Icon from '@commonground/design-system/dist/components/Icon'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import HtmlContent from 'src/components/HtmlContent'

export const List = styled.ul`
  margin: ${(p) =>
    `${p.theme.tokens.spacing09} auto ${p.theme.tokens.spacing05}`};
  padding: 0;
  list-style-type: none;

  ${mediaQueries.mdUp`
    margin-top: ${(p) => p.theme.tokens.spacing10};
  `}
`

export const Item = styled.li`
  display: flex;
  margin-bottom: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.mdUp`
    margin-top: ${(p) => p.theme.tokens.spacing06};
  `}
`

export const StyledIcon = styled(Icon)`
  flex: 0 0 auto;
  width: ${(p) => p.theme.tokens.iconSizeLarge};
  height: ${(p) => p.theme.tokens.iconSizeLarge};
  margin-right: ${(p) => p.theme.tokens.spacing05};
  fill: ${(p) => p.theme.tokens.colorFocus};

  ${mediaQueries.smUp`
    width: ${(p) => p.theme.listIconSize};
    height: ${(p) => p.theme.listIconSize};
    margin-right: ${(p) => p.theme.tokens.spacing07};
  `}
`

export const StyledHtmlContent = styled(HtmlContent)`
  text-align: left;

  h3 {
    margin-top: 0;
  }
`
