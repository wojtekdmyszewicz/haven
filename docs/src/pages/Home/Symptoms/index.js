// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string, arrayOf, shape } from 'prop-types'
import { Container, CenterCol } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import Section from 'src/components/Section'
import { getIcon } from 'src/icons'
import { List, Item, StyledIcon } from './index.styles'

const Symptoms = ({ content, symptoms }) => (
  <Section alternate>
    <Container>
      <CenterCol>
        <HtmlContent content={content} />
      </CenterCol>

      <List>
        {symptoms.map(({ icon, text }, i) => (
          <Item key={i}>
            <StyledIcon as={getIcon(icon)} inline />
            <span>{text}</span>
          </Item>
        ))}
      </List>
    </Container>
  </Section>
)

Symptoms.propTypes = {
  content: string.isRequired,
  symptoms: arrayOf(
    shape({
      icon: string,
      text: string.isRequired,
    }),
  ).isRequired,
}

export default Symptoms
