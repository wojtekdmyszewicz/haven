// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import React from 'react'
import { string, arrayOf, shape } from 'prop-types'
import { Container, CenterCol } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import LinkButton from 'src/components/LinkButton'
import { IconQuotationMark as QuotationMark } from 'src/icons'
import Icon from '@commonground/design-system/dist/components/Icon'
import {
  LinkWrapper,
  AdoptationText,
  List,
  Item,
  Image,
  QuotesContainer,
  Figure,
  Blockquote,
  StyledSection,
} from './index.styles'

const Adoptation = ({
  content,
  mainLinkText,
  mainLinkHref,
  adoptationText,
  organizationImages,
  reviews,
}) => (
  <StyledSection>
    <Container>
      <CenterCol>
        <HtmlContent content={content} />

        <LinkWrapper>
          <LinkButton href={mainLinkHref} text={mainLinkText} />
        </LinkWrapper>
      </CenterCol>

      {organizationImages.length && (
        <>
          <AdoptationText>{adoptationText}</AdoptationText>
          <List>
            {organizationImages.map(({ src, alt, url }, i) => (
              <Item key={i}>
                <a href={url} target="_blank" rel="noreferrer">
                  <Image src={src} alt={alt} />
                </a>
              </Item>
            ))}
          </List>
        </>
      )}
    </Container>

    {reviews.length && (
      <QuotesContainer>
        {reviews.map(({ quote, quotee, organization }, i) => (
          <Figure key={i}>
            <Icon as={QuotationMark} />
            <Blockquote>
              <p>{quote}</p>
              <figcaption>
                {quotee}
                <cite>{organization}</cite>
              </figcaption>
            </Blockquote>
          </Figure>
        ))}
      </QuotesContainer>
    )}
  </StyledSection>
)

Adoptation.propTypes = {
  content: string.isRequired,
  mainLinkText: string,
  mainLinkHref: string,
  adoptationText: string.isRequired,
  organizationImages: arrayOf(
    shape({
      src: string.isRequired,
      alt: string,
    }),
  ),
  reviews: arrayOf(
    shape({
      quote: string.isRequired,
      quotee: string,
      organization: string,
    }),
  ),
}

export default Adoptation
