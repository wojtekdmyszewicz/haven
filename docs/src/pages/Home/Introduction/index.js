// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { useEffect, useRef } from 'react'
import { string, arrayOf, shape } from 'prop-types'
import { Container, Row, Col } from 'src/components/Grid'
import { Media } from 'src/styling/media'
import Link from 'next/link'
import {
  Section,
  Jumbotron,
  MobileBottomBg,
  Background,
  TextContainer,
  Title,
  SubTitle,
  Figure,
  FigCaption,
  Video,
  List,
  Item,
  Image,
  ReferenceText,
  ReferenceUrl,
} from './index.styles'

const Introduction = ({
  siteTitle,
  siteSubTitle,
  videoCaption,
  videoUriWebm,
  videoUriMp4,
  videoStill,
  fallbackYoutubeText,
  fallbackYoutubeUri,
  organizationImages,
  referenceText,
  referenceUrl,
  referenceUrlText,
}) => {
  const refVideo = useRef(null)

  useEffect(() => {
    const playVideo = () => {
      refVideo.current.play()
    }

    // Not using regular onClick because only needed once: poster is a static image, not a player
    refVideo.current.addEventListener('click', playVideo, { once: true })

    return () => refVideo.current?.removeEventListener('click', playVideo)
  }, [])

  return (
    <Section omitArrow>
      <Jumbotron>
        <Media greaterThanOrEqual="md" />
        <Background>
          <img src="home/background.svg" alt="" />
        </Background>

        <TextContainer>
          <Row>
            <Col>
              <Title>{siteTitle}</Title>
              <SubTitle>{siteSubTitle}</SubTitle>
            </Col>
          </Row>
        </TextContainer>
        <MobileBottomBg />
      </Jumbotron>

      <Container>
        <Figure>
          <FigCaption>{videoCaption}</FigCaption>

          <Video controls preload="none" poster={videoStill} ref={refVideo}>
            <source src={videoUriWebm} type="video/webm" />
            <source src={videoUriMp4} type="video/mp4" />
            <p>
              {fallbackYoutubeText} <a href={fallbackYoutubeUri}>youtube</a>
            </p>
          </Video>
        </Figure>
      </Container>

      {organizationImages.length && (
        <Container>
          <List>
            <ReferenceText>{referenceText}</ReferenceText>
            {organizationImages.map(({ src, alt }, i) => (
              <Item key={i}>
                <Image src={src} alt={alt} />
              </Item>
            ))}
            <Link href={referenceUrl}>
              <ReferenceUrl>{referenceUrlText}</ReferenceUrl>
            </Link>
          </List>
        </Container>
      )}
    </Section>
  )
}

Introduction.propTypes = {
  siteTitle: string.isRequired,
  siteSubTitle: string.isRequired,
  videoCaption: string.isRequired,
  videoUriWebm: string.isRequired,
  videoUriMp4: string.isRequired,
  videoStill: string.isRequired,
  fallbackYoutubeText: string.isRequired,
  fallbackYoutubeUri: string.isRequired,
  referenceText: string.isRequired,
  referenceUrl: string.isRequired,
  referenceUrlText: string.isRequired,
  organizationImages: arrayOf(
    shape({
      src: string.isRequired,
      alt: string,
    }),
  ),
}

export default Introduction
