// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//

import { getPageSections } from 'src/lib/api'
import Home from '../src/pages/Home'

export default function HomePage(props) {
  return <Home {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('home')
  return { props: { sections } }
}
