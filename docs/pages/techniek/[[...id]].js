// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import fs from 'fs'
import path from 'path'
import {
  parseMdFile,
  getPageSections,
  buildSectionMap,
  getFilesRecursively,
} from 'src/lib/api'
import yaml from 'js-yaml'
import Technique from '../../src/pages/Technique'

export default function TechniquePage(props) {
  return <Technique {...props} />
}

export async function getStaticProps(pathProps) {
  const files = getFilesRecursively(
    'public/technique/content/documentation',
  ).filter((file) => file.match(/.md$/))

  const currentUrl = files.find(
    (file) =>
      parseMdFile(file).data?.path ===
      `/${pathProps.params.id?.join('/') || ''}`,
  )

  const sectionMap = await buildSectionMap(currentUrl, [''], 'technique')
  const section = Object.fromEntries(sectionMap)

  if (section.technique.specialPage === 'checks') {
    section.technique.checks = yaml.load(
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      fs.readFileSync(
        path.join(process.cwd(), section.technique.checksPath),
        'utf8',
      ),
    )
  }

  const genericSections = await getPageSections('technique')
  const sections = { ...section, ...genericSections }

  return {
    props: { sections },
  }
}

export async function getStaticPaths() {
  const files = getFilesRecursively(
    'public/technique/content/documentation',
  ).filter((file) => file.match(/.md$/))
  const urls = files.map((file) => parseMdFile(file).data?.path)
  const paths = urls.map((url) => {
    return { params: { id: url.split('/').slice(1) } }
  })

  return { paths, fallback: false }
}
