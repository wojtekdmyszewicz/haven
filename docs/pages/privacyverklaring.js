// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { getPageSections } from 'src/lib/api'
import Privacy from 'src/pages/Privacy'

export default function PrivacyPage(props) {
  return <Privacy {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('privacy')
  return { props: { sections } }
}
