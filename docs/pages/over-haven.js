// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { getPageSections } from 'src/lib/api'
import About from 'src/pages/About'

export default function AboutPage(props) {
  return <About {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('about')
  return { props: { sections } }
}
