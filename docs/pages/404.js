// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { getPageSections } from 'src/lib/api'
import NotFound from 'src/pages/NotFound'

export default function NotFoundPage(props) {
  return <NotFound {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('not-found')
  return { props: { sections } }
}
