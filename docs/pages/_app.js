// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
import { ThemeProvider } from 'styled-components'
import { elementType, object } from 'prop-types'
import Head from 'next/head'
import GlobalDsStyles from '@commonground/design-system/dist/components/GlobalStyles'
import GlobalStyles from 'src/components/GlobalStyles'
import DomainNavigation from '@commonground/design-system/dist/components/DomainNavigation'
import { MediaProvider } from 'src/styling/media'
import theme from 'src/styling/theme'
import '@fontsource/source-sans-pro/latin.css'
import '../src/styling/prism-okaidia.css'
import '../src/styling/fonts.css'

function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <GlobalDsStyles />
      <GlobalStyles />
      <MediaProvider>
        <Head>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>

        {pageProps.statusCode !== 404 && (
          <DomainNavigation
            activeDomain="Haven"
            gitLabLink="https://www.gitlab.com/commonground/haven/haven"
          />
        )}
        <Component {...pageProps} />
      </MediaProvider>
    </ThemeProvider>
  )
}

MyApp.propTypes = {
  Component: elementType,
  pageProps: object,
}

export default MyApp
