// Copyright © VNG Realisatie 2019-2023
// Licensed under EUPL v1.2
//
module.exports = {
  plugins: ['react', 'prettier', 'security'],
  extends: ['eslint:recommended', 'next'],
  settings: {
    react: {
      version: 'detect',
    },
  },
  env: {
    es6: true
  },
  rules: {
    'react/react-in-jsx-scope': 'off',
  },
}
